//
//  ViewController.m
//  LiveChatWithNearest
//
//  Created by ibuildx on 2/26/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "DashBoardViewController.h"
#import "EditProfileViewController.h"
#import "UIViewController+CWPopup.h"
#import <CoreLocation/CoreLocation.h>
#import "SearchViewController.h"
#import "SettingViewController.h"
#import "SupportViewController.h"
#import "InviteTableViewController.h"
#import "DataManager.h"
#import "AppDelegate.h"
#import <MMDrawerController.h>
#import "DemoMessagesViewController.h"
#import <SVProgressHUD.h>
#import <TSMessage.h>
#import "SearchViewController.h"
#import "EditProfileViewController.h"
#import "ProfilePopUp.h"
#import <FBSDKLoginManager.h>
#import "BoostViewController.h"


@interface DashBoardViewController () <CLLocationManagerDelegate, UIAlertViewDelegate>
{
    CLLocationManager *locationManager;
    NSString *appId;
    UIAlertController *alert;
}

@end

@implementation DashBoardViewController

#pragma mark - View Life Cycle Methods

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    appId = @"1287696896";
    
    [SVProgressHUD dismiss];
    [self SetupUI];

    [DataManager locationIntegration:locationManager];
    

    self.adView.adUnitID = @"ca-app-pub-6416144291405618/9693659683";
    self.adView.rootViewController = self;
    [self.adView loadRequest:[GADRequest request]];
}



- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self resignSearchResponder];
    
    //pass drawer controller has the chat delegate upon showing this view -- easier than configuring each subview segue to set Drawer the chat delegate
    
    
    if (self.userImageView.image == nil){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            NSString *imgUrl = self.chat.currentUser.imgUrl;
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.userImageView setImage:[UIImage imageWithData:data]];
                
            });
        });
    }

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (_chat != nil){
        [appDelegate.drawerController setChat:_chat];
        _chat.returned = self;
        _chat.delegate = appDelegate.drawerController;
    }else{

        [self setChat:appDelegate.drawerController.chat];
        
    }
    
    
    if (![self.chat.currentUser.firstName  isEqualToString:@""]){
        self.userNameText.text = appDelegate.drawerController.chat.currentUser.firstName;
    }
    
    
    //Hiding and showing the ad based on subscription
    if ([self.chat.currentUser.subscriptionPurchased isEqualToString:@"1"]){

        [self.adView setHidden:YES];
       
    }else{
        
        [self.adView setHidden:NO];
    }
    
    [self.view layoutIfNeeded];
    [self SetupUI];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*****************************************
 *
 * ----- UI SETUP METHODS -----
 *
 *****************************************/



#pragma mark - View Starting Methods
-(void)maskTriananlgesAndCircularBorderedImage {
    
    
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){0, 0}];
    [path addLineToPoint:(CGPoint){0, self.leftDarkView.frame.size.height}];
    [path addLineToPoint:(CGPoint){self.view.frame.size.width / 2, 0}];
    [path addLineToPoint:(CGPoint){0, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = self.leftDarkView.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    self.leftDarkView.layer.mask = mask;
    
    // Build a triangular path
    UIBezierPath *pathRight = [UIBezierPath new];
    [pathRight moveToPoint:(CGPoint){0, 0}];
    [pathRight addLineToPoint:(CGPoint){self.view.frame.size.width / 2 , self.leftDarkView.frame.size.height}];
    [pathRight addLineToPoint:(CGPoint){self.view.frame.size.width / 2 , 0}];
    [pathRight addLineToPoint:(CGPoint){0, 0}];
    
    // Create a CAShapeLayer with this triangular path
    CAShapeLayer *maskRight = [CAShapeLayer new];
    maskRight.frame = self.rightDarkView.bounds;
    maskRight.path = pathRight.CGPath;
    
    // Mask the imageView's layer with this shape
    self.rightDarkView.layer.mask = maskRight;
    self.userImageView.backgroundColor = [UIColor clearColor];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
    self.userImageView.clipsToBounds = YES;
    
    UIColor *bgColor = [DataManager getGreenColor];
    
    self.userImageView.layer.borderWidth = self.userImageView.frame.size.width / 40;
    self.userImageView.layer.borderColor = bgColor.CGColor;
    
}

-(void)SetupUI {
    
    _settingsBtn.layer.cornerRadius = 15;
    _supportBtn.layer.cornerRadius = 15;
    _editProfileBtn.layer.cornerRadius = 15;
    _inviteBtn.layer.cornerRadius = 15;
    _rateBtn.layer.cornerRadius = 15;
    _boostBtn.layer.cornerRadius = 15;
    
    
    //drop shadows
    self.boostBtn.clipsToBounds = NO;
    self.boostBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostBtn.layer.shadowOpacity = 0.4;
    self.boostBtn.layer.shadowRadius = 8;
    self.boostBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.boostBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostBtn.titleLabel.layer.shadowRadius = 5;
    self.boostBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.boostBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.rateBtn.clipsToBounds = NO;
    self.rateBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.rateBtn.layer.shadowOpacity = 0.4;
    self.rateBtn.layer.shadowRadius = 8;
    self.rateBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.rateBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.rateBtn.titleLabel.layer.shadowRadius = 5;
    self.rateBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.rateBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.inviteBtn.clipsToBounds = NO;
    self.inviteBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.inviteBtn.layer.shadowOpacity = 0.4;
    self.inviteBtn.layer.shadowRadius = 8;
    self.inviteBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.inviteBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.inviteBtn.titleLabel.layer.shadowRadius = 5;
    self.inviteBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.inviteBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.settingsBtn.clipsToBounds = NO;
    self.settingsBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.settingsBtn.layer.shadowOpacity = 0.4;
    self.settingsBtn.layer.shadowRadius = 8;
    self.settingsBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.settingsBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.settingsBtn.titleLabel.layer.shadowRadius = 5;
    self.settingsBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.settingsBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.supportBtn.clipsToBounds = NO;
    self.supportBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.supportBtn.layer.shadowOpacity = 0.4;
    self.supportBtn.layer.shadowRadius = 8;
    self.supportBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.supportBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.supportBtn.titleLabel.layer.shadowRadius = 5;
    self.supportBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.supportBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.editProfileBtn.clipsToBounds = NO;
    self.editProfileBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.editProfileBtn.layer.shadowOpacity = 0.4;
    self.editProfileBtn.layer.shadowRadius = 8;
    self.editProfileBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.editProfileBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.editProfileBtn.titleLabel.layer.shadowRadius = 5;
    self.editProfileBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.editProfileBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    [self.editProfileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.editProfileBtn setTitle:@"Edit Profile" forState:UIControlStateNormal];
    
    NSString *bio = self.chat.currentUser.profileInfo;
    
    
    
    if ([bio isEqualToString:@"I'm looking for..." ] || [bio isEqualToString:@"\n"]){
        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        BOOL hasSeenEditProfilePrompt = [defaults boolForKey:@"hasSeenEditProfilePrompt"];
//        [defaults setBool:YES forKey:@"hasSeenEditProfilePrompt"];
//        [defaults synchronize];
        
        [self.editProfileBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [self.editProfileBtn setTitle:@"Update Profile!" forState:UIControlStateNormal];
        

            alert=   [UIAlertController
                  alertControllerWithTitle:@"Profile Setup"
                  message:@"Edit your profile info!"
                  preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Take Me There"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [self performSegueWithIdentifier:@"EditProfileSegue" sender:nil];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
            [alert addAction:cancel];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    [self maskTriananlgesAndCircularBorderedImage];
}


#pragma mark - Backendless And Responses Methods

/*****************************************
 *
 * ----- ACTION METHODS -----
 *
 *****************************************/


#pragma mark - UI Buttons Method

- (IBAction)editProfileAction:(id)sender {
    [self performSegueWithIdentifier:@"EditProfileSegue" sender:sender];
}

- (IBAction)settingAction:(id)sender {
    [self performSegueWithIdentifier:@"ToSettingView" sender:sender];
}

- (IBAction)supportAction:(id)sender {
    [self performSegueWithIdentifier:@"ToSupportView" sender:sender];
}

- (IBAction)inviteAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toInviteView" sender:sender];
}

- (IBAction)listBtnAction:(id)sender {
    [self performSegueWithIdentifier:@"backToList" sender:sender];
}

- (IBAction)logOutBtnAction:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"NO" forKey:@"AlreadyUserLogin"];
    [defaults synchronize];
    
    [self performSegueWithIdentifier:@"backToLogin" sender:sender];
}

- (IBAction)rateAction:(id)sender {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", appId]];

    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
        NSLog(success ? @"Yes" : @"No");
        
    }];
}


- (IBAction)showProfileAction:(id)sender {
    
    //Show the profile from the drawer controller otherwise it only shows in the dash drawer
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerController showProfile];
    
}

#pragma mark - Navigation

/*****************************************
 *
 * ----- NAVIGATION METHODS -----
 *
 *****************************************/


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    ChatObject chat = [ChatObject new];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
   
    _chat = appDelegate.drawerController.chat;
    _chat.returned = appDelegate.drawerController;
    _chat.delegate = self;
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = self.chat.currentUser;
        vc.selectedUser = self.selectedUser;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"DashboardView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        vc.delegateModal = self;
        


    }
    
    if ([segue.identifier isEqualToString:@"backToList"]) {
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        [appDelegate.drawerController setChat:_chat];
        _chat.returned = self;
        _chat.delegate = appDelegate.drawerController;
    }
    
    if([segue.identifier isEqualToString:@"EditProfileSegue"]){
        
        EditProfileViewController *messageView = segue.destinationViewController;

        [messageView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = messageView;

        
    }else if([segue.identifier isEqualToString:@"ToSettingView"]){
        SettingViewController *settingView = segue.destinationViewController;
        settingView.currentUser = self.chat.currentUser;
        
        [settingView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = settingView;

        
    }else if([segue.identifier isEqualToString:@"ToSupportView"]){
        SupportViewController *supportView = segue.destinationViewController;
        supportView.currentUser = self.chat.currentUser;
        
        [supportView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = supportView;
        
    }else if([segue.identifier isEqualToString:@"toInviteView"]){
        InviteTableViewController *inviteView = segue.destinationViewController;
        inviteView.currentUser = self.chat.currentUser;
        
        [inviteView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = inviteView;
    }
    
    else if([segue.identifier isEqualToString:@"toBoostView"]){
        BoostViewController *boostVC = segue.destinationViewController;
        
        [boostVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = boostVC;
    }
    
    _chat = nil;
}
- (IBAction)unwindToDashBoard:(UIStoryboardSegue *)unwindSegue

{
    NSLog(@"UNWIND TO DASH CALLED");
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = self.chat.currentUser.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.userImageView setImage:[UIImage imageWithData:data]];
            
        });
    });

    [appDelegate.drawerController setChat:_chat];
    _chat.returned = self;
    _chat.delegate = appDelegate.drawerController;
}

#pragma mark - ActioSheet Method

-(void)showActioSheetWithMessage :(NSString *)msg {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}






#pragma mark - Demo delegate

- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
    
    
-(void)resignSearchResponder{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
     [(SearchViewController*)appDelegate.drawerController.centerViewController resignSearchResponder];
    
}



/*****************************************
 *
 * ----- CHABOBJECT DELEGATE METHODS -----
 *
 *****************************************/



-(void)getMessageNotificationFromUser :(NSString *)user :(NSString *)msgId{
    

}

-(void)getMessage:(NSString *)message fromUser:(NSString *)user :(NSString *)msgId{
    
    NSLog(@"A Message Has Been Recieved as a MEG in Dash...................%@", msgId);
    
    if(![message isEqualToString:@"-1-2-3-4-5"]){


    }
}



@end
