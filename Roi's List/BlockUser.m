//
//  BlockUser.m
//  Accomplice
//
//  Created by ibuildx on 5/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "BlockUser.h"

@implementation BlockUser



-(NSDictionary*) blockUserToDictionary{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:_CurrentUserId forKey:@"currentUserID"];
    [dictionary setValue:_created forKey:@"created"];
    [dictionary setValue:_nBlockUserId forKey:@"nBlockUserId"];
    
    return dictionary;
    
}

-(void) dictionaryToBlockUser:(NSDictionary*)userdict{
    
    self.CurrentUserId = [userdict valueForKey:@"currentUserID"];
    self.created = [userdict valueForKey:@"created"];
    self.nBlockUserId = [userdict valueForKey:@"nBlockUserId"];
}

@end
