//
//  FirebaseManager.h
//  Complice
//
//  Created by Justin Hershey on 8/23/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"
#import "Inbox.h"
#import <Firebase.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import "DataManager.h"
#import "GeoFire.h"
#import "BlockUser.h"


@import FirebaseDatabase;
@import FirebaseStorage;
@import FirebaseMessaging;
@import FirebaseCore;
@import Firebase;


@interface FirebaseManager : NSObject



/************************************************
 * ------------  REFERENCE PATHS
 ************************************************/
+(FIRDatabaseReference*) getUsersPath;
+(FIRDatabaseReference*) getMyBlockedUsersPath;
+(FIRDatabaseReference*) getBlockedMeUsersPath;
+(FIRDatabaseReference*) getConversationPath;
+(FIRDatabaseReference*) getInboxListPath;
+(FIRDatabaseReference*) getFlaggedUsersPath;
+(FIRDatabaseReference*) getSupportPath;
+(FIRDatabaseReference*) getInstagramPath;
+(FIRStorageReference*) getStoragePath;
+(FIRDatabaseReference*) getGeoPath;
+(FIRDatabaseReference*) getBoostedNotificationUsersPath;

+(NSString*) nowInMillisString;



/************************************************
 * ------------  USER METHODS
 ************************************************/
+(void) updateFirebaseUserValue:(NSString*)key value:(NSString*)value uid:(NSString*)uid;
+(void) getUserFirebaseDataForUid:(NSString*)uid completion:(void (^)(UserObject* userdata))completion;



+(void) getBadgeCount:(void (^)(int badgeCount))completion;


/************************************************
 * ------------  INBOX METHODS
 ************************************************/
+(void) getInboxList:(void (^)(NSDictionary* inboxList))completion;
+(void) getInboxMessagesForObjectId:(NSString*)objectId completion:(void (^)(NSArray* messageArray))completion;
+(void) getLatestMessage:(NSString*)objectID completion:(void (^)(Inbox* inbox))completion;
+(void) getInboxListRemovingBlockedUsers:(void (^)(NSDictionary* inboxList))completion;
+(void) getConversationObjectId:(NSString*)withUserUid completion:(void (^)(NSString* conversationId))completion;
+(void)postNotification:(NSString*)deviceToken conversationId:(NSString*)conversationId forName:(NSString*)forName withMessage:(NSString*)withMessage;


//this method updates local lists stored in NSUserDefaults
+(void) updateInboxList: (void (^)(NSDictionary *inboxList))completion;
+(void) setInboxMessage:(NSString*)message recipient:(NSString*)recipient completion:(void (^)(NSString* convoId))completion;
//will create a conversation ID and set it as the value for key withUserId in the current users inbox list in Firebase, additionally it sets the conversation Id to the current user's Id in the withUserId's inbox List
+(void) updateInboxListsForUser:(NSString*)withUserUid completion:(void (^)(NSString* conversationId))completion;
+(void) updateSingleInboxListForUser:(NSString*)uid parter:(NSString*)parter conversationId:(NSString*)conversationId;






/************************************************
 * ------------  GEOFIRE METHODS
 ************************************************/

+(void)setGeoFireLocationForUser:(NSString*)uid latitude:(double)latitude longitude:(double)longitude;
+(void)queryNearbyUsersWithRadius:(CLLocation*)location radius:(double)radius completion:(void (^)(NSDictionary * users))completion;







/************************************************
 * ------------  BLOCKED USER METHODS
 ************************************************/

+(void)getAllUsersWhoBlockedMe:(void (^)(NSDictionary* users))completion;
+(void)getAllBlockedUsersForUser:(void (^)(NSDictionary* users))completion;
+(void)unblockUser:(NSString*)uid;
+(void)setBlockedUser:(NSString*)uid;
+(void)getBlockedUserListToExclude:(void (^)(NSDictionary * users))completion;
+(void)checkIfUserBlocked:(NSString*)uid completion:(void (^)(bool blocked))completion;





/************************************************
 * ------------  FLAG USER METHODS
 ************************************************/

+(void)flagUser:(NSString*)uid reason:(NSString*)reason offenderUID:(NSString*)offenderUID offenderEmail:(NSString*)offenderEmail reporterEmail:(NSString*)reporterEmail completion:(void (^)(bool success))completion;





/************************************************
 * ------------   SUPPORT METHODS
 ************************************************/

+(void)sendSupportRequest:(NSString*)uid reason:(NSString*)reason reporterEmail:(NSString*)reporterEmail completion:(void (^)(bool success))completion;





/**************************************
 *    PHOTO STORAGE METHODS
 ***************************************/
+(void) getPhotoDictionary:(NSString*)uid completion:(void (^)(NSDictionary *photosData))completion;
+(void)uploadPhoto:(NSData*)imageData creationDate:(NSString*)creationDate caption:(NSString*)caption completion:(void (^)(NSString *downloadString))completion;
+(void)deletePhoto:(NSString*)creationDate completion:(void (^)(bool success))completion;
+(void)uploadProfilePhoto:(NSData*)imageData completion:(void (^)(NSString *downloadString))completion;

@end
