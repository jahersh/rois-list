//
//  BoostViewController.h
//  Roi's List
//
//  Created by Justin Hershey on 10/5/17.
//  Copyright © 2017 Justin Hershey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import <StoreKit/StoreKit.h>
#import <SVProgressHUD.h>
#import "ChatObject.h"
#import "DashBoardViewController.h"
#import "FirebaseManager.h"
#import <Firebase.h>

@interface BoostViewController : UIViewController <SKProductsRequestDelegate,SKPaymentTransactionObserver, ChatProtocol, UIPickerViewDelegate, UIPickerViewDataSource>

@property ChatObject *chat;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@property (weak, nonatomic) IBOutlet UILabel *timeRemainingLbl;
@property (weak, nonatomic) IBOutlet UIButton *startStopBtn;
@property (weak, nonatomic) IBOutlet UIButton *purchaseMoreBtn;
@property (weak, nonatomic) IBOutlet UILabel *boostDaysLeftLbl;

@property NSString *kBoostedProfileProductId;
@property SKProductsRequest *productsRequest;
@property NSArray *validProducts;

- (void)fetchAvailableProducts;
- (BOOL)canMakePurchases;
- (void)purchaseMyProduct:(SKProduct*)product;

- (IBAction)startStopAction:(id)sender;
- (IBAction)purchaseAction:(id)sender;

@property NSTimeInterval interval;
@property NSTimer *timer;
@property NSTimeInterval sExpireTime;

@end
