//
//  ProfilePopUp.m
//  Accomplice
//
//  Created by ibuildx on 5/2/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "InstagramProfilePopUp.h"
#import "BlockUser.h"
#import "DataManager.h"
#import "Inbox.h"
#import "FlagViewController.h"
#import "SearchViewController.h"



@interface InstagramProfilePopUp ()
{
    
    UIAlertController *alert;
    
}
@end


@implementation InstagramProfilePopUp

@synthesize user;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //INSTAGRAM INIT
    _cachedImages = [[NSMutableDictionary alloc] init];
    InstagramManager *instagramMan = [InstagramManager alloc];
    self.instagramPhotoArray = [[NSMutableArray alloc] init];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    // Do any additional setup after loading the view from its nib.
    _scrollView.delegate = self;
    
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CustomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"instagramCollectionCell"];
    
    NSString *tempText = user.profileInfo; //@" Profile description";
    
    self.discText.delegate = self;
    self.discText.text = tempText;
    
    self.nameText.text = [NSString stringWithFormat:@"%@", user.firstName];
    
    self.nameDistanceText.text = [NSString stringWithFormat:@"%@, %@", user.city, user.state];
//    self.genderAgeText.text = [NSString stringWithFormat:@"%@", user.gender];
    
    
    //GET INSTAGRAM PHOTO LIST
    [instagramMan getPhotosListingAsync:self.user.uid completion:^(double mostRecentTime, NSDictionary *currentPhotoData) {
        
        if (currentPhotoData != nil){
            
            for (NSString* key in currentPhotoData){
                
                NSDictionary *temp = [currentPhotoData objectForKey:key];
                
                [self.instagramPhotoArray addObject:temp];
                
                NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
                NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
                self.instagramPhotoArray = [self.instagramPhotoArray sortedArrayUsingDescriptors:sortDescriptors].mutableCopy;
            }
            
            //Adding "Take me to Instagram Photo"
            NSMutableDictionary *localInstaPhoto = [[NSMutableDictionary alloc] init];
            [localInstaPhoto setValue:@"toInstagram" forKey:@"url"];
            [localInstaPhoto setValue:@"" forKey:@"caption"];
            
            NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
            NSString *nowStr = [NSString stringWithFormat:@"%f", now];
            
            
            [localInstaPhoto setValue:nowStr forKey:@"creation"];
            [self.collectionView reloadData];
        }
    }];
    
    
    [self setupView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [self loadViewIfNeeded];
}


/**********************************************
 *
 * ------------ UI SETUP METHODS --------------
 *
 **********************************************/


-(void)viewDidLayoutSubviews{
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.view.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii:(CGSize){15.0, 15.}].CGPath;
    
    self.view.layer.mask = maskLayer;
    
    
    CAShapeLayer * infoMaskLayer = [CAShapeLayer layer];
    infoMaskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.infoView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
    
    self.infoView.layer.mask = infoMaskLayer;
    
    self.prfilePic.clipsToBounds = YES;
    self.prfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.prfilePic.layer.borderWidth = self.prfilePic.frame.size.width / 100;
    self.prfilePic.layer.cornerRadius = self.prfilePic.frame.size.width / 2 ;
    
    CGFloat fixedWidth = self.discText.frame.size.width;
    CGSize textViewSize = [self.discText sizeThatFits:CGSizeMake(fixedWidth, CGFLOAT_MAX)];
    
    self.textViewHeight.constant = textViewSize.height + .4 * textViewSize.height;
    
    CGRect newFrame = _discText.frame;
    newFrame.size = CGSizeMake(fmaxf(textViewSize.width, fixedWidth), textViewSize.height);
    _discText.frame = newFrame;
    
    if(textViewSize.height <= _scrollView.frame.size.height){
        
        [self.scrollArrow setHidden:YES];
        
    }
    
    
}



-(void)setupView{
    
    NSLog(@" textView size %f", self.textViewHeight.constant);
    NSLog(@" scrollview size %f", self.scrollView.frame.size.height);
    
    if (self.textViewHeight.constant - 10 <= self.scrollView.frame.size.height) {
        self.scrollArrow.hidden = YES;
    }
    
    self.scrollArrowUp.hidden = YES;
    
    
    //drop shadows
    self.propertyContractedLbl.clipsToBounds = NO;
    self.propertyContractedLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.propertyContractedLbl.layer.shadowOpacity = 0.4;
    self.propertyContractedLbl.layer.shadowRadius = 6;
    self.propertyContractedLbl.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    self.propertyContractedLbl.backgroundColor = [UIColor orangeColor];
    self.propertyContractedLbl.layer.cornerRadius = 5.0;
    self.propertyContractedLbl.text = @"Pending Contract";
    
    if ([self.user.propertyContracted isEqualToString:@"1"]){
        self.propertyContractedLbl.hidden = NO;
    }else{
        self.propertyContractedLbl.hidden = YES;
    }
    
    
    double sExpireTime = [self.user.boostExpireTime doubleValue];
    NSString *profileBoosted = user.boostedProfile;
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    
    if ([profileBoosted isEqualToString: @"1"] && (nowMillis < sExpireTime)){
        
        [self setBoostVisibility:NO];
        
        if(sExpireTime > nowMillis){
            
            self.interval = sExpireTime - nowMillis;
            self.expireTime = sExpireTime;
            
            [self startTimer];
        }
    }else{
        [self setBoostVisibility:YES];
    }
    
    self.boostLbl.clipsToBounds = NO;
    self.boostLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostLbl.layer.shadowOpacity = 0.4;
    self.boostLbl.layer.shadowRadius = 3;
    self.boostLbl.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.boostExpiresLbl.clipsToBounds = NO;
    self.boostExpiresLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostExpiresLbl.layer.shadowOpacity = 0.4;
    self.boostExpiresLbl.layer.shadowRadius = 3;
    self.boostExpiresLbl.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    
    self.flagBtn.clipsToBounds = NO;
    self.flagBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.flagBtn.layer.shadowOpacity = 0.4;
    self.flagBtn.layer.shadowRadius = 6;
    self.flagBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.nameDistanceText.clipsToBounds = NO;
    self.nameDistanceText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.nameDistanceText.layer.shadowOpacity = 0.4;
    self.nameDistanceText.layer.shadowRadius = 3;
    self.nameDistanceText.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.genderAgeText.clipsToBounds = NO;
    self.genderAgeText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.genderAgeText.layer.shadowOpacity = 0.4;
    self.genderAgeText.layer.shadowRadius = 3;
    self.genderAgeText.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.nameText.clipsToBounds = NO;
    self.nameText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.nameText.layer.shadowOpacity = 0.4;
    self.nameText.layer.shadowRadius = 8;
    self.nameText.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.scrollArrow.clipsToBounds = NO;
    self.scrollArrow.layer.shadowColor = [UIColor blackColor].CGColor;
    self.scrollArrow.layer.shadowOpacity = 0.4;
    self.scrollArrow.layer.shadowRadius = 3;
    self.scrollArrow.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.scrollArrowUp.clipsToBounds = NO;
    self.scrollArrowUp.layer.shadowColor = [UIColor blackColor].CGColor;
    self.scrollArrowUp.layer.shadowOpacity = 0.4;
    self.scrollArrowUp.layer.shadowRadius = 3;
    self.scrollArrowUp.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.blkBtn.clipsToBounds = NO;
    self.blkBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.blkBtn.layer.shadowOpacity = 0.4;
    self.blkBtn.layer.shadowRadius = 6;
    self.blkBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.waveBtn.clipsToBounds = NO;
    self.waveBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.waveBtn.layer.shadowOpacity = 0.4;
    self.waveBtn.layer.shadowRadius = 6;
    self.waveBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.messageBtn.clipsToBounds = NO;
    self.messageBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.messageBtn.layer.shadowOpacity = 0.4;
    self.messageBtn.layer.shadowRadius = 6;
    self.messageBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.closeBtn.clipsToBounds = NO;
    self.closeBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.closeBtn.layer.shadowOpacity = 0.4;
    self.closeBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    
    //Setup  prfile pic
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = user.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.prfilePic setImage:[UIImage imageWithData:data]];
            
        });
        
    });
    
}

//hide and display scroll arrow in description

/*************************************************
 *
 * -------- SCROLLVIEW DELEGATE METHODS ----------
 *
 ************************************************/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    float scrollOffset = scrollView.contentOffset.y;
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    
    self.scrollArrow.hidden = YES;
    self.scrollArrowUp.hidden = YES;
    
    if (scrollOffset == 0)
    {
        self.scrollArrow.hidden = NO;
    }
    
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        // then we are at the end
        self.scrollArrowUp.hidden = NO;
        
    }
}


/************************************
 *
 *      COLLECTION VIEW DELEGATE
 *
 ************************************/
//Allows us to cache user images to prevent redownloading over and over
- (UIImage *)imageForUrlString:(NSString*)urlString {
    
    __block UIImage *image = [_cachedImages valueForKey:urlString];
    
    if (!image) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            image = [UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_cachedImages setObject:image forKey:urlString];
                [self.collectionView reloadData];
                
            });
        });
        
    }
    return image;
}



-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row + 1 == _instagramPhotoArray.count){
        
        NSLog(@"Open Instagram");
        //open user in instagram
        NSURL *instagram = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?id=%@",[self.user instagramLinked]]];
        
        NSLog(@"%@", instagram);
        
        if([[UIApplication sharedApplication] canOpenURL:instagram]){
            [[UIApplication sharedApplication] openURL:instagram options:@{} completionHandler:nil];
            
        }else{
            
            NSLog(@"Instagram not installed");
        }
        
    }else{
        
        ImageViewController *imagePopUp = [[ImageViewController alloc] initWithNibName:@"ImageViewController" bundle:nil];
        
        [imagePopUp setInstagramPhotoArray:self.instagramPhotoArray];
        [imagePopUp setCachedImages:self.cachedImages];
        imagePopUp.user = self.user;
        
        
        imagePopUp.view.frame = CGRectMake(0, 0, self.parentViewController.view.frame.size.width, self.parentViewController.view.frame.size.height);
        
        [self presentPopupViewController:imagePopUp animated:YES completion:^(void) {
            
            [imagePopUp.closeBtn addTarget:self action:@selector(dismissImageViewer) forControlEvents:UIControlEventTouchUpInside];
            
            [imagePopUp.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
            
            
            UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissImageViewer)];
            swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
            [imagePopUp.view addGestureRecognizer:swipeDown];
            
        }];
        
    }
    
}


-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    NSInteger count = 0;
    
    if (_instagramPhotoArray){
        
        count = (NSInteger)_instagramPhotoArray.count;
    }
    
    return count;

}



-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    CustomCollectionViewCell *cell = (CustomCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"instagramCollectionCell" forIndexPath:indexPath];
    
    
    if (_instagramPhotoArray){
        
        if (![[_instagramPhotoArray[indexPath.row] valueForKey:@"url"] isEqualToString:@"toInstagram"]){
            
            
            cell.imageView.image = [self imageForUrlString:[_instagramPhotoArray[indexPath.row] valueForKey:@"url"]];
        }else{
            
            cell.imageView.image = [UIImage imageNamed:@"toInstagram"];
        }
        
        
    }
    
    return cell;
}


-(void)dismissImageViewer {
    
    
    if (self.popupViewController != nil) {
        
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            self.popupViewController = nil;
            
        }];
    }
    
}




-(void) startTimer{
    
    if (_timer == nil){
        
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
        
    }else{
        
        [_timer invalidate];
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
}


-(void) refreshTimer{
    
    NSInteger ti = (NSInteger)self.interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    self.boostExpiresLbl.text = [NSString stringWithFormat:@"Boost Time Left: %02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    
    self.interval = self.expireTime - nowMillis;
    
}


-(void) setBoostVisibility:(bool)hidden{
    
    [self.boostExpiresLbl setHidden:hidden];
    [self.boostLbl setHidden:hidden];
}




/**************************************
 *
 * -------- ACTION METHODS ----------
 *
 *************************************/


- (IBAction)closeAction:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:-1 forKey:@"BtnNum"];
    [defaults synchronize];
}

- (IBAction)messageAction:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:-2 forKey:@"BtnNum"];
    [defaults synchronize];
}

- (IBAction)waveAction:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:-5 forKey:@"BtnNum"];
    [defaults synchronize];
    
}


- (IBAction)blockAction:(id)sender {
    

    
    alert=   [UIAlertController
              alertControllerWithTitle:@""
              message:[NSString stringWithFormat:@"Are you sure you want to block %@?", user.firstName]
              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [FirebaseManager setBlockedUser:user.uid];
                             
                             
                             //complete alert
                             UIAlertController *blockedAlert;
                             blockedAlert=   [UIAlertController
                                              alertControllerWithTitle:[NSString stringWithFormat:@"You blocked %@", user.firstName ]
                                              message:@"You can unblock users from Settings"
                                              preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* ok = [UIAlertAction
                                                  actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action)
                                                  {
                                                      [self.closeBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
                                                      
                                                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                      [defaults setInteger:-4 forKey:@"BtnNum"];
                                                      [defaults synchronize];
                                                      
                                                      [blockedAlert dismissViewControllerAnimated:YES completion:nil];
                                                      
                                                      
                                                  }];
                             
                             [blockedAlert addAction:ok];
                             [self presentViewController:blockedAlert animated:YES completion:nil];
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"NO"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 NSLog(@"Cancel button touched!");
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    

}

- (IBAction)flagAction:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:-6 forKey:@"BtnNum"];
    [defaults synchronize];
    
}







@end
