//
//  UserObject.m
//  Complice
//
//  Created by Justin Hershey on 8/31/17.
//  Copyright © 2017 Complice. All rights reserved.
//
//
//

#import "UserObject.h"

@implementation UserObject


-(void)setDefaults{
    
    self.uid =  @"";
    self.profileInfo = @"";
    self.email = @"";
    self.fcmToken = @"";
    self.gender = @"";
    self.facebookId = @"";
    self.imgUrl = @"";
    self.state = @"";
    self.instagramLinked = @"0";
    self.lastName = @"";
    self.firstName = @"";
    self.latitude = @"";
    self.longitude = @"";
    self.subscriptionPurchased = @"0";
    self.city = @"";
    self.deletedDate = @"";
    self.rateDate = @"";
    self.propertyContracted = @"0";
    self.boostedProfile = @"0";
    self.boostExpireTime = @"0";
    self.boostDaysRemaining = @"0";
    self.searchRadius = @"100";
    self.boostRadiusNotification = @"0";
}



-(NSDictionary*)userAsDictionary{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:_uid forKey:@"uid"];
    [dictionary setValue:_profileInfo forKey:@"profileInfo"];
    [dictionary setValue:_email forKey:@"email"];
    [dictionary setValue:_fcmToken forKey:@"fcmToken"];
    [dictionary setValue:_gender forKey:@"gender"];
    [dictionary setValue:_facebookId forKey:@"facebookId"];
    [dictionary setValue:_imgUrl forKey:@"imgUrl"];
    [dictionary setValue:_state forKey:@"state"];
    [dictionary setValue:_instagramLinked forKey:@"instagramLinked"];
    [dictionary setValue:_lastName forKey:@"lastName"];
    [dictionary setValue:_firstName forKey:@"firstName"];
    [dictionary setValue:_subscriptionPurchased forKey:@"subscriptionPurchased"];
    [dictionary setValue:_latitude forKey:@"latitude"];
    [dictionary setValue:_longitude forKey:@"longitude"];
    [dictionary setValue:_city forKey:@"city"];
    [dictionary setObject:_deletedDate forKey:@"deletedDate"];
    [dictionary setObject:_rateDate forKey:@"rateDate"];
    [dictionary setObject:_propertyContracted forKey:@"propertyContracted"];
    [dictionary setObject:_boostedProfile forKey:@"boostedProfile"];
    [dictionary setObject:_boostExpireTime forKey:@"boostExpireTime"];
    [dictionary setObject:_boostDaysRemaining forKey:@"boostDaysRemaining"];
    [dictionary setObject:_searchRadius forKey:@"searchRaduis"];
    [dictionary setObject:_boostRadiusNotification forKey:@"boostRadiusNotification"];
    
    return dictionary;
}




-(void)setUserData:(NSMutableDictionary*)userdict{
    
    self.uid = [userdict valueForKey:@"uid"];
    self.profileInfo = [userdict valueForKey:@"profileInfo"];
    self.email = [userdict valueForKey:@"email"];
    self.fcmToken = [userdict valueForKey:@"fcmToken"];
    self.gender = [userdict valueForKey:@"gender"];
    self.facebookId = [userdict valueForKey:@"facebookId"];
    self.imgUrl = [userdict valueForKey:@"imgUrl"];
    self.state = [userdict valueForKey:@"state"];
    self.instagramLinked = [userdict valueForKey:@"instagramLinked"];
    self.lastName = [userdict valueForKey:@"lastName"];
    self.firstName = [userdict valueForKey:@"firstName"];
    self.subscriptionPurchased = [userdict valueForKey:@"subscriptionPurchased"];
    self.latitude = [userdict valueForKey:@"latitude"];
    self.longitude = [userdict valueForKey:@"longitude"];
    self.city = [userdict valueForKey:@"city"];
    self.deletedDate = [userdict objectForKey:@"deletedDate"];
    self.rateDate = [userdict objectForKey:@"rateDate"];
    self.propertyContracted = [userdict objectForKey:@"propertyContracted"];
    self.boostedProfile = [userdict objectForKey:@"boostedProfile"];
    self.boostExpireTime = [userdict objectForKey:@"boostExpireTime"];
    self.boostDaysRemaining = [userdict objectForKey:@"boostDaysRemaining"];
    self.searchRadius = [userdict objectForKey:@"searchRadius"];
    self.boostRadiusNotification = [userdict objectForKey:@"boostRadiusNotification"];

}




@end
