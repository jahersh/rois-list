//
//  UICollectionViewCell+CustomCollectionViewCell.h
//  Complice
//
//  Created by Justin Hershey on 5/24/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell: UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *captionLbl;

@end
