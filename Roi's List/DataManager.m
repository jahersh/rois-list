//
//  DataManager.m
//  Accomplice
//
//  Created by ibuildx on 3/17/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager


+(UIColor*)getGreenColor{

    return [self colorFromHexString:@"#2BA88F"];
}

+(UIColor*)getLightGreenColor{

//    return [self colorFromHexString:@"#34D0B3"];
    return [self colorFromHexString:@"#40FFDB"];
    
}

UIImage *MultiplyImageByConstantColor( UIImage *image, UIColor *color ) {
    
    CGSize backgroundSize = image.size;
    UIGraphicsBeginImageContext(backgroundSize);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGRect backgroundRect;
    backgroundRect.size = backgroundSize;
    backgroundRect.origin.x = 0;
    backgroundRect.origin.y = 0;
    
    CGFloat r,g,b,a;
    [color getRed:&r green:&g blue:&b alpha:&a];
    CGContextSetRGBFillColor(ctx, r, g, b, a);
    CGContextFillRect(ctx, backgroundRect);
    
    CGRect imageRect;
    imageRect.size = image.size;
    imageRect.origin.x = (backgroundSize.width - image.size.width)/2;
    imageRect.origin.y = (backgroundSize.height - image.size.height)/2;
    
    // Unflip the image
    CGContextTranslateCTM(ctx, 0, backgroundSize.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    CGContextDrawImage(ctx, imageRect, image.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}


+ (NSString*)ageCalculate:(NSString*)birthday
{
    
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    int time = [todayDate timeIntervalSinceDate:[dateFormatter dateFromString:birthday]];
    int allDays = (((time/60)/60)/24);
    int days = allDays%365;
    int years = (allDays-days)/365;
    
    NSLog(@"You live since %i years and %i days",years,days);
    
    return  [NSString stringWithFormat:@"%i",years];
    
}



+(double) milesToKilometers:(double)miles{
    
    double kilometers = miles * 1.60934;
    return kilometers;
}



+(double) kilometersToMiles:(double)kilometers{
    double miles = kilometers * 0.621371;
    return miles;
}



+ (double)convertToMillisSince1970:(NSDate*)date{
    
    double millis = [date timeIntervalSince1970];
    return millis;
}


+ (NSString*)convertDoubleToString:(double)dateString{
    
    NSString *millis = [NSString stringWithFormat:@"%f", dateString];
    return millis;
}



+ (void)RoundedStyleForBtn:(UIButton *)btn
{
    btn.layer.cornerRadius = btn.frame.size.width / 2;
    btn.clipsToBounds = YES;
    btn.layer.borderWidth = 2.0f;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
}


+ (void)RoundedStyleForImage:(UIImageView *)img {
    img.layer.cornerRadius = img.frame.size.width / 2;
    img.clipsToBounds = YES;
    img.layer.borderWidth = 2.0f;
    img.layer.borderColor = [UIColor whiteColor].CGColor;
}


+ (void)locationIntegration :(CLLocationManager *)locationManager {
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}


+(float)getDistanceBetweenUser :(NSDictionary *)userOne andOtherUser :(NSDictionary *)userTwo{
    
    UserObject *user1 = [[UserObject alloc] init];
    UserObject *user2 = [[UserObject alloc] init];
    
    [user2 setUserData:userTwo.mutableCopy];
    [user1 setUserData:userOne.mutableCopy];
    
    NSString *lat1Str = user1.latitude;
    NSString *long1Str = user1.longitude;
    
    if ([user1.longitude isEqualToString:@""]) {
        long1Str = @"0";
    }
    
    CLLocationDegrees lat1 = [lat1Str doubleValue];
    CLLocationDegrees long1 = [long1Str doubleValue];
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:lat1 longitude:long1];
    NSString *lat2Str = user2.latitude;
    NSString *long2Str = user2.longitude;
    CLLocationDegrees lat2 = [lat2Str doubleValue];
    CLLocationDegrees long2 = [long2Str doubleValue];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:lat2 longitude:long2];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    float distanceMiles = (distance/1000) * 0.621371 ;
    distanceMiles= [[NSString stringWithFormat:@"%.2f",distanceMiles]floatValue];

    return distanceMiles;
}


+(void) getAddressFromLatLon:(CLLocation *)bestLocation completion:(void (^)(CLPlacemark* placemark))completion
{
    NSLog(@"%f %f", bestLocation.coordinate.latitude, bestLocation.coordinate.longitude);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
         NSLog(@"locality %@",placemark.locality);
         NSLog(@"postalCode %@",placemark.postalCode);
         completion(placemark);
         
     }];
    
}



+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (NSString *)getFirstNameFromName :(NSString *)name {
    NSArray* firstLastStrings = [name componentsSeparatedByString:@" "];
    NSString* firstName = [firstLastStrings objectAtIndex:0];
    return firstName;
}



+(NSString *)dictionaryToJSON:(NSDictionary*)dictionary{
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dictionary options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    return myString;
}

+(NSDictionary *)jsonToDictionary:(NSString*)string{
    
    NSError * err;
    NSData *data =[string dataUsingEncoding:NSUTF8StringEncoding];
    
    //could return nil
    NSDictionary *response = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
    
    return response;
}


+(NSString*)dateToString:(NSDate*)date{
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+(NSDate*)stringToDate:(NSString*)dateString{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
    NSDate *myDate = [df dateFromString: dateString];
    return myDate;
}





@end
