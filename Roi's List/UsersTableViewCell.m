//
//  UsersTableViewCell.m
//  Accomplice
//
//  Created by ibuildx on 3/9/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "UsersTableViewCell.h"
#import "DataManager.h"

@implementation UsersTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self setupBtns];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupBtns{
    

    //adding drop shadows
    self.userName.clipsToBounds = NO;
    self.userName.layer.shadowColor = [UIColor blackColor].CGColor;
    self.userName.layer.shadowOpacity = 0.4;
    self.userName.layer.shadowRadius = 8;
    self.userName.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    self.cityLbl.clipsToBounds = NO;
    self.cityLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.cityLbl.layer.shadowOpacity = 0.4;
    self.cityLbl.layer.shadowRadius = 8;
    self.cityLbl.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    self.stateLbl.clipsToBounds = NO;
    self.stateLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.stateLbl.layer.shadowOpacity = 0.4;
    self.stateLbl.layer.shadowRadius = 8;
    self.stateLbl.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    self.waveBtn.clipsToBounds = NO;
    self.waveBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.waveBtn.layer.shadowOpacity = 0.4;
    self.waveBtn.layer.shadowRadius = 8;
    self.waveBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.messageBtn.clipsToBounds = NO;
    self.messageBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.messageBtn.layer.shadowOpacity = 0.4;
    self.messageBtn.layer.shadowRadius = 8;
    self.messageBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.userImageView.layer.borderColor = [DataManager getGreenColor].CGColor;
    self.userImageView.layer.borderWidth = 1;
    
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    self.shadowView.layer.masksToBounds = NO;

    
}




@end


