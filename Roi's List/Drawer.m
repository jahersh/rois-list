//
//  Drawer.m
//  Complice
//
//  Created by Justin Hershey on 1/12/17.
//  Copyright © 2017 Complice. All rights reserved.
//


/*MMDrawerController class that implements the ChatProtocol so we don't need to worry about passing the Chat object and delegate between the drawers when opened and closed.
 
*/

#import "Drawer.h"
#import <TSMessage.h>
#import "MyProfilePopUp.h"
#import "MyInstagramProfilePopUp.h"
#import "AppDelegate.h"
#import "InboxProfPopup.h"
#import "FlagViewController.h"
#import <SVProgressHUD.h>
#import "EditProfileViewController.h"

@interface Drawer ()

@end

@implementation Drawer


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.currentUser = [self.chat currentUser];
    
    _chat.delegate = self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    
    return UIStatusBarStyleDefault;
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    _chat.delegate = self;
    
    if(_gotoChatView){
        
        self.gotoChatView = NO;
        [self performSelector:@selector(showAnotherViewController) withObject:nil afterDelay:0.0];
        
    }
}




-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]  removeObserver:self];
}


//must set showTSMessage to NO when app enters the background or all notifications will show after opening the app after a
//push notification has been received

-(void)applicationDidEnterBackground:(NSNotification *)notification{
    
    _chat.showTSMessage = NO;
}



/***********************************************
 *
 * --------- HUB MANIPULATION METHODS ----------
 *
 ***********************************************/

- (int) getHubCount {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    return  [(SearchViewController*)appDelegate.drawerController.centerViewController getHubCount];;
    
}

-(void) decrementHubCount {
    
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [(SearchViewController*)appDelegate.drawerController.centerViewController decrementHubCount];
    
}

-(void) incrementHubCount {
    
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [(SearchViewController*)appDelegate.drawerController.centerViewController incrementHubCount];
    
}

-(void) updateInboxView:(NSString*)convoId fromUser:(NSString*)fromUser {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [(InboxViewController*)appDelegate.drawerController.rightDrawerViewController replaceWithMostRecentMsg:convoId userId:fromUser];
    
}


//Will get selected users badge count and return an incremented number to be sent in the push notification
-(ChatObject*)getChat{
    
    return self.chat;
}






/**********************************************************
 *
 * ---------- CHATOBJECT DELEGATE METHOD -----------
 *
 **********************************************************/


-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if(appDelegate.openSide == MMDrawerSideRight){
        
        [(InboxViewController*)appDelegate.drawerController.rightDrawerViewController replaceWithMostRecentMsg:conversationId userId:fromUser];
        
    }
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    

}




//display the DemoMessagesViewController
- (void)showAnotherViewController{
    
    [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
    
}





/**********************************************************
 *
 * ----------------- NAVIGATION METHODS ------------------
 *
 **********************************************************/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"Segue in DrawerController: %@", segue.identifier);
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"SearchView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;

    }else if([segue.identifier isEqualToString:@"toFlagUserView"]){
        
        FlagViewController *fvc = segue.destinationViewController;
        
        fvc.senderView = @"list";

        fvc.currentUser = self.chat.currentUser;
        fvc.selectedUser = self.selectedUser;
        
        [fvc setChat:_chat];
        _chat.returned = appDelegate.drawerController;
        _chat.delegate = fvc;
        
        NSLog(@"UserName: %@", fvc.selectedUser.firstName);
        
    }else if([segue.identifier isEqualToString:@"EditProfileSegue"]){
        
        EditProfileViewController *messageView = segue.destinationViewController;
        
        [messageView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = messageView;

    }
}




-(void)showProfile{

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    UIView *backgroundLayer = [[UIView alloc] initWithFrame:self.view.bounds];
    backgroundLayer.backgroundColor = [UIColor colorWithWhite:0.0 alpha:.7];
    
   

    //disable drawer gestions and viewcontrollers otherwise touching outside the popup can navigate the app and weird things happen
    appDelegate.drawerController.centerViewController.view.userInteractionEnabled = NO;
    appDelegate.drawerController.leftDrawerViewController.view.userInteractionEnabled = NO;
    appDelegate.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
    appDelegate.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeNone;
    
    
    
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"INSTA_ACCESS_TOKEN"]){
        
        MyInstagramProfilePopUp *profilePopUp = [[MyInstagramProfilePopUp alloc] initWithNibName:@"MyInstagramProfilePopUp" bundle:nil];
        
        profilePopUp.user = [self.chat currentUser];
        
        profilePopUp.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width *.8, self.view.frame.size.height *.9);
        
        [self presentPopupViewController:profilePopUp animated:YES completion:^(void) {
            
            
            [profilePopUp.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            
            UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPopup)];
            swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
            [profilePopUp.view addGestureRecognizer:swipeDown];
            
        }];
        
        
    }else{
        
        MyProfilePopUp *profilePopUp = [[MyProfilePopUp alloc] initWithNibName:@"MyProfilePopUp" bundle:nil];
        
        profilePopUp.user = [self.chat currentUser];
        
        profilePopUp.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width *.8, self.view.frame.size.height *.75);
        
        [self presentPopupViewController:profilePopUp animated:YES completion:^(void) {
            
            [profilePopUp.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            
            UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPopup)];
            swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
            [profilePopUp.view addGestureRecognizer:swipeDown];
            
        }];
    }
    
}



-(void) dismissPopup{
    if (self.popupViewController != nil) {
        
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            self.popupViewController = nil;
            
            //re-enable drawer interaction and gestures
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            appDelegate.drawerController.centerViewController.view.userInteractionEnabled = YES;
            appDelegate.drawerController.leftDrawerViewController.view.userInteractionEnabled = YES;
            appDelegate.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
            appDelegate.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;

            NSLog(@"Popup Dismissed");
            
        }];
    }
}




@end
