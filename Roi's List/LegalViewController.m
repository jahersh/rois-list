//
//  UIViewController+LegalViewController.m
//  Complice
//
//  Created by Justin Hershey on 10/30/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import "LegalViewController.h"
#import "TermsViewController.h"
#import <TSMessage.h>
#import "SettingViewController.h"

@implementation LegalViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];


    [self setupView];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationIsActive:)
//                                                 name:UIApplicationDidBecomeActiveNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationDidEnterBackground:)
//                                                 name:UIApplicationDidEnterBackgroundNotification
//                                               object:nil];
    
    
}



-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}

//-(void)applicationDidEnterBackground:(NSNotification *)notification{
//    
//    _chat.showTSMessage = NO;
//}
//
//- (void)applicationIsActive:(NSNotification *)notification {
//    NSLog(@"Application Did Become Active");
//    if(!notification){
//        _chat.showTSMessage = YES;
//    }
//}


/*************************************************
 *
 * ---------- ACTION METHODS ------------
 *
 *************************************************/

- (IBAction)privacyAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toPrivacyView" sender:nil];
}

- (IBAction)termsAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toTermsView" sender:nil];
}

- (IBAction)usageAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toUsageView" sender:nil];
}

- (IBAction)backToSettingsAction:(id)sender {
    [self performSegueWithIdentifier:@"toSettingsView" sender:nil];
}



/*************************************************
 *
 * ---------- UI SETUP METHODS ------------
 *
 *************************************************/

-(void) setupView{
    
    self.termsBtn.layer.cornerRadius = 10;
    self.usageBtn.layer.cornerRadius = 10;
    self.privacyBtn.layer.cornerRadius = 10;
    
    //button/image drop shadows
    self.privacyBtn.clipsToBounds = NO;
    self.privacyBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.privacyBtn.layer.shadowOpacity = 0.4;
    self.privacyBtn.layer.shadowRadius = 8;
    self.privacyBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.privacyBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.privacyBtn.titleLabel.layer.shadowRadius = 5;
    self.privacyBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.privacyBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    
    self.usageBtn.clipsToBounds = NO;
    self.usageBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.usageBtn.layer.shadowOpacity = 0.4;
    self.usageBtn.layer.shadowRadius = 8;
    self.usageBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.usageBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.usageBtn.titleLabel.layer.shadowRadius = 5;
    self.usageBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.usageBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    
    self.termsBtn.clipsToBounds = NO;
    self.termsBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.termsBtn.layer.shadowOpacity = 0.4;
    self.termsBtn.layer.shadowRadius = 8;
    self.termsBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.termsBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.termsBtn.titleLabel.layer.shadowRadius = 5;
    self.termsBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.termsBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
}



/*************************************************
 *
 * ---------- NAVIGATION METHODS ------------
 *
 *************************************************/
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
        

    if ([segue.identifier isEqualToString:@"toPrivacyView"]) {
        TermsViewController *vc = segue.destinationViewController;
        self.legalView = @"privacy";
        vc.termsOrPrivacyText = @"Privacy Policy Text" ;
        vc.legalView = self.legalView ;
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;

    }
    else if([segue.identifier isEqualToString:@"toTermsView"])
    {
        TermsViewController *vc = segue.destinationViewController;
        self.legalView = @"terms";
        vc.termsOrPrivacyText = @"Terms And Condition Text" ;
        vc.legalView = self.legalView ;
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;
    }
    
    else if([segue.identifier isEqualToString:@"toUsageView"])
    {
        TermsViewController *vc = segue.destinationViewController;
        self.legalView = @"usage";
        vc.termsOrPrivacyText = @"Usage Policy Text";
        vc.legalView = self.legalView ;
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;
    }
    else if([segue.identifier isEqualToString:@"backToSettings"])
    {
        NSLog(@"PASSING CHAT DELEGATE TO SETTINGS");
        SettingViewController *settingsView = segue.destinationViewController;
        
        [settingsView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = settingsView;
        
    }else if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"LegalView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;
        
        
    }
    
    _chat = nil;

}




// Get the new view controller using [segue destinationViewController].
// Pass the selected object to the new view controller.
    

- (IBAction)unwindToLegal:(UIStoryboardSegue *)unwindSegue{
}



/*************************************************
 *
 * ---------- CHATOBJECT DELEGATE METHODS ------------
 *
 *************************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}

@end




