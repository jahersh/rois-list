//
//  BlockUser.h
//  Accomplice
//
//  Created by ibuildx on 5/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlockUser : NSObject

@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *CurrentUserId;
@property (nonatomic, strong) NSString *nBlockUserId;


-(void) dictionaryToBlockUser:(NSDictionary*)userdict;
-(NSDictionary*) blockUserToDictionary;
@end
