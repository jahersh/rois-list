//
//  ImageViewController.h
//  Complice
//
//  Created by Justin Hershey on 6/9/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+CWPopup.h"
#import "CaptionsCollectionViewCell.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "UserObject.h"

@interface ImageViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>


@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property NSMutableDictionary *cachedImages;
@property NSArray<NSDictionary*> *instagramPhotoArray;


@property UserObject *user;
@end
