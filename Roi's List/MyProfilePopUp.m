//
//  myProfilePopUp.m
//  Complice
//
//  Created by Justin Hershey on 2/23/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import "MyProfilePopUp.h"
#import "ProfilePopUp.h"
#import "BlockUser.h"
#import "DataManager.h"
#import "Inbox.h"
#import "FlagViewController.h"
#import "SearchViewController.h"
#import "InstagramManager.h"

@interface MyProfilePopUp ()

@end

@implementation MyProfilePopUp
@synthesize user;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _scrollView.delegate = self;
    
    NSString *tempText = user.profileInfo; //@" Profile description";
    
    self.discText.text = tempText;
    self.discText.delegate = self;
    self.nameText.text = [NSString stringWithFormat:@"%@", user.firstName];
    
    CGSize textViewSize = [self.discText sizeThatFits:CGSizeMake(self.discText.frame.size.width, FLT_MAX)];
    
    if(textViewSize.height > self.scrollView.frame.size.height){
        
        self.discText.text = [tempText stringByAppendingString:@"\n\n\n"];
        textViewSize = [self.discText sizeThatFits:CGSizeMake(self.discText.frame.size.width, FLT_MAX)];
    }
    
    self.textViewHeight.constant = textViewSize.height;
    self.nameDistanceText.text = [NSString stringWithFormat:@"%@, %@", user.city, user.state];

    [self setupView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadViewIfNeeded];
}





/*************************************
 *
 * -------- UI SETUP METHODS --------
 *
 *************************************/

-(void)viewDidLayoutSubviews{
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.view.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii:(CGSize){15.0, 15.}].CGPath;
    
    self.view.layer.mask = maskLayer;
    
    self.prfilePic.clipsToBounds = YES;
    self.prfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.prfilePic.layer.borderWidth = self.prfilePic.frame.size.width / 100;
    self.prfilePic.layer.cornerRadius = self.prfilePic.frame.size.width /2 ;
    
    CGFloat fixedWidth = self.discText.frame.size.width;
    CGSize textViewSize = [self.discText sizeThatFits:CGSizeMake(fixedWidth, CGFLOAT_MAX)];
    
    self.textViewHeight.constant = textViewSize.height + .4 * textViewSize.height;
    
    CGRect newFrame = _discText.frame;
    newFrame.size = CGSizeMake(fmaxf(textViewSize.width, fixedWidth), textViewSize.height);
    _discText.frame = newFrame;
    
    if(textViewSize.height <= _scrollView.frame.size.height){
        
        [self.scrollArrow setHidden:YES];
    }
}



-(void)setupView{
    
    NSLog(@" textView size %f", self.scrollView.contentSize.height);
    NSLog(@" scrollview size %f", self.scrollView.frame.size.height);
    
    if (self.textViewHeight.constant - 10 <= self.scrollView.frame.size.height) {
        self.scrollArrow.hidden = YES;
    }

    self.infoView.layer.cornerRadius = 15;
    self.scrollArrowUp.hidden = YES;
    
    self.propertyContractedLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.propertyContractedLbl.layer.shadowOpacity = 0.4;
    self.propertyContractedLbl.layer.shadowRadius = 6;
    self.propertyContractedLbl.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    self.propertyContractedLbl.backgroundColor = [UIColor orangeColor];
    self.propertyContractedLbl.layer.cornerRadius = 10.0;
    self.propertyContractedLbl.text = @"Pending Contract";
    
    if ([self.user.propertyContracted isEqualToString:@"1"]){
        self.propertyContractedLbl.hidden = NO;
    }else{
        self.propertyContractedLbl.hidden = YES;
    }
    
    double sExpireTime = [self.user.boostExpireTime doubleValue];
    NSString *profileBoosted = user.boostedProfile;
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    
    if ([profileBoosted isEqualToString: @"1"] && (nowMillis < sExpireTime)){
        
        [self setBoostVisibility:NO];
        
        if(sExpireTime > nowMillis){
            
            self.interval = sExpireTime - nowMillis;
            self.expireTime = sExpireTime;
            
            [self startTimer];
        }
    }else{
        [self setBoostVisibility:YES];
    }
    
    
    self.boostLbl.clipsToBounds = NO;
    self.boostLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostLbl.layer.shadowOpacity = 0.4;
    self.boostLbl.layer.shadowRadius = 3;
    self.boostLbl.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.boostExpiresLbl.clipsToBounds = NO;
    self.boostExpiresLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostExpiresLbl.layer.shadowOpacity = 0.4;
    self.boostExpiresLbl.layer.shadowRadius = 3;
    self.boostExpiresLbl.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.nameDistanceText.clipsToBounds = NO;
    self.nameDistanceText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.nameDistanceText.layer.shadowOpacity = 0.4;
    self.nameDistanceText.layer.shadowRadius = 3;
    self.nameDistanceText.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.genderAgeText.clipsToBounds = NO;
    self.genderAgeText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.genderAgeText.layer.shadowOpacity = 0.4;
    self.genderAgeText.layer.shadowRadius = 6;
    self.genderAgeText.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.nameText.clipsToBounds = NO;
    self.nameText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.nameText.layer.shadowOpacity = 0.4;
    self.nameText.layer.shadowRadius = 8;
    self.nameText.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.scrollArrow.clipsToBounds = NO;
    self.scrollArrow.layer.shadowColor = [UIColor blackColor].CGColor;
    self.scrollArrow.layer.shadowOpacity = 0.4;
    self.scrollArrow.layer.shadowRadius = 3;
    self.scrollArrow.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.scrollArrowUp.clipsToBounds = NO;
    self.scrollArrowUp.layer.shadowColor = [UIColor blackColor].CGColor;
    self.scrollArrowUp.layer.shadowOpacity = 0.4;
    self.scrollArrowUp.layer.shadowRadius = 3;
    self.scrollArrowUp.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.closeBtn.clipsToBounds = NO;
    self.closeBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.closeBtn.layer.shadowOpacity = 0.4;
    self.closeBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);

    
    //Setup  prfile pic
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = user.imgUrl;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.prfilePic setImage:[UIImage imageWithData:data]];
            
        });
    });
}





-(void) startTimer{
    
    if (_timer == nil){
        
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
        
    }else{
        
        [_timer invalidate];
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
}


-(void) refreshTimer{
    
    NSInteger ti = (NSInteger)self.interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    self.boostExpiresLbl.text = [NSString stringWithFormat:@"Boost Time Left: %02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    
    self.interval = self.expireTime - nowMillis;
    
}


-(void) setBoostVisibility:(bool)hidden{
    
    [self.boostExpiresLbl setHidden:hidden];
    [self.boostLbl setHidden:hidden];
}








/*************************************
 *
 * --- SCROLLVIEW DELEGATE METHODS ---
 *
 *************************************/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    float scrollOffset = scrollView.contentOffset.y;
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    
    //NSLog(@"THE THING IS SCROLLING");
    self.scrollArrow.hidden = YES;
    self.scrollArrowUp.hidden = YES;
    
    if (scrollOffset == 0)
    {
        self.scrollArrow.hidden = NO;
    }
    
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        // then we are at the end
        self.scrollArrowUp.hidden = NO;
    }
}


@end
