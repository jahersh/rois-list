//
//  EditProfileView.h
//  LiveChatWithNearest
//
//  Created by ibuildx on 2/29/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoMessagesViewController.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "FirebaseManager.h"



@interface EditProfileViewController : UIViewController <UIImagePickerControllerDelegate , UINavigationControllerDelegate, ChatProtocol, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;
- (IBAction)saveAction:(id)sender;

//@property (strong, nonatomic) DownPicker *downPicker;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@property id fbObject;
- (IBAction)backAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;

@property (strong) NSArray  *usersFound;

@property (strong) UserObject *currentUser;
@property (strong) UserObject *selectedUser;
@property UIImagePickerController *picker;
@property NSString * userId;
- (IBAction)uploadPhotoAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *uploadPhotoBtn;


@property (weak, nonatomic) IBOutlet UILabel *profileInfo;
@property (weak, nonatomic) IBOutlet UITextField *nameText;

//@property (strong, nonatomic) IBOutlet UITextField *nickNameText;

@property (weak, nonatomic) IBOutlet UITextView *profileInfoText;

@property (weak, nonatomic) IBOutlet UILabel *nameAgeText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomAlignImageConstraint;

@property (weak, nonatomic) IBOutlet UIButton *saveProfileBtn;
@property (weak, nonatomic) IBOutlet UILabel *genderText;


@property (nonatomic, strong) ChatObject *chat;

@end
