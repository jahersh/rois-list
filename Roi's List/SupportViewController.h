//
//  SupportViewController.h
//  Accomplice
//
//  Created by ibuildx on 3/18/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoMessagesViewController.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>

@interface SupportViewController : UIViewController <ChatProtocol>

@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;


@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@property (weak, nonatomic) IBOutlet UIButton *legalBtn;

@property (strong) NSMutableArray  *usersFound;

@property (strong) UserObject *currentUser;

@property (strong) UserObject *selectedUser;

@property (weak, nonatomic) IBOutlet UIButton *faqBtn;

- (IBAction)backBtnAction:(id)sender;

- (IBAction)sendAction:(id)sender;

- (IBAction)legalAction:(id)sender;


@property (weak) NSString * userId;
@property (weak, nonatomic) IBOutlet UITextView *supportMessageTestView;

@property (nonatomic, strong) ChatObject *chat;

@end
