//
//  TermsViewController.h
//  Accomplice
//
//  Created by Muhammad Saqib Yaqeen on 5/7/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatObject.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "FirebaseManager.h"

@interface TermsViewController : UIViewController <ChatProtocol, UIScrollViewDelegate, UITextViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;
@property (weak, nonatomic) IBOutlet UILabel *userNameText;

@property (weak, nonatomic) IBOutlet UIWebView *webview;

@property (strong) UserObject *selectedUser;
@property (weak) NSString *termsOrPrivacyText;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;


//@property (weak, nonatomic) IBOutlet UITextView *privacyOrTermsTextField;
@property NSString *legalView;

@property (nonatomic, strong) ChatObject *chat;
@property (strong) UserObject *currentUser;

- (IBAction)backBtnAction:(id)sender;

@end
