//
//  UIViewController+FlagViewController.h
//  Complice
//
//  Created by Justin Hershey on 10/31/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatObject.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "UserObject.h"
#import "FirebaseManager.h"

@interface FlagViewController: UIViewController<UITextViewDelegate, ChatProtocol>


@property (weak, nonatomic) IBOutlet UITextView *flagUserText;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

//Actions

- (IBAction)submitFlagAction:(id)sender;
- (IBAction)backAction:(id)sender;

//Values
@property (strong) UserObject *currentUser;
@property (weak) NSString *senderView;

@property (strong) UserObject *selectedUser;

@property (weak) UIAlertController *alert;

@property (nonatomic, strong) ChatObject *chat;

@end
