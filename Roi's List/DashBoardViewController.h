//
//  ViewController.h
//  LiveChatWithNearest
//
//  Created by ibuildx on 2/26/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoMessagesViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>

@interface DashBoardViewController : UIViewController <JSQDemoViewControllerDelegate, ChatProtocol>
//@interface DashBoardViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *background;

@property id fbObject;



@property (strong) UserObject *currentUser;



@property (weak, nonatomic) IBOutlet UIView *shadowView;


@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;
@property (weak, nonatomic) IBOutlet UILabel *userNameText;
@property (weak, nonatomic) IBOutlet UIButton *listBtn;
@property (weak, nonatomic) IBOutlet UIButton *editProfileBtn;
@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;
@property (weak, nonatomic) IBOutlet UIButton *rateBtn;

//@property (strong, nonatomic) IBOutlet UIButton *emailBtn;
@property (weak, nonatomic) IBOutlet UIButton *settingsBtn;
@property (weak, nonatomic) IBOutlet UIButton *supportBtn;

- (IBAction)inviteAction:(id)sender;
@property (weak, nonatomic) IBOutlet GADBannerView *adView;
@property (weak, nonatomic) IBOutlet UIButton *showProfileBtn;
@property (weak, nonatomic) IBOutlet UIButton *boostBtn;


- (IBAction)listBtnAction:(id)sender;
- (IBAction)rateAction:(id)sender;
- (IBAction)showProfileAction:(id)sender;

- (IBAction)settingAction:(id)sender;
- (IBAction)supportAction:(id)sender;
- (IBAction)editProfileAction:(id)sender;

@property NSString * userId;

@property (strong) UserObject *selectedUser;

@property (nonatomic, strong) ChatObject *chat;


@end

