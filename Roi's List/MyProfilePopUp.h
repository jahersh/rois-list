//
//  myProfilePopUp.h
//  Complice
//
//  Created by Justin Hershey on 2/23/17.
//  Copyright © 2017 Complice. All rights reserved.
//

//
//  ProfilePopUp.h
//  Complice
//
//  Created by ibuildx on 5/2/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "UserObject.h"


@interface MyProfilePopUp : UIViewController <UITextViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameText;
@property (weak, nonatomic) IBOutlet UILabel *nameDistanceText;
@property (weak, nonatomic) IBOutlet UILabel *genderAgeText;


@property (weak, nonatomic) IBOutlet UITextView *discText;
@property (weak, nonatomic) IBOutlet UIImageView *prfilePic;
@property (weak, nonatomic) IBOutlet UIImageView *scrollArrowUp;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeight;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *profilePicBtn;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *scrollArrow;
@property NSString *profileImg;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *propertyContractedLbl;
@property (weak, nonatomic) IBOutlet UILabel *boostExpiresLbl;
@property (weak, nonatomic) IBOutlet UILabel *boostLbl;

@property NSTimeInterval expireTime;
@property NSTimeInterval interval;
@property NSTimer *timer;
@property UserObject *user;


@end

