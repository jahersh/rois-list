//
//  StartView.h
//  Accomplice
//
//  Created by ibuildx on 3/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EAIntroView.h>
#import <MMDrawerController.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "UserObject.h"

@interface LoginViewController : UIViewController< EAIntroDelegate>

@property (weak, nonatomic) IBOutlet UIView *background;

@property (weak, nonatomic) IBOutlet UIButton *fbLoginBtn;
@property (weak, nonatomic) IBOutlet UILabel *noPostLbl;

@property (weak, nonatomic) UserObject *selectedUser;
@property (weak, nonatomic) UserObject *currentUser;

@property FIRDatabaseReference *ref;

@property (weak, nonatomic) IBOutlet UIImageView *lockImage;
@property (weak, nonatomic) IBOutlet UIImageView *fbImage;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;

@property bool gotoChatView;

@property (weak, nonatomic) IBOutlet UserObject *notificationUser;

@end
