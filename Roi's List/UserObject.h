//
//  UserObject.h
//  Complice
//
//  Created by Justin Hershey on 8/31/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"


@interface UserObject : NSObject


@property (strong, nonatomic) NSString *uid;
@property (strong, nonatomic) NSString *profileInfo;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *fcmToken;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *facebookId;
@property (strong, nonatomic) NSString *imgUrl;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *instagramLinked;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *subscriptionPurchased;
@property (strong, nonatomic) NSString *propertyContracted;
@property (strong, nonatomic) NSString *deletedDate;
@property (strong, nonatomic) NSString *rateDate;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *boostedProfile;
@property (strong, nonatomic) NSString *boostExpireTime;
@property (strong, nonatomic) NSString *boostDaysRemaining;
@property (strong, nonatomic) NSString *searchRadius;
@property (strong, nonatomic) NSString *boostRadiusNotification;

-(NSDictionary*)userAsDictionary;
-(void)setUserData:(NSMutableDictionary*)userdict;
-(void)setDefaults;


@end
