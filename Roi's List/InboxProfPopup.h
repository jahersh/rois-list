//
//  inboxProfPopup.h
//  Complice
//
//  Created by Justin Hershey on 3/2/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "UserObject.h"

//Properties

@interface InboxProfPopup : UIViewController <UITextViewDelegate, UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discTextHeight;


@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextView *discText;

@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *displayName;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIButton *blockBtn;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;

@property (weak, nonatomic) IBOutlet UIButton *flagBtn;
@property (weak, nonatomic) IBOutlet UILabel *genderAgeText;

@property (weak, nonatomic) IBOutlet UIImageView *downArrow;
@property (weak, nonatomic) IBOutlet UIImageView *upArrow;
@property (weak, nonatomic) IBOutlet UILabel *propertyContractedLbl;
@property (weak, nonatomic) IBOutlet UILabel *boostExpiresLbl;
@property (weak, nonatomic) IBOutlet UILabel *boostLbl;

@property NSTimeInterval expireTime;
@property NSTimeInterval interval;
@property NSTimer *timer;
@property UserObject *user;

//Actions
- (IBAction)flagAction:(id)sender;

- (IBAction)closeAction:(id)sender;

- (IBAction)blockAction:(id)sender;


@end
