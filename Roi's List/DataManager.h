//
//  DataManager.h
//  Accomplice
//
//  Created by ibuildx on 3/17/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DataManager.h"
#import "UserObject.h"


@interface DataManager : NSObject


+(UIColor*)getGreenColor;
+(UIColor*)getLightGreenColor;
UIImage *MultiplyImageByConstantColor( UIImage *image, UIColor *color );

+ (NSString*)ageCalculate:(NSString*)birthday;
+ (void)RoundedStyleForBtn:(UIButton *)btn;
+ (void)RoundedStyleForImage:(UIImageView *)img;
+ (void)locationIntegration :(CLLocationManager *)locationManager;

+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (NSString *)getFirstNameFromName :(NSString *)name;

+ (double)convertToMillisSince1970: (NSDate*)date;
+ (NSString*)convertDoubleToString:(double)dateString;

+(NSString *)dictionaryToJSON:(NSDictionary*)dictionary;
+(NSDictionary *)jsonToDictionary:(NSString*)string;

+(NSString*)dateToString:(NSDate*)date;
+(NSDate*)stringToDate:(NSDate*)dateString;

+(double) milesToKilometers:(double)miles;
+(double) kilometersToMiles:(double)kilometers;
+(float)getDistanceBetweenUser : (NSDictionary *) user1 andOtherUser : (NSDictionary *) user2;
+ (void) getAddressFromLatLon:(CLLocation *)bestLocation completion:(void (^)(CLPlacemark* placemark))completion;

@end
