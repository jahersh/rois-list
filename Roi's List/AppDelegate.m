//
//  AppDelegateNew.m
//  Complice
//
//  Created by Justin Hershey on 6/3/17.
//  Copyright © 2017 Complice. All rights reserved.
//


#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <SVProgressHUD.h>
#import "DataManager.h"
#import "SearchViewController.h"
#import "DemoMessagesViewController.h"
#import "ChatObject.h"
#import "LoginViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "DashBoardViewController.h"
#import "InboxViewController.h"
#import "NetworkCheck.h"
#import <TSMessage.h>
#import <Instabug/Instabug.h>


@interface AppDelegate ()


@end



@implementation AppDelegate {
    
    UserObject *user;
    NSDictionary *notification;
    NSNotification *selected;
    BOOL notificationOpenedApp;
    
    
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSSetUncaughtExceptionHandler(&myExceptionHandler);
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    
    //add notification selected observer, observation is attatched to the ChatObject delegate passed between Views
    selected = [NSNotification notificationWithName:@"notificationSelected" object:self];
    notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];

    [GADMobileAds configureWithApplicationID:@"ca-app-pub-6416144291405618~5914988089"];

    [FIRApp configure];
    
    
    

    
    //FIREBASE NOTIFICATIONS
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        [FIRMessaging messaging].delegate = self;
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [Instabug startWithToken:@"e53cece337d38baa1eddb6aa7e88284c" invocationEvent:IBGInvocationEventShake];
    
    BOOL fb = [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    
    /**************************
     *
     * NETWORK NOTIFIER START
     *
     **************************/
    
    
    // Allocate a reachability object
    _reach = [NetworkCheck reachabilityWithHostname:@"www.google.com"];
    
    // Here we set up a NSNotification observer. The Reachability that caused the notification
    //     is passed in the object parameter
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [_reach startNotifier];
    
//    if (notification){
//     
//        bool loggedIn = false;
//        if ([[FIRAuth auth] currentUser] != nil){
//            loggedIn = true;
//        }
//        
//        [self showMainView:<#(NSString *)#> loggedIn:loggedIn];
//    }
    
    return fb;
}




void myExceptionHandler(NSException *exception)
{
    NSArray *stack = [exception callStackReturnAddresses];
    NSLog(@"Stack trace: %@", stack);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSDKAppEvents activateApp];
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}


- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSLog(@"FCM registration token: %@", fcmToken);
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:fcmToken forKey:@"fcmToken"];
    [defaults synchronize];
    

}


-(void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage{
    
    
    NSLog(@"Application Received Remote Message %@", remoteMessage);
}


//FIREBASE NOTIFICATION RECEIVED METHODS

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.'
    
//    print(userInfo)
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    
    NSString *senderId = [userInfo valueForKey:@"gcm.notification.sender"];
    NSString *conversationId = [userInfo valueForKey:@"gcm.notification.convoId"];
    NSString *type = [userInfo valueForKey:@"gcm.notification.type"];
    NSString *msg = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
    NSString *title = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"title"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:senderId forKey:@"selectedUserId"];
    [defaults setValue:conversationId forKey:@"selectedConversationId"];
    [defaults setValue:type forKey:@"selectedType"];
    [defaults setValue:msg forKey:@"notificationMsg"];
    [defaults setValue:title forKey:@"notificationTitle"];
    
    [defaults synchronize];
    
    
    if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
        
        NSString *senderId = [userInfo valueForKey:@"gcm.notification.sender"];
        
        bool loggedIn = false;
        if ([[FIRAuth auth] currentUser] != nil){
            loggedIn = true;
        }
        
        [self showMainView:senderId loggedIn:loggedIn];
        
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[NSNotificationCenter defaultCenter] postNotification:selected];

        });
    }

    completionHandler(UIBackgroundFetchResultNewData);
    
    NSLog(@"completion handler method %@ ", userInfo);
    
}




#pragma mark -

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    BOOL result = [[FBSDKApplicationDelegate sharedInstance]
                   application:application
                   openURL:url
                   sourceApplication:sourceApplication
                   annotation:annotation];
    
    NSLog(@"OpenUrl Link result: %d", result);
    return result;
}



- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    
    NSLog(@"Push Notif Device Token_____%@", deviceToken);
    
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:FIRMessagingAPNSTokenTypeProd];
    
//    [[FIRMessaging messaging] setAPNSToken:deviceToken type:FIRMessagingAPNSTokenTypeSandbox];
    NSString *fcmToken = [FIRInstanceID instanceID].token;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:fcmToken forKey:@"fcmToken"];
    [defaults synchronize];
    
    NSLog(@"FCM registration token: %@", fcmToken);
    
}



- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSLog(@"Error At %@",err);
}



-(void) showLaunchScreen{
    
    UIStoryboard *storyboardLaunch = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
    NSString *launchScreenId = @"launchScreen";
    UIViewController *launchScreen = [storyboardLaunch instantiateViewControllerWithIdentifier:launchScreenId];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = launchScreen;
    [self.window makeKeyAndVisible];
    
}




/*******************************************************
 *
 * ---------- MASTER FLOW NAVIGATION METHOD -----------
 *
 *******************************************************/


//showMainView will show the login view controller or Search view controller based on the arguments
//If newUSR exists, the user is not new and we goto the SearchView, if it is nil, goto loginViewController
//
// Sets SelectedUser property for the SearchViewController class
-(void) showMainView: (NSString *) selectedUser loggedIn:(bool) loggedIn{
    
    
    NSString *storyboardId = loggedIn ? @"MainIdentifier" : @"LoginIdentifier";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    

    
    if ([storyboardId  isEqualToString:@"LoginIdentifier"]){
        //selectedUser will always be nil because this will never be opened from a push nofification
        
        __unsafe_unretained typeof(self) weakSelf = self;
        
        [self showLaunchScreen];
        
        [FirebaseManager getUserFirebaseDataForUid:selectedUser completion:^(UserObject *userdata) {
                
            [SVProgressHUD show];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
            [defaults setValue:userdata.uid forKey:@"selectedUserId"];
            [defaults setValue:@"SearchView" forKey:@"Source"];
            [defaults synchronize];
                                
            LoginViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:storyboardId];
            initViewController.selectedUser = userdata;
            initViewController.gotoChatView = YES;
            
            weakSelf.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            weakSelf.window.rootViewController = initViewController;
            [weakSelf.window makeKeyAndVisible];
                                
        }];
        
    }else{
        
        [FirebaseManager getUserFirebaseDataForUid:[[FIRAuth auth] currentUser].uid completion:^(UserObject *userdata) {
            
            UserObject *currentUser = userdata;
            
            SearchViewController *centerDrawer = [storyboard instantiateViewControllerWithIdentifier:storyboardId];
            NSString *inboxStoryBoardId = @"inboxviewcontroller";
            NSString *dashStoryboardId = @"DashBoard";
            NSString *drawerStoryboardId = @"drawerController";
            DashBoardViewController *leftDrawer = [storyboard instantiateViewControllerWithIdentifier:dashStoryboardId];
            InboxViewController *rightDrawer = [storyboard instantiateViewControllerWithIdentifier:inboxStoryBoardId];
            
            if(selectedUser != nil){
                
                __unsafe_unretained typeof(self) weakSelf = self;
                
                [FirebaseManager getUserFirebaseDataForUid:selectedUser completion:^(UserObject *userdata) {
                    
                    
                    if (_drawerController == nil ){
                    
                        [SVProgressHUD show];
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setValue:userdata.uid forKey:@"selectedUserId"];
                        [defaults synchronize];
                        
                        //-------  drawer code start
                        [leftDrawer setChat:nil];
                        weakSelf.drawerController = [[storyboard instantiateViewControllerWithIdentifier:drawerStoryboardId]
                                                     initWithCenterViewController:centerDrawer
                                                     leftDrawerViewController:leftDrawer rightDrawerViewController:rightDrawer];
                        
                        
                        weakSelf.drawerController.selectedUser = userdata;
                        weakSelf.drawerController.chat = [ChatObject alloc];
                        weakSelf.drawerController.chat.currentUser = currentUser;
                        weakSelf.drawerController.gotoChatView = YES;
                        
                        weakSelf.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
                        weakSelf.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
                        
                        
                        //drawer code -- end
                        weakSelf.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                        weakSelf.window.rootViewController = weakSelf.drawerController;
                        [weakSelf.window makeKeyAndVisible];
                        
                    }
                }];
                
            }else{
                
                
                
                
                
                
                
                
            }
        }];
        
        
    }
}


-(MMDrawerSide)openSide{
    
    return self.drawerController.openSide;
    
}






-(void)reachabilityChanged{
    
    if (!_reach.isReachable){
        
        NSLog(@"UNREACHABLE");
        //For now, I can't figure out why reachability is firing twice. It happens with nofication and blocks and NetworkChecks.
        //Therefore I use changeCount to make sure
        if (_changeCount == 1){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self NetworkConnectionAlert];
            });
            
        }else{
            _changeCount++;
        }
    }else{
        NSLog(@"REACHABLE");
    }
}



-(void)NetworkConnectionAlert{
    _changeCount = 0;
    UIAlertController *netAlert=   [UIAlertController
                                    alertControllerWithTitle:@"Alert"
                                    message:@"Please check your network connection"
                                    preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* close = [UIAlertAction
                            actionWithTitle:@"Close"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            
                            {
                                //TODO: refresh search table
                                
                                [netAlert dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    UIAlertAction* settings = [UIAlertAction
                               actionWithTitle:@"Settings"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               
                               {
                                   //TODO: refresh search table
                                   //                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=General"] options:@{} completionHandler:^(BOOL success) {
                                       
                                       
                                   }];
                                   [netAlert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    
    
    
    
    [netAlert addAction:settings];
    [netAlert addAction:close];
    
    UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];
    alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:netAlert animated:YES completion:nil];
}

@end

