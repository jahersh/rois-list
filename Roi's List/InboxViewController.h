//
//  InboxViewController.h
//  Accomplice
//
//  Created by ibuildx on 4/19/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DemoMessagesViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>

@interface InboxViewController : UIViewController <JSQDemoViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, ChatProtocol>

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
//@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *logOutBtn;

@property (weak, nonatomic) IBOutlet UILabel *noConvoLbl;

@property (strong) UIColor *previousColor;
@property (strong) UIRefreshControl *refreshControl;
@property (weak) UIView *refreshView;

@property (strong) NSMutableDictionary *cachedImages;

@property double pageSize;
@property (weak, nonatomic) IBOutlet GADBannerView *adView;
@property (strong) NSMutableArray *toDelete;

@property (strong) NSMutableDictionary  *usersFound;

@property (strong) NSArray  *flatColors;
@property (strong) UserObject *selectedUser;
@property (strong) UserObject *currentUser;

- (IBAction)backBtnAction:(id)sender;
- (IBAction)deleteAction:(id)sender;


//@property (strong) BackendlessUser *selectedUser;

@property (weak) NSString * userId;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) ChatObject *chat;

@property (strong) NSMutableArray *uniqueMsgs;
@property (strong) NSMutableArray *allUniqueMsgs;
@property (strong) UIAlertController *alert;
@property UIAlertController *alertDelete;
@property NSDate *compareDate;
//@property (strong) NSMutableDictionary *unseenMsgsDict;
@property NSUInteger deleteOffset;
@property NSUInteger deletedPage;
@property int totalDeletedPages;
@property (strong) NSMutableArray *sortedArray;
@property (strong, nonatomic) NSMutableDictionary *inboxList;

-(void) replaceWithMostRecentMsg:(NSString *)convoId userId:(NSString*)userId;
- (void)refresh:(UIRefreshControl *)refreshControl;


@end
