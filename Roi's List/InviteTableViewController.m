//
//  InviteTableViewController.m
//  Rois List
//
//  Created by Justin Hershey on 11/1/16.
//  Copyright © 2016 Rois LIst. All rights reserved.
//

#import "InviteTableViewCell.h"
#import "InviteTableViewController.h"
#import <SVProgressHUD.h>
#import <TSMessage.h>
#import "DashBoardViewController.h"


//#import "MFMessageComposeViewController.h"

@interface InviteTableViewController () <ChatProtocol> {
    
    //contacts available to be searched (Master)
    NSMutableArray *searchContacts;
    
    //selected users in path
    NSMutableArray *masterPath;
    NSString *appId;
    
}

@end
@implementation InviteTableViewController


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



-(void) viewDidLoad{
    [super viewDidLoad];
    
    _tableview.delegate = self;
    _tableview.dataSource = self;
    _tableview.allowsMultipleSelection = YES;
    appId = @"1287696896";
    
    _searchBar.delegate = self ;
    
    _sendBtn.hidden = YES;
    
    _chat.delegate = self;
    
    
    [_tableview reloadData];
    
}


//
//-(void)applicationDidEnterBackground:(NSNotification *)notification{
//
//    _chat.showTSMessage = NO;
//
//}
//
//- (void)applicationIsActive:(NSNotification *)notification {
//    NSLog(@"Application Did Become Active");
//
//    if(!notification){
//        _chat.showTSMessage = YES;
//    }
//
//}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(applicationIsActive:)
    //                                                 name:UIApplicationDidBecomeActiveNotification
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(applicationDidEnterBackground:)
    //                                                 name:UIApplicationDidEnterBackgroundNotification
    //                                               object:nil];
    
    self.contacts = [[NSMutableArray alloc] init];
    searchContacts = [[NSMutableArray alloc] init];
    masterPath = [[NSMutableArray alloc] init];
    
    [self fetchContactsAndRequest];
    //    [self requestAndFetch];
    self.sendBtn.hidden = YES;
    
    [_tableview reloadData];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}


/*************************************************
 *
 * ---------- SEARCH DELEGATE METHODS ------------
 *
 *************************************************/

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    
    NSLog(@"Search String = %@",searchBar.text);
    [_contacts removeAllObjects];
    
    if ([searchBar.text isEqualToString:@""]) {
        
        [_contacts addObjectsFromArray:searchContacts];
        [self.tableview reloadData] ;
        return ;
    }
    
    for (NSDictionary *contact in searchContacts) {
        if ([[contact valueForKey:@"fullName"] containsString:searchBar.text]) {
            [_contacts addObject:contact];
        }
    }
    [self.tableview reloadData];
    
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"Search String = %@",searchBar.text);
    
    [_contacts removeAllObjects];
    
    if ([searchBar.text isEqualToString:@""]) {
        [_contacts addObjectsFromArray:searchContacts];
        [self.tableview reloadData] ;
        return ;
    }
    
    for (NSDictionary *contact in searchContacts) {
        
        if ([[contact valueForKey:@"fullName"] containsString:searchBar.text]) {
            [_contacts addObject:contact];
        }
    }
    
    [self.tableview reloadData] ;
}


-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    NSLog(@"Search did begin editing");
    searchBar.showsCancelButton = true;
}

-(void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    NSLog(@"Search did end editing");
    searchBar.showsCancelButton = false;
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    
    [self.searchBar resignFirstResponder];
    
    
}


-(void) mergeSelectedCells: (NSString*) selectedId{
    
    //Merge the selectedcells from the searching table to the master
    
    
    //willDisplayCell and cellForRowatIndexOath will handle the cell highlighing and checkmark accessory when table is reloaded
    
    int f = 1;
    for(NSString* i in masterPath){
        
        if([i isEqualToString:selectedId]){
            break;
            
        }else{
            f++;
        }
        
    }
    
    //if the string is not in the array these two integers will not be equal
    if (masterPath.count != f){
        
        [masterPath addObject:selectedId];
        
    }
}




/*************************************************
 *
 * ---------- CONTACT METHODS ------------
 *
 *************************************************/

-(void) fetchContactsAndRequest{
    
    CNContactStore *contactStore = [[CNContactStore alloc] init];
    
    [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        
        if (granted == YES) {
            
            _rowId = 0;
            NSError* contactError;
            CNContactStore* addressBook = [[CNContactStore alloc]init];
            [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
            NSArray * keysToFetch =@[CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey];
            CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
            
            [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
                
                
                NSString *string = [NSString stringWithFormat:@"%d", _rowId];
                [self parseContacts:contact rowId:string];
            }];
            
            
        } if (granted == NO){
            
            UIAlertController *alert =   [UIAlertController
                                          alertControllerWithTitle:@"Access Denied"
                                          message:@"Please grant access to contacts in settings"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            UIAlertAction* settings = [UIAlertAction
                                       actionWithTitle:@"Settings"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           
                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                           [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                                           
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                           
                                       }];
            
            [alert addAction:cancel];
            [alert addAction:settings];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            NSLog(@"Did not grant access to contacts");
            
        }
    }];
}




-(void)parseContacts:(CNContact *)contact rowId: (NSString*)rowId{
    
    
    NSString *phone;
    NSString *fullName;
    NSString *firstName;
    NSString *lastName;
    NSString *rId;
    
    
    // copy data to my custom Contacts class.
    firstName = contact.givenName;
    lastName = contact.familyName;
    rId = rowId;
    
    fullName=[NSString stringWithFormat:@"%@ %@",firstName, lastName];
    NSLog(@"FUll NAME: %@", fullName);
    
    for (CNLabeledValue *label in contact.phoneNumbers) {
        
        if ([label.label isEqualToString:CNLabelHome]){
            
            phone = [label.value stringValue];
            
        }
        if (phone == nil && [label.label isEqualToString:CNLabelPhoneNumberMobile]){
            
            phone = [label.value stringValue];
            
        }
        if(phone == nil && [label.label isEqualToString:CNLabelPhoneNumberiPhone]){
            
            phone = [label.value stringValue];
            
        }
        if(phone == nil && [label.label isEqualToString:CNLabelPhoneNumberMain]){
            
            phone = [label.value stringValue];
            
        }
        if(phone == nil && [label.label isEqualToString:CNLabelWork]){
            
            phone = [label.value stringValue];
            
        }
        if(phone == nil && [label.label isEqualToString:CNLabelOther]){
            
            phone = [label.value stringValue];
            
        }
    }
    
    
    
    
    
    NSLog(@"Phone Number: %@", phone);
    
    if ((phone != nil) && (fullName != nil) && (phone.length >= 7)){
        
        
        NSDictionary *personData = [[NSDictionary alloc] initWithObjectsAndKeys:fullName,@"fullName",phone,@"phoneNumber", rId,@"id", nil];
        
        //we only increment the rowId if in fact we are adding an object to contacts and searchContacts
        _rowId++;
        
        [_contacts addObject:personData];
        [searchContacts addObject:personData];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [_tableview reloadData];
        
    });
}


/*************************************************
 *
 * --------- MESSAGING DELEGATE METHODS ---------
 *
 *************************************************/


-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    switch (result) {
            
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultFailed:
        {
            _alert=   [UIAlertController
                       alertControllerWithTitle:@"Error"
                       message:@"Failed to send SMS!"
                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [_alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [_alert addAction:ok];
            
            [self presentViewController:_alert animated:YES completion:nil];
            break;
            
        }
            
        case MessageComposeResultSent:
            
            break;
            
        default:
            
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}



//method to compile selected users and send invites
-(void) sendInvites{
    
    NSMutableArray *recipientArray = [[NSMutableArray alloc] init];
    NSArray *selectedUsers = masterPath;
    
    for (NSString *indexRow in selectedUsers){
        
        int path = [indexRow intValue];
        NSDictionary *contact = [self.contacts objectAtIndex:path];
        
        NSString *stringToTrim = [contact objectForKey:@"phoneNumber"];
        
        //replacing extraneous symbols for a consistant number
        NSString *recipient = [stringToTrim stringByReplacingOccurrencesOfString:@"(" withString:@""];
        recipient = [recipient stringByReplacingOccurrencesOfString:@")" withString:@""];
        recipient = [recipient stringByReplacingOccurrencesOfString:@")" withString:@""];
        recipient = [recipient stringByReplacingOccurrencesOfString:@"-" withString:@""];
        recipient = [recipient stringByReplacingOccurrencesOfString:@"+" withString:@""];
        recipient = [recipient stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        NSLog(@"Recipient: %@", recipient);
        [recipientArray addObject:recipient];
    }
    
    
    NSString *appStoreLink = [NSString stringWithFormat:@"https://itunes.apple.com/us/app/roi's list/id%@?mt=8", appId];
    NSString *message = [NSString stringWithFormat:@"Check out Rois List on the app store! %@", appStoreLink];
    
    
    MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc] init];
    smsController.messageComposeDelegate = self;
    
    
    if(![MFMessageComposeViewController canSendText]) {
        
        self.contacts = [[NSMutableArray alloc] init];
        searchContacts = [[NSMutableArray alloc] init];
        
        masterPath = [[NSMutableArray alloc] init];
        [self fetchContactsAndRequest];
        self.sendBtn.hidden = YES;
        
        [_tableview reloadData];
        
        
    }else{
        
        
        [smsController setRecipients:recipientArray];
        [smsController setBody:message];
        [self presentViewController:smsController animated:YES completion:nil];
        
    }
    [SVProgressHUD dismiss];
    
}






/*****************************************
 *
 * ----- TABLEVIEW DELEGATE METHODS -----
 *
 *****************************************/



- (void)tableView:(UITableView *)tableView willDisplayCell:(InviteTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *selectedId = cell.masterRowLbl.text;
    
    
    NSLog(@"MasterPath: %@", masterPath);
    
    
    //If the masterRowLbl is in the master path, select that cell
    if ([masterPath containsObject:selectedId]) {
        
        
        [_tableview selectRowAtIndexPath:indexPath animated:YES  scrollPosition:UITableViewScrollPositionBottom];
        
        
    }else{
        //searching will still leave the accessory checkmark in selected index paths but the indexPaths change when searching
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}



-(void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    InviteTableViewCell *cell = [self.tableview cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    
    
    [masterPath removeObject:cell.masterRowLbl.text];
    
    NSLog(@"Selected Users: %@", masterPath);
    
    if (masterPath.count == 0){
        
        self.sendBtn.hidden = YES;
        
    }
}




-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    InviteTableViewCell *cell = [self.tableview cellForRowAtIndexPath:indexPath];
    
    
    
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    
    [self mergeSelectedCells:cell.masterRowLbl.text];
    
    
    
    
    if (masterPath.count > 0){
        self.sendBtn.hidden = NO;
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.contacts.count;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *CellIdentifier = @"contactCell";
    InviteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:15.0/255.0 green:117.0/255.0 blue:149.0/255.0 alpha:0.5];
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell setTintColor:[UIColor blackColor]];
    
    NSDictionary *selectedContact = [self.contacts objectAtIndex:indexPath.row];
    
    cell.nameLb.text = [selectedContact objectForKey:@"fullName"];
    cell.masterRowLbl.text = [selectedContact objectForKey:@"id"];
    [cell.masterRowLbl setHidden:YES];
    
    
    if ([masterPath containsObject:cell.masterRowLbl.text])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    NSString *stringToTrim = [selectedContact objectForKey:@"phoneNumber"];
    
    //replacing extraneous symbols for a consistant number
    NSString *pn = [stringToTrim stringByReplacingOccurrencesOfString:@"(" withString:@""];
    pn = [pn stringByReplacingOccurrencesOfString:@")" withString:@""];
    pn = [pn stringByReplacingOccurrencesOfString:@"-" withString:@""];
    pn = [pn stringByReplacingOccurrencesOfString:@"+" withString:@""];
    pn = [pn stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSMutableString *mutableString = (NSMutableString*)[pn stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"%@", mutableString);
    
    cell.numberLbl.text = mutableString;
    
    return cell;
}

/*****************************************
 *
 * --------- ACTION METHODS ----------
 *
 *****************************************/

- (IBAction)sendAction:(id)sender {
    
    [SVProgressHUD show];
    
    
    [_contacts removeAllObjects];
    _contacts = searchContacts;
    
    [self.tableview reloadData];
    [self sendInvites];
}




/*****************************************
 *
 * ----- CHATOBJECT DELEGATE METHODS -----
 *
 *****************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                           
                                           
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}





/*****************************************
 *
 * ----- NAVIGATION METHODS -----
 *
 *****************************************/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"backToDash"]) {
        DashBoardViewController *dashView = segue.destinationViewController;
        //        dashView.fbObject = self.fbObject;
        
        [dashView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = dashView;
    }
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"InviteView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;
        
        [SVProgressHUD dismiss];
        
    }
    _chat = nil;
    
    
}

- (IBAction)unwindToInvite:(UIStoryboardSegue *)unwindSegue
{
    
}


@end
