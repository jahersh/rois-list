//
//  inboxProfPopup.m
//  Complice
//
//  Created by Justin Hershey on 3/2/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import "InboxProfPopup.h"
#import "BlockUser.h"
#import "DataManager.h"
#import "Inbox.h"
#import "FlagViewController.h"
#import "SearchViewController.h"

@interface InboxProfPopup (){
    
    UIAlertController *alert;
}

@end

@implementation InboxProfPopup

@synthesize user;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its
    _scrollView.delegate = self;
    
    NSString *tempText = user.profileInfo;
    self.discText.text = tempText;
    self.discText.delegate = self;
    
    CGSize textViewSize = [self.discText sizeThatFits:CGSizeMake(self.discText.frame.size.width, FLT_MAX)];
    
    if(textViewSize.height > self.scrollView.frame.size.height){
        
        self.discText.text = [tempText stringByAppendingString:@"\n\n\n"];
        textViewSize = [self.discText sizeThatFits:CGSizeMake(self.discText.frame.size.width, FLT_MAX)];
        
    }
    
    self.discTextHeight.constant = textViewSize.height;
    
    self.displayName.text = [NSString stringWithFormat:@"%@", user.firstName];
    
    self.distanceLbl.text = [NSString stringWithFormat:@"%@, %@", user.city, user.state];
//    self.genderAgeText.text = [NSString stringWithFormat:@"%@", user.gender];
    
    [self setupView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*************************************
 *
 * -------- UI SETUP METHODS --------
 *
 *************************************/


-(void)viewDidLayoutSubviews{
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.view.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii:(CGSize){15.0, 15.}].CGPath;
    
    self.view.layer.mask = maskLayer;
    self.userImageView.clipsToBounds = YES;
    self.userImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userImageView.layer.borderWidth = self.userImageView.frame.size.width / 100;
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2;
    
}


-(void)setupView{
    
    NSLog(@" textView size %f", self.scrollView.contentSize.height);
    NSLog(@" scrollview size %f", self.scrollView.frame.size.height);
    
    if (self.discTextHeight.constant - 10 <= self.scrollView.frame.size.height) {
        self.downArrow.hidden = YES;
    }
    
    self.infoView.layer.cornerRadius = 15;
    self.upArrow.hidden = YES;
    
    //drop shadows
    self.propertyContractedLbl.clipsToBounds = NO;
    self.propertyContractedLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.propertyContractedLbl.layer.shadowOpacity = 0.4;
    self.propertyContractedLbl.layer.shadowRadius = 6;
    self.propertyContractedLbl.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    self.propertyContractedLbl.backgroundColor = [UIColor orangeColor];
    self.propertyContractedLbl.layer.cornerRadius = 5.0;
    self.propertyContractedLbl.text = @"Pending Contract";
    
    if ([self.user.propertyContracted isEqualToString:@"1"]){
        self.propertyContractedLbl.hidden = NO;
    }else{
        self.propertyContractedLbl.hidden = YES;
    }
    
    
    
    
    double sExpireTime = [self.user.boostExpireTime doubleValue];
    NSString *profileBoosted = user.boostedProfile;
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    
    if ([profileBoosted isEqualToString: @"1"] && (nowMillis < sExpireTime)){
        
        [self setBoostVisibility:NO];
        
        if(sExpireTime > nowMillis){
            
            self.interval = sExpireTime - nowMillis;
            self.expireTime = sExpireTime;
            
            [self startTimer];
        }
    }else{
        [self setBoostVisibility:YES];
    }
    
    
    self.boostLbl.clipsToBounds = NO;
    self.boostLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostLbl.layer.shadowOpacity = 0.4;
    self.boostLbl.layer.shadowRadius = 3;
    self.boostLbl.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.boostExpiresLbl.clipsToBounds = NO;
    self.boostExpiresLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.boostExpiresLbl.layer.shadowOpacity = 0.4;
    self.boostExpiresLbl.layer.shadowRadius = 3;
    self.boostExpiresLbl.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    
    
    self.flagBtn.clipsToBounds = NO;
    self.flagBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.flagBtn.layer.shadowOpacity = 0.4;
    self.flagBtn.layer.shadowRadius = 6;
    self.flagBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.distanceLbl.clipsToBounds = NO;
    self.distanceLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.distanceLbl.layer.shadowOpacity = 0.4;
    self.distanceLbl.layer.shadowRadius = 3;
    self.distanceLbl.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.genderAgeText.clipsToBounds = NO;
    self.genderAgeText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.genderAgeText.layer.shadowOpacity = 0.4;
    self.genderAgeText.layer.shadowRadius = 3;
    self.genderAgeText.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.displayName.clipsToBounds = NO;
    self.displayName.layer.shadowColor = [UIColor blackColor].CGColor;
    self.displayName.layer.shadowOpacity = 0.4;
    self.displayName.layer.shadowRadius = 8;
    self.displayName.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.blockBtn.clipsToBounds = NO;
    self.blockBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.blockBtn.layer.shadowOpacity = 0.4;
    self.blockBtn.layer.shadowRadius = 6;
    self.blockBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.downArrow.clipsToBounds = NO;
    self.downArrow.layer.shadowColor = [UIColor blackColor].CGColor;
    self.downArrow.layer.shadowOpacity = 0.4;
    self.downArrow.layer.shadowRadius = 3;
    self.downArrow.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.upArrow.clipsToBounds = NO;
    self.upArrow.layer.shadowColor = [UIColor blackColor].CGColor;
    self.upArrow.layer.shadowOpacity = 0.4;
    self.upArrow.layer.shadowRadius = 3;
    self.upArrow.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.closeBtn.clipsToBounds = NO;
    self.closeBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.closeBtn.layer.shadowOpacity = 0.4;
    self.closeBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    //Setup  prfile pic
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = user.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.userImageView setImage:[UIImage imageWithData:data]];
            
        });
        
    });
    
}


/*************************************
 *
 * --------- ACTION METHODS ----------
 *
 *************************************/


- (IBAction)flagAction:(id)sender {
    
    //    [self.window setRootViewController:initViewController];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:-6 forKey:@"BtnNum"];
    [defaults synchronize];
}


- (IBAction)closeAction:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:-1 forKey:@"BtnNum"];
    [defaults synchronize];
}


- (IBAction)blockAction:(id)sender {
    
    alert=   [UIAlertController
              alertControllerWithTitle:@""
              message:[NSString stringWithFormat:@"Are you sure you want to block %@?", user.firstName]
              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [FirebaseManager setBlockedUser:user.uid];
                             
                             //complete alert
                             UIAlertController *blockedAlert;
                             blockedAlert=   [UIAlertController
                                              alertControllerWithTitle:[NSString stringWithFormat:@"You blocked %@", user.firstName ]
                                              message:@"You can unblock users from Settings"
                                              preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* ok = [UIAlertAction
                                                  actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action)
                                                  {
                                                      [self.closeBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
                                                      
                                                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                      [defaults setInteger:-4 forKey:@"BtnNum"];
                                                      [defaults setBool:YES forKey:@"refreshSearch"];
                                                      [defaults synchronize];
                                                      
                                                      [blockedAlert dismissViewControllerAnimated:YES completion:nil];
                                                      
                                                  }];
                             
                             [blockedAlert addAction:ok];
                             [self presentViewController:blockedAlert animated:YES completion:nil];
                             
                             
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"NO"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 NSLog(@"Cancel button touched!");
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}






-(void) startTimer{
    
    if (_timer == nil){
        
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
        
    }else{
        
        [_timer invalidate];
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
}


-(void) refreshTimer{
    
    NSInteger ti = (NSInteger)self.interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    self.boostExpiresLbl.text = [NSString stringWithFormat:@"Boost Time Left: %02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    
    self.interval = self.expireTime - nowMillis;
    
}


-(void) setBoostVisibility:(bool)hidden{
    
    [self.boostExpiresLbl setHidden:hidden];
    [self.boostLbl setHidden:hidden];
}






/***********************************************
 *
 * -------- SCROLLVIEW DELEGATE METHOD --------
 *
 **********************************************/

//hide and display scroll arrow in description
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    float scrollOffset = scrollView.contentOffset.y;
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    
    //NSLog(@"THE THING IS SCROLLING");
    self.downArrow.hidden = YES;
    self.upArrow.hidden = YES;
    
    
    if (scrollOffset == 0)
    {
        self.downArrow.hidden = NO;
    }
    
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        // then we are at the end
        self.upArrow.hidden = NO;
        
    }
}


@end
