//
//  ProfileViewController.h
//  Accomplice
//
//  Created by ibuildx on 3/15/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoMessagesViewController.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "FirebaseManager.h"



//@import GoogleMaps;

@interface ProfileViewController : UIViewController <JSQDemoViewControllerDelegate, ChatProtocol>



@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;
@property (weak, nonatomic) IBOutlet UILabel *userNameText;
@property (weak, nonatomic) IBOutlet UIView *shadowView;


@property (strong) UserObject *currentUser;
@property (strong) UserObject *selectedUser;


@property (weak, nonatomic) IBOutlet UIButton *backBtn;



@property (weak, nonatomic) IBOutlet UILabel *ageText;
@property (weak, nonatomic) IBOutlet UITextView *profileInfoText;


- (IBAction)backBtnAction:(id)sender;



@property (weak) NSString * userId;

@property (nonatomic, strong) ChatObject *chat;

@end
