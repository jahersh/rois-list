//
//  UIViewController+FaqViewController.h
//  Complice
//
//  Created by Justin Hershey on 11/8/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatObject.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "FirebaseManager.h"


@interface FaqViewController : UIInputViewController <ChatProtocol, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *tutorialBtn;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)tutorialAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *faqLbl;

@property (nonatomic, strong) ChatObject *chat;
@property (strong) UserObject *currentUser;

@property (strong) UserObject *selectedUser;

@end
