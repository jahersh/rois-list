

#import "DemoMessagesViewController.h"
#import "DataManager.h"
#import "SearchViewController.h"
#import "InboxViewController.h"
#import "BlockViewController.h"
#import "DashBoardViewController.h"
#import "EditProfileViewController.h"
#import "FaqViewController.h"
#import "FlagViewController.h"
#import "InviteTableViewCell.h"
#import "LegalViewController.h"
#import "ProfileViewController.h"
#import "SettingViewController.h"
#import "SupportViewController.h"
#import "InviteTableViewController.h"
#import "AppDelegate.h"
#import "TSMessage.h"
#import <SVProgressHUD.h>
#import "InboxProfPopup.h"
#import "InstagramInboxProfPopup.h"



@implementation DemoMessagesViewController

#pragma mark - View lifecycle


    UIAlertController *alert;
    UILongPressGestureRecognizer *longPress;
    CGFloat statusBarHeight;
    UIView *navigationBar;
    UIButton* profileImage;


-(void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc{
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    
    return UIStatusBarStyleDefault;
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    

    self.navigationController.navigationBar.backgroundColor = UIColor.clearColor;
    
    statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    self.inputToolbar.contentView.leftBarButtonItem = nil ;
    [self.inputToolbar.contentView.rightBarButtonItem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //set to False in viewDidLoad otherwise scrollViewDidScroll will call loadMore() twice on load, set to true after initial scrollToBottom in ViewDidAppear
    
    self.scrollViewInitialScroll = true;
    
    
    /**
     *  Create message bubble images objects.
     *
     *  Be sure to create your bubble images one time and reuse them for good performance.
     *
     */
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[DataManager getGreenColor]];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    
    self.messages = [[NSMutableArray alloc] init];
    
    
    /**
     *  You MUST set your senderId and display name
     */
    
    self.senderId = [self.chat currentUser].uid;
    self.senderDisplayName =  [self.chat currentUser].firstName;
    
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    self.inputToolbar.contentView.textView.delegate = self;
    self.inputToolbar.contentView.textView.keyboardType=UIKeyboardTypeDefault;
    self.showLoadEarlierMessagesHeader = NO;

    
    //get convoId or just get conversation data
    if ([_conversationId isEqualToString:@""] || _conversationId == nil){
        
        [FirebaseManager getConversationObjectId:_selectedUser.uid completion:^(NSString *conversationId) {
            
            self.conversationId = conversationId;
            [self refreshInboxData];
        }];
    }else{
        [self refreshInboxData];
    }
    

    if (_chat == nil) {
        _chat = [ChatObject new] ;
    }

    
    BOOL isDeleted = false;
    if(![self.selectedUser.deletedDate isEqualToString: @""]){
        isDeleted = true;
    }
    
    if (isDeleted){
        
        alert=   [UIAlertController
                  alertControllerWithTitle:@"User Deleted"
                  message:@"They will not receive new messages"
                  preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];

        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    self.collectionView.backgroundColor = [UIColor colorWithRed:(242.0 / 255.0) green:(242.0 /255.0) blue:(242.0 /255.0) alpha:1.0];

    self.inputToolbar.contentView.backgroundColor = [DataManager getGreenColor];
    
    //open drawer image setup
    _chat.delegate = self ;
    
    NSLog(@"Chat object %@",_chat);
    
    
    [SVProgressHUD show];
    
}





- (void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshCollectionView:)
                                                 name:@"refreshNotify"
                                               object:nil];
    
    [self setupCustomNavBar];
    
    
}



- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    self.collectionView.collectionViewLayout.springinessEnabled = NO;
    
    //To re-enable scroll to top to load previous messages
    self.scrollViewInitialScroll = false;
    
    self.collectionView.frame = CGRectMake(0, navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - (navigationBar.frame.size.height));
    
    [self scrollToBottomAnimated:NO];
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //remove refresh observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


/***************************************************
 *
 * ------------ CUSTOM NAV BAR SETUP --------------
 *
 ***************************************************/

-(void) setupCustomNavBar {
    
    CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
    
    CGFloat statusHeight = statusBarFrame.size.height;
    
    navigationBar = [[UIView alloc] init];
    
    UIImage* image = [UIImage imageNamed:@"Wave"];
    UIImage* closeImage = [UIImage imageNamed:@"closeX"];
    
    navigationBar.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 50 + statusHeight);
    
    CGRect waveFrame = CGRectMake(self.view.frame.size.width - (navigationBar.frame.size.height * 0.7 + 10), statusBarHeight + statusBarHeight/3, navigationBar.frame.size.height * 0.6 , navigationBar.frame.size.height * 0.6);
    
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, statusBarHeight + statusBarHeight/2, navigationBar.frame.size.height/2, navigationBar.frame.size.height/2)];
    
    UILabel* displayNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 10, navigationBar.frame.size.height/4 + statusBarHeight/2, (self.view.frame.size.width/2 - (waveFrame.size.width + 10)), navigationBar.frame.size.height/2)];
    
    [displayNameLbl setFont:[UIFont boldSystemFontOfSize:20]];
    [displayNameLbl setText:self.selectedUser.firstName];

    displayNameLbl.textColor = [UIColor whiteColor];
    displayNameLbl.backgroundColor = [UIColor clearColor];
    
    profileImage = [[UIButton alloc] init];
    profileImage.clipsToBounds = YES;
    profileImage.contentMode = UIViewContentModeScaleAspectFill;
    [profileImage setUserInteractionEnabled:YES];
    profileImage.frame = CGRectMake(self.view.frame.size.width/2 - (navigationBar.frame.size.height*0.6 + 10), navigationBar.frame.size.height/4 + statusBarHeight/2, navigationBar.frame.size.height*0.5, navigationBar.frame.size.height*0.5);
    
    profileImage.layer.cornerRadius = profileImage.frame.size.width / 2;
    profileImage.layer.borderWidth = profileImage.frame.size.width / 16;
    profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = self.selectedUser.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
             [profileImage setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
        });
    });


    closeBtn.backgroundColor = [UIColor clearColor];
    [closeBtn setImage:closeImage forState:UIControlStateNormal];
    
    
    UIButton* someButton = [[UIButton alloc] initWithFrame:waveFrame];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    someButton.tintColor = [UIColor whiteColor];
    [closeBtn addTarget:self action:@selector(closePressed:) forControlEvents:UIControlEventTouchDown];
    [someButton addTarget:self action:@selector(waveAction:) forControlEvents:UIControlEventTouchDown];
    [profileImage addTarget:self action:@selector(showUserProfile:) forControlEvents:UIControlEventTouchDown];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfile:)];
    tapGesture.numberOfTapsRequired = 1;
    
    [displayNameLbl addGestureRecognizer:tapGesture];
    displayNameLbl.userInteractionEnabled = YES;
    
    navigationBar.backgroundColor = [DataManager getGreenColor];

    [navigationBar addSubview:closeBtn];
    [navigationBar addSubview:someButton];
    [navigationBar addSubview:profileImage];
    [navigationBar addSubview:displayNameLbl];
    
    [self.navigationController.view addSubview:navigationBar];
    
}



/***********************************
 *
 *  INBOX MANIPULATION METHODS
 *
 **********************************/


-(void) addFromInbox: (Inbox *) message reciever: (UserObject *)reciever{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[message.created doubleValue]];
    
    if ([message.senderID isEqualToString:self.currentUser.uid])   {
        
        if ([message.messageBody isEqualToString:@"-1-2-3-4-5"]) {
            [self addWave:self.currentUser.firstName :@"Send" :message.created];
            
        }else{
            
            [self.messages addObject:
            [[JSQMessage alloc] initWithSenderId:self.currentUser.uid
                                senderDisplayName:self.currentUser.firstName
                                             date:date
                                             text:message.messageBody]];
        }
    }
    
    
    else if ([message.receiverID isEqualToString:self.currentUser.uid]){
        
        //checking for deleted messages
        if ([message.messageBody isEqualToString:@"-1-2-3-4-5"]) {
            [self addWave:reciever.firstName :@"Recieve" :message.created];
        }else{
            
            [self.messages addObject:
             [[JSQMessage alloc] initWithSenderId:@"receiverAvatar"
                                senderDisplayName:reciever.firstName
                                             date:date
                                             text:message.messageBody ]];
        }
        
    }else{
        
        NSLog(@"Made it here but shouldn't because My object Id should be either the receiver or the sender");
    }
    
    if ([message.isSeen isEqualToString:@"NO"]) {
        [self updateMessageStatus:message];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshNotify" object:self];
}



-(void)updateMessageStatus:(Inbox *)inbox
{
    
    if ([inbox.receiverID isEqualToString:self.currentUser.uid]){
        
        inbox.isSeen = @"YES";

        FIRDatabaseReference *inboxRef = [[[[FirebaseManager getConversationPath] child:_conversationId] child:inbox.objectId] child:@"isSeen"];
        
        [inboxRef setValue:@"YES"];

        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        int currentCount = [appDelegate.drawerController getHubCount];
            
        if(currentCount > 0){
            currentCount--;
            [appDelegate.drawerController decrementHubCount];
        }
            
        if(currentCount < 1){
            
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        }
    }
    
    NSString *str = [inbox valueForKey:@"isSeen"];
    NSLog(@"isSeen = %@" ,str);
}



-(void)sortMessagesArray{
    
    [_messages sortUsingComparator: ^(JSQMessage* a, JSQMessage* b) {
        return [a.date compare:b.date];
    }];
}


-(void)addWave:(NSString *)name :(NSString *)status :(NSString *)created{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970: [created doubleValue]];
    
    JSQPhotoMediaItem *photoItem;
    if ([status isEqualToString:@"Send"]) {
        
        photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"waveSend"]];
        
        
        [self.messages addObject:
         [[JSQMessage alloc] initWithSenderId:self.currentUser.uid senderDisplayName:name date:date media:photoItem]];
    }
    
    else
    {
        
        photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"waveRecieve"]];
        
        
        [self.messages addObject:
         [[JSQMessage alloc] initWithSenderId:@"receiverAvatar" senderDisplayName:name date:date media:photoItem]];
    }
    
}









/***************************************************
 *
 * ---------- SHOW USER PROFILE ACTION ------------
 *
 ***************************************************/

-(IBAction)showUserProfile: (id) sender{
    
    NSLog(@"Show User Profile button touched");
    
    //setup popup view
    navigationBar.userInteractionEnabled = NO;
    self.collectionView.userInteractionEnabled = NO;
    
    [self.inputToolbar.contentView.textView resignFirstResponder];
    self.inputToolbar.userInteractionEnabled = NO;
    
    
    
    if (![self.selectedUser.instagramLinked isEqualToString:@"0"]){
        
        InstagramInboxProfPopup *inboxProfPopup = [[InstagramInboxProfPopup alloc] initWithNibName:@"InstagramInboxProfPopup" bundle:nil];
        
        //setup popup view
        inboxProfPopup.user = self.selectedUser;
        
        inboxProfPopup.view.frame = CGRectMake(0, self.view.frame.origin.y + 20, self.view.frame.size.width*.8, self.view.frame.size.height * .8);
        [self.view sendSubviewToBack:self.navigationController.navigationBar];
        
        [self presentPopupViewController:inboxProfPopup animated:YES completion:^(void) {
            
            [inboxProfPopup.closeBtn addTarget:self action:@selector(dismissInboxPopup) forControlEvents:UIControlEventTouchUpInside];
            
            [inboxProfPopup.flagBtn addTarget:self action:@selector(dismissInboxPopup) forControlEvents:UIControlEventTouchUpInside];
            
            UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToDismiss)];
            swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
            [inboxProfPopup.view addGestureRecognizer:swipeDown];
            
        }];
        
    }else{
        
        InboxProfPopup *inboxProfPopup = [[InboxProfPopup alloc] initWithNibName:@"InboxProfPopup" bundle:nil];
        
        //setup popup view
        inboxProfPopup.user = self.selectedUser;
        
        inboxProfPopup.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width*.8, self.view.frame.size.height * .7);
        
        [self presentPopupViewController:inboxProfPopup animated:YES completion:^(void) {
            
            [inboxProfPopup.closeBtn addTarget:self action:@selector(dismissInboxPopup) forControlEvents:UIControlEventTouchUpInside];
            
            [inboxProfPopup.flagBtn addTarget:self action:@selector(dismissInboxPopup) forControlEvents:UIControlEventTouchUpInside];
            
            UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToDismiss)];
            swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
            [inboxProfPopup.view addGestureRecognizer:swipeDown];

            
        }];
    }

    
}




/***************************************************
 *
 * ------------ DISMISS MESSAGES VC ---------------
 *
 ***************************************************/

-(void) swipeToDismiss {
    
    if (self.popupViewController != nil) {
        
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            self.popupViewController = nil;
            
            //re-enable drawer interaction and gestures
            navigationBar.userInteractionEnabled = YES;
            self.collectionView.userInteractionEnabled = YES;
            self.inputToolbar.userInteractionEnabled = YES;
            
            NSLog(@"Popup Dismissed");
        }];
    }
    
    
}



-(void) dismissInboxPopup{
    
    if (self.popupViewController != nil) {
        
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            self.popupViewController = nil;
            
            //re-enable drawer interaction and gestures
            navigationBar.userInteractionEnabled = YES;
            self.collectionView.userInteractionEnabled = YES;
            self.inputToolbar.userInteractionEnabled = YES;
            
            NSLog(@"Popup Dismissed");
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSInteger btnNum = [[defaults valueForKey:@"BtnNum"] integerValue];
            
            if (btnNum != -1) {
                
                if (btnNum == -3) {
                    
                    [defaults setInteger:-1 forKey:@"BtnNum"];
                    [defaults synchronize];
                    
                }
                else if (btnNum == -4)
                    
                    {
                        
                        [defaults setInteger:-1 forKey:@"BtnNum"];
                        [defaults synchronize];
                        
                        [self performSegueWithIdentifier:@"BackToSearch" sender:nil];
                        
                    }
                
                    else if(btnNum == -6){
                        
                        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                        [defaults setInteger:-1 forKey:@"BtnNum"];
                        [defaults synchronize];
                        
                        appDelegate.drawerController.selectedUser = self.selectedUser;
                        [self performSegueWithIdentifier:@"BackToSearch" sender:nil];
                        [appDelegate.drawerController performSegueWithIdentifier:@"toFlagUserView" sender:nil];

                    }
            }
            
        }];
    }
}




/***************************************************
 *
 * ------------ REFRESH INBOX METHOD ---------------
 *
 ***************************************************/


-(void) refreshInboxData{
    
    self.messages = [[NSMutableArray alloc] init];
    self.inboxData = [[NSMutableArray alloc] init];
    
    if (![_conversationId isEqualToString:@""] && _conversationId != nil){
        
        [FirebaseManager getInboxMessagesForObjectId:_conversationId completion:^(NSArray *messageArray) {
            
            for(NSDictionary *message in messageArray){
                
                Inbox *inbox = [[Inbox alloc] init];
                inbox.sendingUser = self.selectedUser;
                [inbox dictionaryToInbox:message];
                [self.inboxData addObject:inbox];
                [self addFromInbox:inbox reciever:self.selectedUser];
            }
            
            [SVProgressHUD dismiss];
            [self.collectionView reloadData];
        }];
    }else{
        
        [SVProgressHUD dismiss];
    }
    
    
    
    

}

/***************************************************
 *
 * ------------ RELOAD COLLECTION DATA -------------
 *
 ***************************************************/


- (void) refreshCollectionView:(NSNotification *) notification
{
    
    [self.collectionView reloadData];
    [self scrollToBottomAnimated:NO];

}



//inactive at the moment, not sure if we care about deleting individual messages right now -- NEEDS TO BE CONFIGURED W/ FIREBASE BEFORE RE-ENABLING
- (void)activateDeletionMode:(UILongPressGestureRecognizer *)gr
{
    if (gr.state == UIGestureRecognizerStateBegan) {

        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created"
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[gr locationInView:self.collectionView]];
        
        NSArray *sortedArray = [self.inboxData sortedArrayUsingDescriptors:sortDescriptors];
        
        Inbox *inbox = [sortedArray objectAtIndex:indexPath.row];
        
        NSLog(@"Deleting at index: %ld", (long)indexPath.row);
        
        alert=   [UIAlertController
                  alertControllerWithTitle:@"Delete Message?"
                  message:@""
                  preferredStyle:UIAlertControllerStyleAlert];

        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Delete"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //marking messages as deleted
                                 if ([inbox.deletedBy isEqualToString:inbox.senderID] || [inbox.deletedBy isEqualToString:@"both"]){
                                     inbox.deletedBy = @"both";
                                     NSLog(@"Marking both have deleted message");
                                 }else{
                                     inbox.deletedBy= self.currentUser.uid;
                                     NSLog(@"Marking %@ has deleted: ", self.currentUser.uid);
                                 }
                                 
//                                 NSLog(@"creating datastore");
//                                 id<IDataStore> dataStore = [backendless.persistenceService of:[Inbox class]];
//                                 
//                                 NSLog(@"saving inbox");
//                                 [dataStore save:inbox responder:responder];
                                 
                                 NSLog(@"Message Text to Delete: %@", inbox.messageBody);
                                 NSLog(@"removing from collection view");
                                 
                                 [self.messages removeObjectAtIndex:indexPath.row];
                                 [self.inboxData removeObjectAtIndex:indexPath.row];
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                                 //need to remove message from inboxData or indexes will be off
                                 [SVProgressHUD show];
                                 [self.collectionView reloadData];
                                 [self scrollToBottomAnimated:YES];
                                 [SVProgressHUD dismiss];
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alert addAction:cancel];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}



/***************************************************
 *
 * ------------ WAVE BUTTON ACTION ---------------
 *
 ***************************************************/


- (IBAction)waveAction:(id)sender {
    NSLog(@"Wave Pressed");
    
    [SVProgressHUD show];
    
    [_chat connectAndSendWave:_selectedUser.uid token:_selectedUser.fcmToken andName:_selectedUser.firstName WithCompletion:^(BOOL success) {
        if (success) {
            [SVProgressHUD dismiss];

//            JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"waveSend"]];
//            JSQMessage *photoMessage = [JSQMessage messageWithSenderId:self.currentUser.uid
//                                                           displayName:_selectedUser.firstName
//                                                                 media:photoItem];
//            
            [self.collectionView reloadData];
            [self finishSendingMessageAnimated:YES];
            
        }
        else{
            
            [SVProgressHUD dismiss];
            alert=   [UIAlertController
                      alertControllerWithTitle:@"Handshake failed"
                      message:@"Handshake has failed to send"
                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        [self scrollToBottomAnimated:NO];
    }];
}




/***************************************************
 *
 * ----------- UPDATE MESSAGE STATUS -------------
 *
 ***************************************************/

- (void)updateReceivedMessageStatus{
    
    for (Inbox *inbox in self.inboxData){
        
        if ([inbox.isSeen isEqualToString:@"NO"] ){
            [self updateMessageStatus:inbox];
        }
    }
}





// An NSUserDefault key "Source" let us know which unwind segue to perform
- (IBAction)closePressed:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"Close Pressed with source: %@", [defaults valueForKey:@"Source"]);
    
    [self.collectionView resignFirstResponder];
    
    if ([[defaults valueForKey:@"Source"] isEqualToString:@"InboxView"]) {
        
        [self performSegueWithIdentifier:@"BackToInbox" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"BlockView"]){
        
        [self performSegueWithIdentifier:@"BackToBlock" sender:nil];
        
    }else if([[defaults valueForKey:@"Source"] isEqualToString:@"DashboardView"]){
        
        [self performSegueWithIdentifier:@"BackToDashboard" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"EditProfileView"]){
        
        [self performSegueWithIdentifier:@"BackToEditProfile" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"FaqView"]){
        
        [self performSegueWithIdentifier:@"BackToFaq" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"FlagView"]){
        
        [self performSegueWithIdentifier:@"BackToFlag" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"InviteView"]){
        
        [self performSegueWithIdentifier:@"BackToInvite" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"LegalView"]){
        
        [self performSegueWithIdentifier:@"BackToLegal" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"ProfileView"]){
        
        [self performSegueWithIdentifier:@"BackToProfile" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"SettingsView"]){
        
        [self performSegueWithIdentifier:@"BackToSettings" sender:nil];
    }
    else if([[defaults valueForKey:@"Source"] isEqualToString:@"SupportView"]){
        
        [self performSegueWithIdentifier:@"BackToSupport" sender:nil];
        
    } else if ([[defaults valueForKey:@"Source"] isEqualToString:@"BoostView"]) {
        
        [self performSegueWithIdentifier:@"BackToBoost" sender:nil];
        
    }else if ([[defaults valueForKey:@"Source"] isEqualToString:@"TermsView"]) {
            
            [self performSegueWithIdentifier:@"BackToTerms" sender:nil];
            
            
    
    }else{
        [self performSegueWithIdentifier:@"BackToSearch" sender:nil];
    }
    
    [SVProgressHUD dismiss];

}
#pragma mark - JSQMessagesViewController method overrides

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    
    [_chat connectAndSendMessage:_selectedUser.uid token:_selectedUser.fcmToken andName:_selectedUser.firstName message:text WithCompletion:^(BOOL success) {
        
        if (success){
            [SVProgressHUD dismiss];
            
//            JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
//                                                     senderDisplayName:senderDisplayName
//                                                                  date:date
//                                                                  text:text];
            [self.collectionView reloadData];
            [self finishSendingMessageAnimated:YES];
            
        }else{
            
            [SVProgressHUD dismiss];
            alert=   [UIAlertController
                      alertControllerWithTitle:@"Message failed"
                      message:@"Message failed to send"
                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"CANCEL"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }];
    
    
    
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    [self scrollToBottomAnimated:NO];
    
}




//#pragma mark - JSQMessages CollectionView DataSource

//
//- (void)collectionView:(JSQMessagesCollectionView *)collectionView
//                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
//{
//    NSLog(@"Load earlier messages!");
//    
//    if(![self getAllMsgsLoaded]){
//        
//        //increment page
//
//        [SVProgressHUD show];
////        [self loadMore];
//        
//    }
//}



- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self.messages objectAtIndex:indexPath.item];
    
}


//NOT USED AT THE MOMENT, CONFIGURE AND TEST WITH FIREBASE BEFORE ENABLING
- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Made it to Did Delete");
//    UIResponder *responder = [UIResponder responder:self
//                             selResponseHandler:@selector(responseHandler:)
//                                selErrorHandler:@selector(errorHandler:)];
    
//    NSSortDescriptor *sortDescriptor;
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created"
//                                                 ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    
//    NSArray *sortedArray = [self.inboxData sortedArrayUsingDescriptors:sortDescriptors];
    
    //this array always adds three objects to the beginning. Maybe becuase it's mutable?
//    Inbox *inbox = [self.messages objectAtIndex:indexPath.row];
    
//    if ([inbox.deletedBy isEqualToString:inbox.senderID] || [inbox.deletedBy isEqualToString:@"both"]){
//        inbox.deletedBy = @"both";
//        NSLog(@"Marking both have deleted messages");
//    }else{
//        inbox.deletedBy= backendless.userService.currentUser.objectId;
//        NSLog(@"Marking %@ has deleted: ", backendless.userService.currentUser.objectId);
//    }
//    
//    NSLog(@"creating datastore");
//    id<IDataStore> dataStore = [backendless.persistenceService of:[Inbox class]];
//    
//    NSLog(@"saving inbox");
//    [dataStore save:inbox responder:responder];
//    
//    NSLog(@"removing from collection view");
//    [self.messages removeObjectAtIndex:indexPath.row];

    
    [SVProgressHUD show];
    [self.collectionView reloadData];
    
    [self scrollToBottomAnimated:NO];
    [SVProgressHUD dismiss];

    
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{

    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubbleImageData;
    }
    
    return self.incomingBubbleImageData;
}



- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     
     */
    

    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    

    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor whiteColor];
        }
        else {
            cell.textView.textColor = [UIColor blackColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}



#pragma mark - UICollectionView Delegate
#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{

    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{

    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}


- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return nil;
    
}

#pragma mark - JSQMessages collection view flow layout delegate
#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events


- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Tapped message bubble!");
    NSLog(@"Message Count: %lu, Message Number: %ld", (unsigned long)self.messages.count, (long)indexPath.row);
    

    NSLog(@"Messages Size: %lu", (unsigned long)[self.messages count]);
    NSLog(@"Inbox Size: %lu", (unsigned long)[self.inboxData count]);
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));

}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods


- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}



-(void) removeMessageObservers{
    
    //Remove Firebase Observers
    if (![_conversationId isEqualToString:@""] && _conversationId != nil){
        FIRDatabaseReference *mRef = [[FirebaseManager getConversationPath] child:_conversationId];
        [mRef removeAllObservers];
    }
}


/************************************
 *
 * --- CHATOBJECT DELEGATE METHODS --
 *
 *************************************/

-(void)notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    [self.chat decrementHubCount];
}







-(void)textViewDidEndEditing:(UITextView *)textView{
    
    self.collectionView.frame = CGRectMake(0, navigationBar.frame.size.height, self.view.frame.size.width, self.collectionView.frame.size.height - navigationBar.frame.size.height);
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"Backing Out");
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"Segue in SearchViewController: %@", segue.identifier);
    
    [self.inputToolbar.contentView.textView resignFirstResponder];
    [self removeMessageObservers];
    
    if ([segue.identifier isEqualToString:@"BackToInbox"]) {

        InboxViewController *inboxVC = segue.destinationViewController;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"refreshInbox"];
        [defaults synchronize];

        [inboxVC setChat:_chat];
        [appDelegate.drawerController setChat:_chat];
        _chat.returned = self;
        _chat.delegate = appDelegate.drawerController;

    }

    
    
    else if ([segue.identifier isEqualToString:@"BackToSearch"]) {

        SearchViewController *searchVC = segue.destinationViewController;
        
        
        [appDelegate.drawerController setChat:_chat];
        [searchVC setChat:_chat];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"refreshInbox"];
        [defaults setBool:YES forKey:@"refreshSearch"];
        [defaults synchronize];

        
        _chat.returned = self;
        _chat.delegate = appDelegate.drawerController;

    }
    
    else if ([segue.identifier isEqualToString:@"BackToBlock"]) {
        
        BlockViewController *blockVC = segue.destinationViewController ;
        [blockVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = blockVC;

    }
    
   else  if ([segue.identifier isEqualToString:@"BackToDashboard"]) {

       Drawer *dashVC = segue.destinationViewController ;
       [appDelegate.drawerController setChat:_chat];
       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       [defaults setBool:YES forKey:@"refreshInbox"];
       [defaults synchronize];
        [dashVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = appDelegate.drawerController;

    }
    
    else if ([segue.identifier isEqualToString:@"BackToEditProfile"]) {
        
        EditProfileViewController *editVC = segue.destinationViewController ;
        [editVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = editVC;

    }
    
    else if ([segue.identifier isEqualToString:@"BackToFaq"]) {
        
        FaqViewController *faqVC = segue.destinationViewController;
        [faqVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = faqVC;
    }
    
    else if ([segue.identifier isEqualToString:@"BackToFlag"]) {
        
        FlagViewController *flagVC = segue.destinationViewController;
        [flagVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = flagVC;

    }
   else  if ([segue.identifier isEqualToString:@"BackToInvite"]) {
       
        InviteTableViewController *inviteVC = segue.destinationViewController;
        [inviteVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = inviteVC;

    }
    
    else if ([segue.identifier isEqualToString:@"BackToLegal"]) {
        LegalViewController *legalVC = segue.destinationViewController ;
        [legalVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = legalVC;

    }
    
    else if ([segue.identifier isEqualToString:@"BackToProfile"]) {
        
        ProfileViewController *profileVC = segue.destinationViewController ;
        [profileVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = profileVC;
    }
    else if ([segue.identifier isEqualToString:@"BackToSettings"]) {
        
        SettingViewController *settingsVC = segue.destinationViewController ;
        [settingsVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = settingsVC;

    }
    else if ([segue.identifier isEqualToString:@"BackToSupport"]) {
        
        SupportViewController *supportVC = segue.destinationViewController ;
        [supportVC setChat:_chat];
        _chat.returned = self;
        _chat.delegate = supportVC;

    }
    
    _chat = nil;
}



@end
