//
//  BlockViewController.h
//  Accomplice
//
//  Created by ibuildx on 5/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoMessagesViewController.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "UserObject.h"
#import "FirebaseManager.h"

@interface BlockViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ChatProtocol>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *logOutBtn;

@property (weak, nonatomic) IBOutlet UIView *shadowView;

@property (weak, nonatomic) IBOutlet UILabel *noBlockedLbl;

@property (strong) NSMutableArray  *usersFound;

@property (strong) NSArray  *flatColors;



- (IBAction)flagAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;


@property (strong) UserObject *selectedUser;

@property NSString * userId;

@property (nonatomic, strong) ChatObject *chat;

@property double currentPage;
@property double deletedPage;

@property NSMutableDictionary *cachedImages;

@end
