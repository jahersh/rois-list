#import <Foundation/Foundation.h>
#import "Inbox.h"
#import "UserObject.h"


@import FirebaseMessaging;
@import FirebaseAuth;
@import FirebaseDatabase;



@protocol ChatProtocol

@optional

//-(void)getMessage:(NSString *)message fromUser:(NSString *)user :(NSString *)msgId;
//-(void)getError:(Fault *)error;
//-(void)getMessageNotificationFromUser :(NSString *)user :(NSString *)msgId;
-(void)notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString*)conversationId msg:(NSString*)msg title:(NSString*)title;

@end


@interface ChatObject : NSObject
@property (nonatomic, weak) id<ChatProtocol> delegate;
@property (nonatomic, weak) id<ChatProtocol> returned;

//-(void)publish:(NSString *)message toRecieverId :(NSString *)recieverId andDeviceId :(NSString *)deviceId ;
//-(void)updateUserDefaultsList:(NSString*)usr name:(NSString*)name;
-(void)connectAndSendWave :(NSString *)userId token:(NSString*)token andName :(NSString *)userName WithCompletion:(void (^)(BOOL success))completion;
-(void)connectAndSendMessage :(NSString *)userId token:(NSString*)token andName :(NSString *)userName message:(NSString*)message WithCompletion:(void (^)(BOOL success))completion;


@property BOOL showTSMessage;

@property (strong) UserObject *currentUser;
@property (strong) UserObject *selectedUser;
@property int badgeCount;
@property NSString *connectTouserId;

-(void) incrementHubCount;
-(void) decrementHubCount;


@end
