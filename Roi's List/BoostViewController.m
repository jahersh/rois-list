//
//  BoostViewController.m
//  Roi's List
//
//  Created by Justin Hershey on 10/5/17.
//  Copyright © 2017 Justin Hershey. All rights reserved.
//

#import "BoostViewController.h"

@interface BoostViewController ()

@end


@implementation BoostViewController


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    _kBoostedProfileProductId = @"com.rois.list.automonthly";
    
    self.picker.delegate = self;
    self.picker.dataSource = self;
    self.picker.showsSelectionIndicator = YES;

    [self setupView];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)setupView{
    
    [self.picker setHidden:YES];
    self.purchaseMoreBtn.clipsToBounds = YES;
    self.purchaseMoreBtn.layer.cornerRadius = 15.0;
    self.purchaseMoreBtn.layer.borderWidth = 2.0;
    self.purchaseMoreBtn.layer.borderColor = [DataManager getGreenColor].CGColor;
    
    self.boostDaysLeftLbl.text = [NSString stringWithFormat:@"Boost Days Remaining: %@", self.chat.currentUser.boostDaysRemaining];
    
    if ([self.chat.currentUser.boostedProfile isEqualToString:@"1"]) {
        
        self.startStopBtn.backgroundColor = [UIColor lightGrayColor];
        [self.startStopBtn setTitle:@"Stop" forState:UIControlStateNormal];
        [self startTimer];
    }else{
        
        self.startStopBtn.backgroundColor = [DataManager getGreenColor];
        [self.startStopBtn setTitle:@"Start" forState:UIControlStateNormal];
    }
    
    
    NSString *profileBoosted = self.chat.currentUser.boostedProfile;
    double sExpireTime = [self.chat.currentUser.boostExpireTime doubleValue];
    
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    if ([profileBoosted isEqualToString: @"1"] && (nowMillis < sExpireTime) && ![self.chat.currentUser.boostExpireTime isEqualToString:@""]){
        
        self.timeRemainingLbl.text = @"Boost Time Left: 00:00:00";
        
        
        if(sExpireTime > nowMillis){
            
            self.interval = sExpireTime - nowMillis;
            self.sExpireTime = sExpireTime;
            
            [self startTimer];
            
        }
    }
}





- (IBAction)startStopAction:(id)sender {
    
    if ([self.startStopBtn.titleLabel.text isEqualToString:@"Start"]) {
        
        //check for expireTime, if still not expired, select days, otherwise just turn on profile boost
        [self selectDays];
        
    }else{
        [self stopWarning];
    }
}




- (IBAction)purchaseAction:(id)sender {
    
    NSString *days = self.chat.currentUser.boostDaysRemaining;
    int daysLeft = [days intValue];
    
    self.chat.currentUser.boostDaysRemaining = [NSString stringWithFormat:@"%d", daysLeft + 5];
    
    [FirebaseManager updateFirebaseUserValue:@"boostDaysRemaining" value:self.chat.currentUser.boostDaysRemaining uid:self.chat.currentUser.uid];
    
    self.boostDaysLeftLbl.text = [NSString stringWithFormat:@"Boost Days Remaining: %@", self.chat.currentUser.boostDaysRemaining];
}




-(void) selectDays{
    
    [self.picker reloadAllComponents];
    [self.picker setHidden:NO];
    
}




- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSAttributedString *attString = [[NSAttributedString alloc] init];
    long longRow = (long)row;
    
    
    
    if (component == 0 ){
        
        if ([self.chat.currentUser.subscriptionPurchased isEqualToString:@"1"]){
            if (row == 0){
                NSString *title = [NSString stringWithFormat:@"%ld day", (longRow+1)];
                NSAttributedString *attString =
                [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
                
                return attString;
            }else{
                
                NSString *title = [NSString stringWithFormat:@"%ld days", (longRow+1)];
                NSAttributedString *attString =
                [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
                
                return attString;
            }
        }else{
            int day = row + 1;
            if (day < [self.chat.currentUser.boostDaysRemaining
                        intValue]){
                
                if (row == 0){
                    NSString *title = [NSString stringWithFormat:@"%ld day", (longRow+1)];
                    NSAttributedString *attString =
                    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
                    
                    return attString;
                    
                }else{
                    NSString *title = [NSString stringWithFormat:@"%ld days", (longRow+1)];
                    NSAttributedString *attString =
                    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
                    
                    return attString;
                }
            }else{
                
                if (row == 0){
                    
                    NSString *title = [NSString stringWithFormat:@"%ld day", (longRow+1)];
                    NSAttributedString *attString =
                    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
                    
                    return attString;
                    
                }else{
                    NSString *title = [NSString stringWithFormat:@"%ld days", (longRow+1)];
                    NSAttributedString *attString =
                    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
                    
                    return attString;
                }
                
            }
            
            
        }
    }
    
    return attString;
}



- (void)pickerView:(UIPickerView *)pV didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    long longRow = (long)row;
    
    if (component == 0 ){
        
        int days = longRow + 1;
        NSString *daysToUse = [NSString stringWithFormat:@"%d", days];
        
        if (days < [self.chat.currentUser.boostDaysRemaining
             intValue]){
            if ([self.chat.currentUser.subscriptionPurchased isEqualToString:@"1"]){
                
                [self startWarning:daysToUse];
            }else{
                
                [self nonPremiumUserWarning];
            }
        }else{
            [self startWarning:daysToUse];
        }
    }else{
        [self.picker setHidden:YES];
    }
}





- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSString *days = self.chat.currentUser.boostDaysRemaining;
    NSUInteger daysLeft = [days integerValue];
    
    return daysLeft;
}







/**************************************
 *
 *      TIMER COUNTDOWN METHODS
 *
 **************************************/

//Will update the user object and firebase with a new expiretime as well as days remaining
- (void)updateExpireTime: (NSString*)days {
    
    int dayCount = [days integerValue];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    double sExpireTime = [self.chat.currentUser.boostExpireTime doubleValue];
    NSDate *expireDate = [NSDate dateWithTimeIntervalSince1970:sExpireTime];
    NSString *currentDaysString = self.chat.currentUser.boostDaysRemaining;
    
    
    int currentDaysInt = [currentDaysString intValue];
    currentDaysInt = currentDaysInt - dayCount;
    
    if(sExpireTime > nowMillis){
        
        sExpireTime = [DataManager convertToMillisSince1970:[calendar dateByAddingUnit: NSCalendarUnitDay value:dayCount toDate: expireDate options:0]];
        
    }else{
        
        sExpireTime = [DataManager convertToMillisSince1970:[calendar dateByAddingUnit: NSCalendarUnitDay value:dayCount toDate:now options:0]];
        
    }
    
    self.interval = sExpireTime - nowMillis;
    self.sExpireTime = sExpireTime;
    
    self.chat.currentUser.boostExpireTime = [NSString stringWithFormat:@"%0.f", sExpireTime];
    self.chat.currentUser.boostDaysRemaining = [NSString stringWithFormat:@"%d",[self.chat.currentUser.boostDaysRemaining intValue] - dayCount];
    self.boostDaysLeftLbl.text = [NSString stringWithFormat:@"Boost Days Left: %@",self.chat.currentUser.boostDaysRemaining];
    
    [FirebaseManager updateFirebaseUserValue:@"boostDaysRemaining" value:self.chat.currentUser.boostDaysRemaining uid:self.chat.currentUser.uid];
    
    [FirebaseManager updateFirebaseUserValue:@"boostExpireTime" value:self.chat.currentUser.boostExpireTime uid:self.chat.currentUser.uid];
    
    self.boostDaysLeftLbl.text = [NSString stringWithFormat:@"Boost Days Remaining: %d",currentDaysInt];
    
    [self startTimer];
    
}




-(void) startTimer{
    
    if (_timer == nil){
        
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
        
    }else{
        
        [_timer invalidate];
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
    
}


-(void) refreshTimer{
    
    NSInteger ti = (NSInteger)self.interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    self.timeRemainingLbl.text = [NSString stringWithFormat:@"Boost Time Left: %02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    
    self.interval = self.sExpireTime - nowMillis;
    
}






/**************************************
 *
 *      START/STOP Warnings
 *
 **************************************/




-(void)nonPremiumUserWarning{

    
    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:[NSString stringWithFormat:@"Premium Content"]
                                  message:@"Only Premium users can use boost for days less than their remaining days. Go Premium from settings"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {

                             
                             
                         }];
//    UIAlertAction* cancel = [UIAlertAction
//                             actionWithTitle:@"Cancel"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                 
//                             }];
//    
    [alert addAction:ok];
//    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];


}

-(void)startWarning: (NSString*)days{
    int daysCount = [days integerValue];
    NSString *dayString = @"days";
    
    if (daysCount < 2){
        dayString = @"day";
    }
    
    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:[NSString stringWithFormat:@"Start %@ %@ Boost",days, dayString ]
                                  message:@"Are you sure you want to start your Boost Period? This cannot be undone. Any fraction of a day used will be charged as a full day."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Start"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [FirebaseManager updateFirebaseUserValue:@"boostedProfile" value:@"1" uid:self.chat.currentUser.uid];
                             self.chat.currentUser.boostedProfile = @"1";
                             
                             [self updateExpireTime:days];
                             
                             self.startStopBtn.backgroundColor = [UIColor lightGrayColor];
                             [self.startStopBtn setTitle:@"Stop" forState:UIControlStateNormal];
                             [self.picker setHidden:YES];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}




-(void)stopWarning{
    
    UIAlertController *alert =  [UIAlertController
                                 alertControllerWithTitle:@"Stop Boost"
                                 message:@"Are you sure you want to stop your Boost Period? This cannot be undone. Any fraction of a day used will be charged as a full day."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Stop"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [FirebaseManager updateFirebaseUserValue:@"boostedProfile" value:@"0" uid:self.chat.currentUser.uid];
                             [FirebaseManager updateFirebaseUserValue:@"boostExpireTime" value:@"0" uid:self.chat.currentUser.uid];
                             self.chat.currentUser.boostedProfile = @"0";
                             self.chat.currentUser.boostExpireTime = @"0";
                             
                             
                             self.timeRemainingLbl.text = @"Boost Time Left: 00:00:00";
                             self.startStopBtn.backgroundColor = [DataManager getGreenColor];
                             [self.startStopBtn setTitle:@"Start" forState:UIControlStateNormal];
                             
                             [self extrapolateRemainingTime];
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}




-(void)extrapolateRemainingTime{
    
    
    NSInteger ti = (NSInteger)self.interval;
    NSInteger hours = (ti / 3600);
    int recoupDays = hours / 24;

    //update local variables and firebase with new expiretime, daysRemaining time interval
    self.interval = 0;
    self.sExpireTime = 0;
    [self.timer invalidate];
    
    self.chat.currentUser.boostDaysRemaining = [NSString stringWithFormat:@"%d",[self.chat.currentUser.boostDaysRemaining intValue] + recoupDays];
    self.chat.currentUser.boostExpireTime = @"0";
    
    //update firebase
    [FirebaseManager updateFirebaseUserValue:@"boostExpireTime" value:self.chat.currentUser.boostExpireTime uid:self.chat.currentUser.uid];
    [FirebaseManager updateFirebaseUserValue:@"boostDaysRemaining" value:self.chat.currentUser.boostDaysRemaining uid:self.chat.currentUser.uid];
    
    self.boostDaysLeftLbl.text = [NSString stringWithFormat:@"Boost Days Remaining: %@", self.chat.currentUser.boostDaysRemaining];
}




/*****************************************************************************
 *
 *
 *__________________PURCHASE AGE HIDING_______________________
 *
 *
 *****************************************************************************/


-(void)fetchAvailableProducts{
    
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:_kBoostedProfileProductId,nil];
    _productsRequest = [[SKProductsRequest alloc]
                        initWithProductIdentifiers:productIdentifiers];
    
    _productsRequest.delegate = self;
    [_productsRequest start];
}



- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}



- (void)purchaseMyProduct:(SKProduct*)product{
    
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        [SVProgressHUD dismiss];
    }
    else{
        
        UIAlertController *alertP=   [UIAlertController
                                      alertControllerWithTitle:@"Purchases are disabled in your device"
                                      message:[NSString stringWithFormat:@""]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [SVProgressHUD dismiss];
                                 [alertP dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        [alertP addAction:ok];
        [self presentViewController:alertP animated:YES completion:nil];
    }
}



#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:_kBoostedProfileProductId]) {
                    
                    NSLog(@"Purchased ");
                    
                    //Add hours to profile
                    UserObject *user = self.chat.currentUser;
                    
                    [FirebaseManager updateFirebaseUserValue:@"subscriptionPurchased" value:@"1" uid:user.uid];
                    [self.chat.currentUser setSubscriptionPurchased:@"1"];
                    
                    UIAlertController *alertQ=   [UIAlertController
                                                  alertControllerWithTitle:@"Purchase completed succesfully"
                                                  message:[NSString stringWithFormat:@""]
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alertQ dismissViewControllerAnimated:YES completion:nil];
                                         }];
                    
                    [alertQ addAction:ok];
                    [self presentViewController:alertQ animated:YES completion:nil];
                }
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                break;
                
            default:
                break;
        }
    }
}




-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    
    if (count>0) {
        _validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        if ([validProduct.productIdentifier
             isEqualToString:_kBoostedProfileProductId]) {
        }
    } else {
        
        UIAlertController *alertR=   [UIAlertController
                                      alertControllerWithTitle:@"Not Available"
                                      message:[NSString stringWithFormat:@"No products to purchase"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertR dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        [alertR addAction:ok];
        [self presentViewController:alertR animated:YES completion:nil];
        
    }
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"unwindToList"]){
        
        DashBoardViewController *dashView = segue.destinationViewController;
        [dashView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = dashView;
    }
}




@end
