#import "ChatObject.h"
#import "DataManager.h"
#import "AppDelegate.h"


@interface ChatObject()
{
    
    BOOL connectionStatus;

}


@end

@implementation ChatObject

@synthesize connectTouserId;


-(instancetype) init{
    self = [super init];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationSelected:) name:@"notificationSelected" object:nil];

    return self;
}

/**********************************************************
 *
 * ---------- MESSAGE RECEIVED RESPONSE HANDLER -----------
 *
 **********************************************************/

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:@"notificationSelected"];
//    [[NSNotificationCenter defaultCenter] removeObserver: self];
    
}


-(void)notificationSelected:(NSNotification*)notification{
    
    NSLog(@"Chat Object Notification Observed");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *senderId  = [defaults valueForKey:@"selectedUserId"];
    NSString *convoId = [defaults valueForKey:@"selectedConversationId"];
    NSString *type = [defaults valueForKey:@"selectedType"];
    NSString *msg = [defaults valueForKey:@"notificationMsg"];
    NSString *title = [defaults valueForKey:@"notificationTitle"];
    
    [self.delegate notificationTouched:type fromUser:senderId conversationId:convoId msg:msg title:title];

}







-(void)notificationTouched:(NSString *)type fromUser:(NSString *)fromUser msgId:(NSString *)msgId conversationId:(NSString*)conversationId msg:(NSString*)msg title:(NSString*)title{
    
    [self incrementHubCount];
    
    [self updateInboxView:conversationId fromUser:fromUser];
    
    
    
}


/***************************************************
 *
 * ---------- USER CONNECTION METHODS ------------
 *
 ***************************************************/

-(void)connectAndSendWave :(NSString *)userId token:(NSString*)token andName :(NSString *)andName WithCompletion:(void (^)(BOOL success))completion{
    NSLog(@"connectAndSendWave -> connectToUser: %@", userId);
    NSLog(@"connectAndSendWave -> token: %@", token);
    NSLog(@"connectAndSendWave -> name: %@", andName);
    
    connectionStatus = NO;
    connectTouserId = userId;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];

    [FirebaseManager setInboxMessage:@"-1-2-3-4-5" recipient:userId completion:^(NSString *conversationId) {
        
        [FirebaseManager postNotification:token conversationId:conversationId forName:self.currentUser.firstName withMessage:@"-1-2-3-4-5"];
        completion(YES);
        
    }];
    
}



-(void)connectAndSendMessage :(NSString *)userId token:(NSString*)token andName :(NSString *)andName message:(NSString*)message WithCompletion:(void (^)(BOOL success))completion{
    
    NSLog(@"connectAndSendWave -> connectToUser: %@", userId);
    NSLog(@"connectAndSendWave -> token: %@", token);
    NSLog(@"connectAndSendWave -> name: %@", andName);
    
    connectionStatus = NO;
    connectTouserId = userId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    [FirebaseManager setInboxMessage:message recipient:userId completion:^(NSString *conversationId) {
        
        [FirebaseManager postNotification:token conversationId:conversationId forName:self.currentUser.firstName withMessage:message];
        completion(YES);
        
    }];
    
}



/***********************************************
 *
 * --------- HUB MANIPULATION METHODS ----------
 *
 ***********************************************/

- (int) getHubCount {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    return  [(SearchViewController*)appDelegate.drawerController.centerViewController getHubCount];;
    
}

-(void) decrementHubCount {
    
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [(SearchViewController*)appDelegate.drawerController.centerViewController decrementHubCount];
    
}

-(void) incrementHubCount {
    
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [(SearchViewController*)appDelegate.drawerController.centerViewController incrementHubCount];
    
}

-(void) updateInboxView:(NSString*)convoId fromUser:(NSString*)fromUser {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.openSide == MMDrawerSideRight){
        
        [(InboxViewController*)appDelegate.drawerController.rightDrawerViewController replaceWithMostRecentMsg:convoId userId:fromUser];
    }
}


@end

