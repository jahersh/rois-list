//
//  CaptionsCollectionViewCell.h
//  Complice
//
//  Created by Justin Hershey on 8/11/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CaptionsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *captionsLbl;

@end
