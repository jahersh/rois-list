//
//  InviteTableViewCell.m
//  Complice
//
//  Created by Justin Hershey on 11/1/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import "InviteTableViewCell.h"

@implementation InviteTableViewCell

- (void)awakeFromNib {
    
    
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    //This label should always be hidden and it's text is used to identify it's position in the masterPath array
    [self.masterRowLbl setHidden:YES];
    
    }

@end
