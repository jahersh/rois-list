//
//  UIView+ContractView.h
//  Complice
//
//  Created by Justin Hershey on 11/30/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContractProtocol <NSObject>



@end

@interface ContractView: UIView
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *declineBtn;
@property (weak, nonatomic) IBOutlet UITextView *contractText;

@property NSNumber *btnAction;

- (IBAction)acceptAction:(id)sender;
- (IBAction)declineAction:(id)sender;

@end
