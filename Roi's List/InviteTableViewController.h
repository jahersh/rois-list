//
//  UITableViewController+InviteTableViewController.h
//  Complice
//
//  Created by Justin Hershey on 11/1/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "ChatObject.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "FirebaseManager.h"


@interface InviteTableViewController: UIViewController<UITableViewDataSource, UITableViewDelegate, MFMessageComposeViewControllerDelegate, ChatProtocol, UISearchBarDelegate>

@property BOOL isRemovingTextWithBackspace;

//contacts array for cells currently being displayed
@property (strong, nonatomic) IBOutlet NSMutableArray *contacts;


@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (weak, nonatomic) IBOutlet UIAlertController *alert;

//button to send invites, will only appear when a cell is selected
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
- (IBAction)sendAction:(id)sender;


@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) ChatObject *chat;
@property (strong) UserObject *currentUser;
@property (strong) UserObject *selectedUser;

//saves data in masterRowLbl so we can easily merge the master selected users list with the selected searched results to hightlight and checkmark appropriately
@property int rowId;

@end
