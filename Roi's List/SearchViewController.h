//
//  InboxView.h
//  LiveChatWithNearest
//
//  Created by ibuildx on 2/29/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoMessagesViewController.h"
#import "INTULocationManager.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <MMDrawerController.h>
#import <RKNotificationHub.h>
#import "NetworkCheck.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "GeoFire.h"


@interface SearchViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,JSQDemoViewControllerDelegate, UIGestureRecognizerDelegate, ChatProtocol, UISearchBarDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;
@property RKNotificationHub *hub;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property BOOL gotoChatView;
@property MMDrawerController *drawerController;

@property (weak, nonatomic) IBOutlet UIView *openDrawerBackground;
@property NetworkCheck *networkConnection;

@property (weak, nonatomic) IBOutlet UIImageView *openDrawerBtn;

//refresh Control views and variables
@property (strong) UIRefreshControl *refreshControl;
@property (strong) UIView *refreshView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property NSMutableArray <UILabel *> *labelsArray;

@property (strong) NSMutableArray  *usersFound;
@property (strong) NSMutableArray *masterSearchList;
@property (strong) GeoFire *geoFire;
 
@property (strong) UserObject *currentUser;

@property (weak) UserObject *selectedUser;
@property (weak, nonatomic) IBOutlet GADBannerView *adView;

@property (strong) NSArray  *flatColors;

@property (nonatomic, strong) ChatObject *chat;
@property double currentPage;
@property double pageSize;
@property double totalPages;
@property NetworkCheck *reach;

@property BOOL contractAccepted;
- (IBAction)inboxAction:(id)sender;


-(void)decrementHubCount;
-(void)incrementHubCount;
-(int)getHubCount;
-(void) resignSearchResponder;


@property (weak) NSString * userId;
@property  (weak) NSString  *checkGender;
@property (weak) UITextView *contractText;
@property BOOL noMoreUsers;

@property NSMutableDictionary *cachedImages;
@property (assign, nonatomic) INTULocationRequestID locationRequestID;

-(void)refresh:(UIRefreshControl*)refreshControl;

@end
