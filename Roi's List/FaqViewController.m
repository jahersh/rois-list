//
//  UIViewController+FaqViewController.m
//  Complice
//
//  Created by Justin Hershey on 11/8/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import "FaqViewController.h"
#import <TSMessage.h>
#import "SupportViewController.h"
#import "IntroViewController.h"
#import "UIViewController+CWPopup.h"

@interface FaqViewController (){
    
}

@end
@implementation FaqViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    _webView.delegate = self;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FAQs" ofType:@"html"];
    
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    self.tutorialBtn.layer.cornerRadius = 15;
    self.tutorialBtn.clipsToBounds = NO;
    self.tutorialBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tutorialBtn.layer.shadowOpacity = 0.4;
    self.tutorialBtn.layer.shadowRadius = 8;
    self.tutorialBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.tutorialBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tutorialBtn.titleLabel.layer.shadowRadius = 5;
    self.tutorialBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.tutorialBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    //reverse commenting out on next two lines after done testing
    _webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    [_webView loadRequest:request];
    
}

//-(void)applicationDidEnterBackground:(NSNotification *)notification{
//    
//    _chat.showTSMessage = NO;
//}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationIsActive:)
//                                                 name:UIApplicationDidBecomeActiveNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationDidEnterBackground:)
//                                                 name:UIApplicationDidEnterBackgroundNotification
//                                               object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}

//
//- (void)applicationIsActive:(NSNotification *)notification {
//    NSLog(@"Application Did Become Active");
//    if(!notification){
//        _chat.showTSMessage = YES;
//    }
//}


- (IBAction)tutorialAction:(id)sender {
    
    [self showEAIntroPage];
}


-(void)showEAIntroPage {
    
    IntroViewController *introVC = [[IntroViewController alloc] initWithNibName:@"IntroPage" bundle:nil];
    
    introVC.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    
    
    [self presentPopupViewController:introVC animated:YES completion:^(void) {
        
        
        [introVC.skipButton addTarget:self action:@selector(introDidFinish) forControlEvents:UIControlEventTouchUpInside];
        
        
    }];
    
    
    
    
    
}


- (void)introDidFinish{
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:@"SecondRun"];
            [defaults synchronize];
            NSLog(@"Intro Finished...........");
        }];
    }
}


// so that tapping popup view doesnt dismiss it
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return touch.view == self.view;
}




/*****************************************
 *
 * ----- CHABOBJECT DELEGATE METHOD -----
 *
 *****************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                           
                                           
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}



/*****************************************
 *
 * ------------- NAVIGATION --------------
 *
 *****************************************/


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"backToSupport"]) {
        SupportViewController *supportView = segue.destinationViewController;
//        dashView.fbObject = self.fbObject;
        
        [supportView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = supportView;
    }
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"FaqView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;

    }
    _chat = nil;
}

- (IBAction)unwindToFaq:(UIStoryboardSegue *)unwindSegue
{
    //    [self updateBadgesView];
    
}



@end
