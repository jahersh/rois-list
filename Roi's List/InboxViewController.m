//
//  InboxViewController.m
//  Accomplice
//
//  Created by ibuildx on 4/19/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "InboxViewController.h"
#import "InboxTableViewCell.h"
#import "DataManager.h"
#import "DemoMessagesViewController.h"
#import "ChatObject.h"
#import "Inbox.h"
#import <SVProgressHUD.h>
#import <TSMessage.h>
#import "SearchViewController.h"
#import "BlockUser.h"
#import "AppDelegate.h"
#import "DashBoardViewController.h"
#import "InboxProfPopup.h"



@interface InboxViewController () 
{
    InboxProfPopup *inboxProfPopup;
}
@end



@implementation InboxViewController


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    

    self.adView.adUnitID = @"ca-app-pub-6416144291405618/9693659683";
    self.adView.rootViewController = self;
    [self.adView loadRequest:[GADRequest request]];

    
    [self getChatUser];
    NSLog(@"user Obj Id = %@",self.chat.currentUser.uid);
    
    [_noConvoLbl setHidden:YES];
    [self tableViewIntegration];
    [_deleteBtn setHidden:YES];
    
    _cachedImages = [[NSMutableDictionary alloc] init];
    
    [SVProgressHUD show];
    
    _inboxList = [[NSMutableDictionary alloc] init];
    _usersFound = [[NSMutableDictionary alloc] init];
    
    //array for search
    _uniqueMsgs = [[NSMutableArray alloc] init];
    
    //master array
    _allUniqueMsgs = [[NSMutableArray alloc] init];
    
    self.searchBar.delegate = self ;
    [self.tableView reloadData];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = .5;
    [self.tableView addGestureRecognizer:lpgr];

    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor = [UIColor blackColor];
    _refreshControl.backgroundColor = [UIColor clearColor];
    _refreshView.frame = _refreshControl.frame;
    
    [self.tableView addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];

}



-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    
    [self resignSearchResponder];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL refresh = [defaults boolForKey:@"refreshInbox"];

    //search bar icons update
    [self.searchBar setImage:[UIImage imageNamed:@"search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.searchBar setImage:[UIImage imageNamed:@"clearSearch"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    
    
    if(refresh){
        
//        [SVProgressHUD show];
        //UPDATE THE INBOX
        [self refresh:self.refreshControl];
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [_toDelete removeAllObjects];
    [self.deleteBtn setHidden:YES];
    
    NSArray *selectedCells = self.tableView.indexPathsForSelectedRows;
    
    for(NSIndexPath *ip in selectedCells){
        
        InboxTableViewCell *cell = [self.tableView cellForRowAtIndexPath:ip];
        cell.selected = NO;
        
    }
    [self.tableView reloadData];
    
    [SVProgressHUD dismiss];
    [self.searchBar resignFirstResponder];

}


-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([self.chat.currentUser.subscriptionPurchased isEqualToString:@"1"]){
        
        [self.adView setHidden:YES];
        
    }else{
        [self.adView setHidden:NO];

    }
}

-(void) dealloc {
    
    _toDelete = nil;
    _uniqueMsgs = nil;
    _allUniqueMsgs = nil;
    _inboxList = nil;

}
    
-(void)resignSearchResponder{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
    [(SearchViewController*)appDelegate.drawerController.centerViewController resignSearchResponder];
        
}

//-- Functions to perform when pulling down in the SearchView Table to refresh
- (void)refresh:(UIRefreshControl *)refreshControl {
    
    [FirebaseManager updateInboxList:^(NSDictionary *inboxList) {
        
        _inboxList = [[NSMutableDictionary alloc] init];
        
        self.inboxList = inboxList.mutableCopy;
        self.toDelete = [[NSMutableArray alloc] init];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:NO forKey:@"refreshInbox"];
        [defaults synchronize];
        
        _uniqueMsgs = [[NSMutableArray alloc] init];
        _allUniqueMsgs = [[NSMutableArray alloc] init];
        
        [self fetchingUsersInboxAsync];
    }];
}



-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    
    if (indexPath == nil) {
        
        NSLog(@"long press on table view but not on a row");
        
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        InboxTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        if (cell.selected){
            
            cell.selected = NO;
            
            [self setCellShadow:cell];
            cell.backgroundColor = _previousColor;
            
            for(NSIndexPath *indexP in _toDelete){
                if (indexP == indexPath){
                    [_toDelete removeObject:indexP];
                }
            }
            
            if([_toDelete count] == 0){
                
                [_deleteBtn setHidden:YES];
            }
            
        }else{
            
            cell.selected = YES;
            
            [_toDelete addObject:indexPath];
            
            for(NSIndexPath *ip in _toDelete){
                NSLog(@"IndexPath: %@", ip);
            }
            self.previousColor = cell.backgroundColor;
            cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:52.0/255.0 blue:0.0/255.0 alpha:1];
            [self removeCellShadow:cell];
            [_deleteBtn setHidden:NO];
            
        }
        
        NSLog(@"long press on table view at row %ld", (long)indexPath.row);
        
        
    } else {
        NSLog(@"gestureRecognizer.state = %ld", (long)gestureRecognizer.state);
    }
}


/*****************************************
 *
 * ----------- DELETE METHODS ------------
 *
 *****************************************/

-(void)removeFromInboxList: (NSString *)userToUpdate {
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    [[[[FirebaseManager getInboxListPath] child:uid] child:userToUpdate] removeValue];
    
    
    [self.tableView reloadData];
    
}




-(void)deleteSelectedCells {
    
    NSLog(@"Action to perform with Delete");
    
    
    //deleting user from table dataSource
    NSMutableIndexSet *indexesToRemove = [NSMutableIndexSet indexSet];
    
    for (NSIndexPath* index in _toDelete){
        
        Inbox *userThreadToDelete = [_uniqueMsgs objectAtIndex:index.row];
        
        UserObject *user = self.chat.currentUser;
        NSString *lSenderID = userThreadToDelete.senderID;
        NSString *lReceiverID = userThreadToDelete.receiverID;
        NSString *userToRemove = @"";
        
        //setting the objectId for the userToRemove
        if ([user.uid  isEqualToString:lSenderID]){
            userToRemove = lReceiverID;
        }else{
            userToRemove = lSenderID;
        }
        
        [indexesToRemove addIndex:index.row];
        
        [_uniqueMsgs removeObjectsAtIndexes:indexesToRemove];
        [_allUniqueMsgs removeObjectsAtIndexes:indexesToRemove];
        
        [self removeFromInboxList:userToRemove];
        
        
        
        
    }
}



/*****************************************
 *
 * -------- UI CONFIG METHODS ---------
 *
 *****************************************/

-(void)setCellShadow:(InboxTableViewCell*)cell{
    
    cell.clipsToBounds = NO;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOpacity = 0.6;
    cell.layer.shadowOffset = CGSizeMake(4.0f, 4.0f);
    
}


-(void)removeCellShadow:(InboxTableViewCell*)cell{
    
    cell.clipsToBounds = YES;
    
}



/*****************************************
 *
 * ----- TABLEVIEW DELEGATE METHODS -----
 *
 *****************************************/



-(UIImage*)imageForID:(NSString*)imgUrl{
    
//    __block NSString *facebookID = imgUrl;
    __block UIImage *image = [_cachedImages valueForKey:imgUrl];
    
    if (!image) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
//            NSString *fbPicString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=small",facebookID];
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
            
            image = [UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_cachedImages setObject:image forKey:imgUrl];
                [self.tableView reloadData];
                
            });
        });
    }
    return image;
}


-(void)tableViewIntegration {
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _uniqueMsgs.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"InboxTableViewCell";
    InboxTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [cell.userImageView setImage:nil];
//    if (cell == nil) {
//        cell = [[InboxTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                         reuseIdentifier:CellIdentifier];
//    }
    
    [self setCellShadow:cell];
    
    // Sort setup
    Inbox *inbox = [_uniqueMsgs objectAtIndex:indexPath.row];
    NSLog(@"Inbox for position %ld is ________ %@", (long)indexPath.row, inbox);
    
    if ([inbox.messageBody isEqualToString:@"-1-2-3-4-5"]) {
        
        if ([inbox.receiverID isEqualToString:self.chat.currentUser.uid]) {
            cell.userIntro.text = @"Introduced themself";
        }
        else{
            cell.userIntro.text = @"You Introduced Yourself";
        }
    }
    else {
        cell.userIntro.text = inbox.messageBody;
    }

    //set the white dot for convos that have unread messages but the last was sent by you
    
    if([inbox.senderID isEqualToString:self.chat.currentUser.uid]){
        
        cell.unseenMsgView.hidden = YES;
        UIFont *newFont = [UIFont fontWithName:@"Arial" size:14.0f];
        cell.unseenMsgView.hidden = YES;
        cell.userIntro.font = newFont;
        
    }else if ([inbox.isSeen isEqualToString:@"NO"] && ![inbox.senderID isEqualToString:self.chat.currentUser.uid]){
        
        UIFont *newFont = [UIFont fontWithName:@"AvenirNextCondensed-Bold" size:16.0f];
        cell.unseenMsgView.hidden = NO;
        cell.userIntro.font = newFont;
        
    }else if ([inbox.isSeen isEqualToString:@"YES"]){
        cell.unseenMsgView.hidden = YES;
        UIFont *newFont = [UIFont fontWithName:@"Arial" size:14.0f];
        cell.unseenMsgView.hidden = YES;
        cell.userIntro.font = newFont;
        
    }

        
    cell.userName.text =  inbox.sendingUser.firstName;
    [cell.userImageView setImage:[self imageForID:inbox.sendingUser.imgUrl]];
    
    cell.backGroundRoundedView.layer.cornerRadius = 10 ;
    cell.backGroundRoundedView.clipsToBounds = YES;
    
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width / 2;
    cell.userImageView.clipsToBounds = YES;
    
//    cell.backGroundRoundedView.backgroundColor = self.flatColors[indexPath.row % self.flatColors.count];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectedBackgroundView = nil;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    //so the inbox will refresh on return from the selected conversation view
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    Inbox *inbox = _uniqueMsgs[indexPath.row];
    self.selectedUser = inbox.sendingUser;
    [self performSegueWithIdentifier:@"segueModalDemoVC" sender:nil];
    
}


/****************************************
 *
 * ------ SEARCH DELEGATE METHODS ------
 *
 *****************************************/

-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    searchBar.showsCancelButton = true;
}


-(void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    searchBar.showsCancelButton = false;
}


-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.searchBar resignFirstResponder];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSLog(@"Search String = %@",searchBar.text);
    [_uniqueMsgs removeAllObjects];
    
    if ([searchBar.text isEqualToString:@""]) {
        [_uniqueMsgs addObjectsFromArray:_allUniqueMsgs];
        [self.tableView reloadData] ;
        return ;
    }
    
    for (Inbox *inbox in _allUniqueMsgs) {
        if ([inbox.sendingUser.firstName containsString:searchBar.text]) {
            [_uniqueMsgs addObject:inbox];
        }
    }
    [self.tableView reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"Search String = %@",searchBar.text);
    [searchBar resignFirstResponder];
    [_uniqueMsgs removeAllObjects];
    
    if ([searchBar.text isEqualToString:@""]) {
        [_uniqueMsgs addObjectsFromArray:_allUniqueMsgs];
        [self.tableView reloadData] ;
        return ;
    }
    
    for (Inbox *inbox in _allUniqueMsgs) {
        if ([inbox.sendingUser.firstName containsString:searchBar.text]) {
            [_uniqueMsgs addObject:inbox];
        }
    }
    [self.tableView reloadData] ;
}



#pragma mark - UI Buttons Method
/****************************************
 *
 * --------- ACTION METHODS -----------
 *
 ****************************************/


- (IBAction)backBtnAction:(id)sender {
    
    [self performSegueWithIdentifier:@"backToList" sender:nil];
    [SVProgressHUD dismiss];
    
}

- (IBAction)deleteAction:(id)sender {
    
    NSLog(@"Delete Button Pressed");
    _alertDelete=   [UIAlertController
                     alertControllerWithTitle:@"Warning"
                     message:@"Delete selected thread(s)?"
                     preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Delete Data and Logout
                             _deleteBtn.hidden = YES;
                             [self deleteSelectedCells];
                             [_alertDelete dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [_toDelete removeAllObjects];
                                 [_alertDelete dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [_alertDelete addAction:ok];
    [_alertDelete addAction:cancel];
    [self presentViewController:_alertDelete animated:YES completion:nil];
    
}


-(void)fetchingUsersInboxAsync {
    
    self.usersFound = [[NSMutableDictionary alloc] init];
    
    for(NSString *keyt in self.inboxList.allKeys){
        
        [FirebaseManager getUserFirebaseDataForUid:keyt completion:^(UserObject *userdata) {
            
            [self.usersFound setObject:userdata forKey:keyt];
            
            if(self.usersFound.count == self.inboxList.count){
                __block NSMutableArray *inboxArray = [[NSMutableArray alloc] init];
                
                NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
                NSDictionary *latestMessages = [defaults objectForKey:@"latestMessages"];
                NSLog(@"%@",latestMessages);
                
                for (NSString *key in latestMessages){
                    
                    Inbox *inbox = [[Inbox alloc] init];
                    NSDictionary *object = [_inboxList objectForKey:key];
                    [inbox dictionaryToInbox:object];
                    inbox.sendingUser = [self.usersFound objectForKey:key];
                    [inboxArray addObject:inbox];
                }
                
                
                inboxArray = [[inboxArray sortedArrayUsingComparator:^NSComparisonResult(NSString* a, NSString* b) {
                    NSString *first = [(Inbox*)a created];
                    NSString *second = [(Inbox*)b created];
                    return [second compare:first];
                }] mutableCopy];
                
                
                [self.uniqueMsgs addObjectsFromArray:inboxArray];
                [self.allUniqueMsgs addObjectsFromArray:inboxArray];
                
                [SVProgressHUD dismiss];
                
                if (inboxArray.count > 0){
                    [_noConvoLbl setHidden:YES];
                }else{
                    [_noConvoLbl setHidden:NO];
                }
                
                [self.tableView reloadData];
                [_refreshControl endRefreshing];
            }
        }];
    }
}





//on receiving a message in the inbox view, add to unseenMsgsDict, update Msg Arrays and reload the table
-(void) replaceWithMostRecentMsg:(NSString *)convoId userId:(NSString*)userId{

    [FirebaseManager getLatestMessage:convoId completion:^(Inbox *inbox) {
        
//        dispatch_async(dispatch_get_main_queue(), ^{
            int index = 0;
            for(Inbox *inboxT in _uniqueMsgs){
                
                if ([inboxT.senderID isEqualToString:userId]){
                    
                    [_uniqueMsgs replaceObjectAtIndex:index withObject:inbox];
                    [_allUniqueMsgs replaceObjectAtIndex:index withObject:inbox];
                }
                index += 1;
            }
        
            [self.tableView reloadData];
//        });
    }];
}
    



/*****************************************
 *
 * ----- CHATOBJECT DELEGATE METHODS -----
 *
 *****************************************/



-(void)notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    [self replaceWithMostRecentMsg:conversationId userId:fromUser];
    //do nothing
}


-(void) getChatUser{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.chat = [appDelegate.drawerController chat];
    
}



/*****************************************
 *
 * ----------- NAVIGATION ----------
 *
 *****************************************/



- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    _chat = appDelegate.drawerController.chat;
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = self.chat.currentUser;
        vc.selectedUser = self.selectedUser ;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.selectedUser.uid forKey:@"selectedUserId"];

        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];

        [defaults setValue:@"InboxView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = appDelegate.drawerController;
        _chat.delegate = segue.destinationViewController;
        
        NSLog(@"Selected User Obj Id = %@",self.selectedUser.uid);
        
        vc.delegateModal = self;
    }
    
    _chat = nil;
    
    [SVProgressHUD dismiss];
    
}


- (IBAction)unwindToInbox:(UIStoryboardSegue *)unwindSegue
{
    
}



@end
