//
//  Inbox.h
//  Complice
//

// Updated and maintained my Justin Hershey 11/2/2016
//  Copyright © 2017 Justin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"


@interface Inbox : NSObject

@property (nonatomic, strong) NSString *senderID;
@property (nonatomic, strong) NSString *receiverID;
@property (nonatomic, strong) NSString *isSeen;
@property (nonatomic, strong) NSString *messageBody;
@property (nonatomic, strong) NSString *created;

//@property (nonatomic, strong) NSString *isBlocked;
@property (nonatomic, strong) NSString *objectId;


// deletedBy determines who has marked the message for deletion values can be recevier, sender, both
//
// Message will be a candidate for deletion 30 days after last udpated date and has been deleted by Both
@property (nonatomic, strong) NSString *deletedBy;

//All messages will have this value marked as receiver, sender or both to denote who has deleted the thread. if 30 days after last updated and a value of Both. Messages will be removed from DB

@property (nonatomic, strong) UserObject *sendingUser;


-(NSDictionary*) inboxToDictionary;
-(void) dictionaryToInbox:(NSDictionary*)userdict;

@end
