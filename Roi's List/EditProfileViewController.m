//
//  EditProfileView.m
//  LiveChatWithNearest
//
//  Created by ibuildx on 2/29/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "EditProfileViewController.h"
#import "DashBoardViewController.h"
#import "DemoMessagesViewController.h"
#import <SVProgressHUD.h>
#import "UIViewController+CWPopup.h"
#import "AppDelegate.h"
#import <TSMessage.h>
#import "ProfileViewController.h"


@interface EditProfileViewController () <ChatProtocol, UIAlertViewDelegate>
{
    
    UIImage *selectedImage;
    UIAlertController *alert;
    UIImage *newPhoto;
}
@end

@implementation EditProfileViewController
@synthesize genderText;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - View Life Cycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.profileInfoText.delegate = self;
    [self.view layoutIfNeeded];
    [self maskTriananlgesAndCircularBorderedImage];
    self.bottomAlignImageConstraint.constant = self.profileImgView.frame.size.height / 2 ;
    
    [self setImage];
    
    self.nameAgeText.text = [NSString stringWithFormat:@"%@",self.chat.currentUser.firstName];
    self.profileInfoText.text = self.chat.currentUser.profileInfo ;
    
    
    [self setupView];
    
    _chat.delegate = self ;
    
}




-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.view layoutIfNeeded];
    [self maskTriananlgesAndCircularBorderedImage];
//    _chat.showTSMessage = NO;
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}


-(void) setImage{
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *fbPicString = self.chat.currentUser.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fbPicString]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.profileImgView setImage:[UIImage imageWithData:data]];
            
        });
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View Starting Methods

/********************************
 *
 * ----- UI SETUP METHODS -----
 *
 *******************************/


-(void)setupView{
    
    if ([self.profileInfoText.text isEqualToString:@"I'm looking for..."]){
        self.profileInfoText.text = @"I'm looking for...";
        self.profileInfoText.textColor = [UIColor lightGrayColor];
    }
    
    self.shadowView.layer.cornerRadius = 15;
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);

    
    self.saveProfileBtn.layer.cornerRadius = 15;
    self.saveProfileBtn.clipsToBounds = NO;
    self.saveProfileBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.saveProfileBtn.layer.shadowOpacity = 0.4;
    self.saveProfileBtn.layer.shadowRadius = 8;
    self.saveProfileBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.saveProfileBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.saveProfileBtn.titleLabel.layer.shadowRadius = 5;
    self.saveProfileBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.saveProfileBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.profileInfoText.delegate = self;
    self.profileInfoText.clipsToBounds = YES;
    self.profileInfoText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.profileInfoText.layer.shadowOpacity = 0.4;
    self.profileInfoText.layer.shadowRadius = 8;
    self.profileInfoText.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.nameAgeText.clipsToBounds = NO;
    self.nameAgeText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.nameAgeText.layer.shadowOpacity = 0.4;
    self.nameAgeText.layer.shadowRadius = 8;
    self.nameAgeText.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.nameText.clipsToBounds = NO;
    self.nameText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.nameText.layer.shadowOpacity = 0.4;
    self.nameText.layer.shadowRadius = 8;
    self.nameText.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.profileInfo.clipsToBounds = NO;
    self.profileInfo.layer.shadowColor = [UIColor blackColor].CGColor;
    self.profileInfo.layer.shadowOpacity = 0.4;
    self.profileInfo.layer.shadowRadius = 8;
    self.profileInfo.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.genderText.clipsToBounds = NO;
    self.genderText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.genderText.layer.shadowOpacity = 0.4;
    self.genderText.layer.shadowRadius = 8;
    self.genderText.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    [_uploadPhotoBtn setTitle:@"Choose Photo" forState:UIControlStateNormal];
    self.uploadPhotoBtn.layer.cornerRadius = 15;
    self.uploadPhotoBtn.clipsToBounds = NO;
    self.uploadPhotoBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.uploadPhotoBtn.layer.shadowOpacity = 0.4;
    self.uploadPhotoBtn.layer.shadowRadius = 8;
    self.uploadPhotoBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
//    _uploadPhotoBtn.size = CGSizeMake(100, 36);
    
    
}


-(void)maskTriananlgesAndCircularBorderedImage {
    // Build a triangular path
    
    self.profileImgView.layer.cornerRadius = self.profileImgView.frame.size.width / 2;
    self.profileImgView.clipsToBounds = YES;
    UIColor *bgColor = [UIColor whiteColor];
    self.profileImgView.layer.borderWidth = self.profileImgView.frame.size.width / 25;
    self.profileImgView.layer.borderColor = bgColor.CGColor;

}





#pragma mark - UI Buttons Actions

/*****************************************
 *
 * ----- ACTION METHODS -----
 *
 *****************************************/


- (IBAction)uploadPhotoAction:(id)sender {
    
    
    if ([_uploadPhotoBtn.titleLabel.text  isEqualToString:@"Choose Photo"]){
        
        self.picker = [[UIImagePickerController alloc] init];
        self.picker.delegate = self;
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker animated:YES completion:NULL];
        
    }else if([_uploadPhotoBtn.titleLabel.text  isEqualToString:@"Remove"]){
       
        newPhoto = nil;
        [self setImage];
        [_uploadPhotoBtn setTitle:@"Choose Photo" forState:UIControlStateNormal];
        
    }
}


- (IBAction)saveAction:(id)sender {


    NSLog(@"Current User Prof Info is ______ %@", self.chat.currentUser.profileInfo);
    NSLog(@"New User profile info will be ______ %@", self.profileInfoText.text);
    [SVProgressHUD show];
    
    self.chat.currentUser.profileInfo = self.profileInfoText.text;
    
    [FirebaseManager updateFirebaseUserValue:@"profileInfo" value:self.profileInfoText.text uid:self.chat.currentUser.uid];
    
    if (newPhoto != nil){
       
        [FirebaseManager uploadProfilePhoto:UIImageJPEGRepresentation(newPhoto, 0.1) completion:^(NSString *downloadString) {
            self.chat.currentUser.imgUrl = downloadString;
            [FirebaseManager updateFirebaseUserValue:@"imgUrl" value:downloadString uid:[[FIRAuth auth] currentUser].uid];
            [SVProgressHUD dismiss];
            [self performSegueWithIdentifier:@"ToSaveAndPreviewProfile" sender:self];
        }];
    }else{
        
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"ToSaveAndPreviewProfile" sender:self];
        
    }
    
    
    
    
}


- (IBAction)backAction:(id)sender {
    [self performSegueWithIdentifier:@"BackToHome" sender:sender];
}



#pragma mark - textView delegate
/****************************************
 *
 * ----- TEXTVIEW DELEGATE METHODS -----
 *
 *****************************************/


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    if([self.profileInfoText.text isEqualToString:@"I'm looking for..."]){
        self.profileInfoText.text = @"";
    }
    
    self.profileInfoText.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(self.profileInfoText.text.length == 0){
        self.profileInfoText.textColor = [UIColor lightGrayColor];
        self.profileInfoText.text = @"I'm looking for...";
        [self.profileInfoText resignFirstResponder];
    }
}


#define MAX_LENGTH 150 // Whatever your limit is
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    if(newLength <= MAX_LENGTH)
    {
        return YES;
    } else {
        NSUInteger emptySpace = MAX_LENGTH - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
}




#pragma mark - Navigation
/*****************************************
 *
 * ----- NAVIGATION -----
 *
 *****************************************/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"EditProfileView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;

        
        
    }
    else if([segue.identifier isEqualToString:@"BackToHome"])
    {
        DashBoardViewController *dashView = segue.destinationViewController;

        [dashView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = dashView;
        
    }else if ([segue.identifier isEqualToString:@"ToSaveAndPreviewProfile"]){
        
        ProfileViewController *pvc = segue.destinationViewController;
        pvc.chat = self.chat;
        pvc.chat.delegate = pvc;
        self.chat.returned = self;
        
    }
    _chat = nil;
    
}


- (IBAction)unwindToEditProfile:(UIStoryboardSegue *)unwindSegue
{
    //    [self updateBadgesView];
    
}

/*****************************************
 *
 * ----- IMAGEPICKER DELEGATE METHODS -----
 *
 *****************************************/


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //? do nothing
    [self.picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSLog(@"Did Choose Photo");
    NSLog(@"%lu",(unsigned long)info.count);
    self.profileImgView.image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    newPhoto = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    [_uploadPhotoBtn setTitle:@"Remove" forState:UIControlStateNormal];

    [self.picker dismissViewControllerAnimated:YES completion:NULL];
}



/*****************************************
 *
 * ----- CHABOBJECT DELEGATE METHODS -----
 *
 *****************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}


@end
