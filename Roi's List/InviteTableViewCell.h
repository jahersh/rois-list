//
//  UITableViewCell+InviteTableViewCell.h
//  Complice
//
//  Created by Justin Hershey on 11/1/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>

@interface InviteTableViewCell: UITableViewCell

//cell labels

@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *numberLbl;
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;

@property (weak, nonatomic) IBOutlet UILabel *masterRowLbl;


@end
