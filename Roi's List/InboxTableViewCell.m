//
//  InboxTableViewCell.m
//  Accomplice
//
//  Created by ibuildx on 4/19/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "InboxTableViewCell.h"
#import "AppDelegate.h"

@implementation InboxTableViewCell

- (void)awakeFromNib {
    
    // Initialization code
    [super awakeFromNib];
    
    [self setupView];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}


-(void)setupView{
    
    self.unseenMsgView.clipsToBounds = NO;
    self.unseenMsgView.layer.cornerRadius = self.unseenMsgView.frame.size.width/2;
    self.unseenMsgView.hidden = YES;
    
    self.userIntro.clipsToBounds = NO;
    self.userIntro.layer.shadowColor = [UIColor blackColor].CGColor;
    self.userIntro.layer.shadowOpacity = 0.3;
    self.userIntro.layer.shadowRadius = 2;
    self.userIntro.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.userName.clipsToBounds = NO;
    self.userName.layer.shadowColor = [UIColor blackColor].CGColor;
    self.userName.layer.shadowOpacity = 0.3;
    self.userName.layer.shadowRadius = 2;
    self.userName.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.userImageView.layer.borderColor = [DataManager getGreenColor].CGColor;
    self.userImageView.layer.borderWidth = 2;
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.shadowView2.clipsToBounds = NO;
    self.shadowView2.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView2.layer.shadowOpacity = 0.4;
    self.shadowView2.layer.shadowRadius = 5;
    self.shadowView2.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.unblockBtn.clipsToBounds = NO;
    self.unblockBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.unblockBtn.layer.shadowOpacity = 0.4;
    self.unblockBtn.layer.shadowRadius = 5;
    self.unblockBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.flagBtn.clipsToBounds = NO;
    self.flagBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.flagBtn.layer.shadowOpacity = 0.4;
    self.flagBtn.layer.shadowRadius = 5;
    self.flagBtn.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
   
}



@end
