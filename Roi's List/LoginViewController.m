//
//  LoginView.m
//  Complice
//
//  Created by ibuildx on 3/4/16.
//  Copyright © 2016 Justin Hershey. All rights reserved.
//

#import "LoginViewController.h"
#import "DashBoardViewController.h"
#import "UIViewController+CWPopup.h"
#import <CoreLocation/CoreLocation.h>
#import <SVProgressHUD.h>
#import "DataManager.h"
#import "SearchViewController.h"
#import "AppDelegate.h"
#import "InboxViewController.h"
#import "ContractViewController.h"
#import "BlockUser.h"
#import "IntroViewController.h"



@interface LoginViewController () <CLLocationManagerDelegate, FBSDKLoginButtonDelegate>
{
    
    CLLocationManager *locationManager;
    FBSDKLoginButton *loginButton;
    ContractViewController *contractView;
    UIAlertController *alert;
    
}
@end



@implementation LoginViewController

#pragma mark - View Life Cycle Methods

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    [DataManager locationIntegration:locationManager];
    
    _ref = [[FIRDatabase database] reference];
    
    CGFloat btnHeight = 28.0;
    
    loginButton = [[FBSDKLoginButton alloc] init];
    
    loginButton.readPermissions = @[
                                    @"email",
                                    @"public_profile"];
    
    
    
    loginButton.frame = CGRectMake(10, self.view.frame.size.height - (btnHeight + 20), self.view.frame.size.width - 20, btnHeight);
    loginButton.delegate = self;
    [self.view addSubview:loginButton];
    
    [self setupView];
    [self validLoginCheck];
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}



-(void)setupView{
    
    self.lockImage.layer.cornerRadius = 5.0;
    self.lockImage.clipsToBounds = YES;
    
    self.fbImage.layer.cornerRadius = 5.0;
    self.fbImage.clipsToBounds = YES;
    
    self.checkImage.layer.cornerRadius = 5.0;
    self.checkImage.clipsToBounds = YES;  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) validLoginCheck{
    
    [SVProgressHUD show];
    //    [[FIRAuth auth] addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
    
    if([[FIRAuth auth] currentUser] != nil){
        
        loginButton.userInteractionEnabled = NO;
        //            [[FIRAuth auth] removeAuthStateDidChangeListener:self];
        
        [FirebaseManager getUserFirebaseDataForUid:[[FIRAuth auth] currentUser].uid completion:^(UserObject *userdata) {
            self.currentUser = userdata;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            int radius = [defaults integerForKey:@"SearchRadius"];
            
            if (!radius){
                [defaults setInteger:100 forKey:@"SearchRadius"];
            }
            
            //update FCM token if it has changed
            self.currentUser.fcmToken = [FIRInstanceID instanceID].token;
            [FirebaseManager updateFirebaseUserValue:@"fcmToken" value:[FIRInstanceID instanceID].token uid:userdata.uid];
            
            [SVProgressHUD dismiss];
            [self determineViewToShow];
        }];
        
        
    }else {
        
        [SVProgressHUD dismiss];
        //            [[FIRAuth auth] removeAuthStateDidChangeListener:self];
        loginButton.userInteractionEnabled = YES;
        
    }
    
    //    }];
    
}


-(void)showAlert:(NSString *)fault{
    
    alert=   [UIAlertController
              alertControllerWithTitle:@"Error"
              message:fault
              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}




/***************************************************
 *
 * ---------- FACEBOOK DELEGATE METHODS ------------
 *
 ***************************************************/

-(void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    
    NSError *signOutError;
    BOOL status = [[FIRAuth auth] signOut:&signOutError];
    if (!status) {
        NSLog(@"Error signing out: %@", signOutError);
        return;
    }
}


-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    
    NSLog(@"Login Button Completion");
    
    
    
    if (error) {
        
        NSLog(@"Process error");
        [self showActionSheetWithMessage:error.description];
        
        
    } else if (result.isCancelled) {
        
        
        NSLog(@"Cancelled");
        [self showActionSheetWithMessage:@"Cancelled By User"];
        
    } else {
        
        [SVProgressHUD show];
        NSLog(@"Logged in");
        [self getFBData:result];
    }
}


//GET FB USER DATA -- WRITE TO FIREBASE
-(void) getFBData:(FBSDKLoginManagerLoginResult*)result {
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"picture, email, first_name, last_name, name, gender, verified"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        
        if (!error) {
            
            //save token to share preferance
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
            [defaults setValue:fbAccessToken forKey:@"FBSDKAccessToken"];
            [defaults synchronize];
            
            //                        [self backendlessLoginWithFB:result];
            FIRAuthCredential *credential = [FIRFacebookAuthProvider
                                             credentialWithAccessToken:[FBSDKAccessToken currentAccessToken].tokenString];
            [self firebaseLoginWithFB:credential result:result];
            
            
        }
        else{
            [self showActionSheetWithMessage:error.description];
        }
    }];
    
}




/*****************************************************************
 *
 *------------ FIREBASE LOGIN AND DATA PLACEMENT ----------------
 *
 *****************************************************************/

-(void) firebaseLoginWithFB:(FIRAuthCredential*)credential result:(id)result{
    
    [locationManager requestWhenInUseAuthorization];

    
    [[FIRAuth auth] signInWithCredential:credential
                              completion:^(FIRUser *fuser, NSError *error) {
                                  if (error) {
                                      NSLog(@"FIR Login error");
                                      [self showActionSheetWithMessage:error.description];
                                      
                                  }else{
                                      
                                      UserObject *loggedUser = [[UserObject alloc] init];
                                      [loggedUser setDefaults];
                                      [loggedUser setUid:fuser.uid];
                                      
                                      //SET AGE
                                      NSString *fbId = [result valueForKey:@"id"];
                                      
                                      double now = [DataManager convertToMillisSince1970:[NSDate date]];
                                      NSString *nowString = [NSString stringWithFormat:@"%.0f",now];
                                      
                                      [loggedUser setRateDate:nowString];
                                      
                                      NSLog(@"first_name = %@", [result objectForKey:@"first_name"]);
                                      
                                      NSString *userEmail = [result valueForKey:@"email"];
                                      if (!userEmail || [userEmail isEqualToString:fbId]) {
                                          userEmail = [NSString stringWithFormat:@"%@@facebook.com", fbId];
                                      }
                                      
                                      
//                                      NSString *ageStr = [DataManager ageCalculate:[result valueForKey:@"birthday"]] ;
//                                      NSLog(@"Age of the rgistering user is ---- %@",ageStr);
                                      
                                      //TODO: Set DeviceId
                                      //                        NSString *currentDeviceId = [FIRMessaging messaging].FCMToken;
                                      
                                      
                                      NSString *userProfilePicStr = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=1080&height=1080", fbId];
                                      
                                      NSString *lastName = [result objectForKey:@"last_name"];
                                      NSString *firstName = [result objectForKey:@"first_name"];
                                      
                                      
                                      
                                      [loggedUser setLastName:lastName];
                                      [loggedUser setFirstName:firstName];
                                      [loggedUser setEmail:userEmail];
                                      [loggedUser setFacebookId:fbId];
                                      [loggedUser setImgUrl:userProfilePicStr];
                                      
                                      
                                      //we want to store genders w/ a capital letter
                                      NSString *capGender;
                                      if([[result valueForKey:@"gender"]  isEqual: @"male"]){
                                          capGender = @"Male";
                                          
                                      }else if ([[result valueForKey:@"gender"]  isEqual: @"female"]){
                                          capGender = @"Female";
                                          
                                      }else{
                                          capGender = @"Gender Non-specific";
                                      }
                                      
                                      [loggedUser setGender:capGender];
                                      
                                      
                                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                      
                                      if ([defaults valueForKey:@"fcmToken"] == nil){
                                          [loggedUser setFcmToken:[FIRInstanceID instanceID].token];
                                          
                                      }else{
                                          [loggedUser setFcmToken:[defaults valueForKey:@"fcmToken"]];
                                      }
                                      
                                      
                                      
                                      [FirebaseManager getUserFirebaseDataForUid:loggedUser.uid completion:^(UserObject *userdata) {
                                          
                                          if (userdata != nil){
                                              //User already exists and fb data should be updated
                                              NSString *uid = loggedUser.uid;
                                              
//                                              [FirebaseManager updateFirebaseUserValue:@"age" value:loggedUser.age uid:uid];
                                              [FirebaseManager updateFirebaseUserValue:@"email" value:loggedUser.email uid:uid];
                                              [FirebaseManager updateFirebaseUserValue:@"fcmToken" value:loggedUser.fcmToken uid:uid];
                                              [FirebaseManager updateFirebaseUserValue:@"deletedDate" value:@"" uid:uid];
//                                              [userdata setValue:loggedUser.age forKey:@"age"];
                                              [userdata setValue:loggedUser.email forKey:@"email"];
                                              [userdata setValue:loggedUser.fcmToken forKey:@"fcmToken"];
                                              [defaults setInteger:100 forKey:@"SearchRadius"];
                                              
                                              self.currentUser = userdata;
                                              [self determineViewToShow];
                                              
                                          }else{
                                              
                                              //first time logging in, set fb data dn user defaults
                                              
                                              [[[FirebaseManager getUsersPath] child:loggedUser.uid]
                                               setValue:[loggedUser userAsDictionary]];
                                              
                                              self.currentUser = loggedUser;
                                              
                                              [defaults setObject:[loggedUser userAsDictionary] forKey:@"initUser"];
                                              [defaults setValue:@"NO" forKey:@"hasSeenEditProfilePrompt"];
                                              [defaults setValue:@"YES" forKey:@"AlreadyUserLogin"];
                                              [defaults setValue:loggedUser.email forKey:@"AlreadyUserName"];
                                              [defaults setInteger:100 forKey:@"SearchRadius"];
                                              
                                              NSString *instaToken = loggedUser.instagramLinked;
                                              
                                              if(![instaToken isEqualToString:@"0"]){
                                                  [defaults setValue:instaToken forKey:@"INSTA_ACCESS_TOKEN"];
                                              }
                                              
                                              if(![defaults boolForKey:@"SecondRun"]){
                                                  [defaults setBool:NO forKey:@"contractAccepted"];
                                              }
                                              
                                              [defaults setBool:NO forKey:@"appRated"];
                                              [defaults synchronize];
                                              
                                              NSLog(@"Current User Set");
                                              [self determineViewToShow];
                                              
                                          }
                                      }];
                                      
                                      
                                      return;
                                  }
                              }];
}




- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
}




#pragma mark - ActioSheet Method

-(void)showActionSheetWithMessage :(NSString *)msg {
    
    [SVProgressHUD dismiss];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}







/*************************************************
 *
 * ---------- ACTION METHODS ------------
 *
 *************************************************/


//ACCEPT CONTRACT BUTTON ACTION
-(void)acceptAction
{
    
    NSLog(@"Accept Contract -- Search View");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];;
    [defaults setBool:YES forKey:@"contractAccepted"];
    
    UserObject *user = [[UserObject alloc] init];
    
    NSDictionary *userData = [defaults objectForKey:@"initUser"];
    [user setUserData:userData.mutableCopy];
    self.currentUser = user;
    
    [defaults synchronize];
    [self showEAIntroPage];
    
    
}


//Contract Action -- Decline - make sure contractAccepted is set to to NO, logout from FB and Backendless
-(void)declineAction

{
    
    NSLog(@"Decline Contract -- Login");
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logOut];
    //    backendless.userService.logout;
    
    NSError *signOutError;
    BOOL status = [[FIRAuth auth] signOut:&signOutError];
    if (!status) {
        NSLog(@"Error signing out: %@", signOutError);
        return;
    }
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"contractAccepted"];
    [defaults setValue:@"NO" forKey:@"AlreadyUserLogin"];
    [defaults setBool:NO forKey:@"SecondRun"];
    [defaults synchronize];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self dismissPopupViewControllerAnimated:YES completion:nil];
    
    
    self.fbLoginBtn.userInteractionEnabled = true;
}




/************************************
 *
 * -------- NETWORK ALERT --------
 *
 *************************************/

-(void)NetworkConnectionAlert{
    
    UIAlertController *netAlert=   [UIAlertController
                                    alertControllerWithTitle:@"Alert"
                                    message:@"Please check your network connection"
                                    preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* retry = [UIAlertAction
                            actionWithTitle:@"Close"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            
                            {
                                [netAlert dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [netAlert addAction:retry];
    
    [self presentViewController:netAlert animated:YES completion:nil];
    
}







/****************************************
 *
 * --------- INTRO PAGE METHODS ---------
 *
 ****************************************/

-(void)showEAIntroPage {
    
    //    IntroViewController *introVC = [[IntroViewController alloc] initWithNibName:@"IntroPage" bundle:nil];
    //
    //    introVC.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    //
    //    [self presentPopupViewController:introVC animated:YES completion:^(void) {
    //
    //        [introVC.skipButton addTarget:self action:@selector(introDidFinish) forControlEvents:UIControlEventTouchUpInside];
    //
    //        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(introDidFinish)];
    //        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(introDidFinish)];
    //        UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(introDidFinish)];
    //
    //        swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    //        swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    //        swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    //
    //        [introVC.view addGestureRecognizer:swipeLeft];
    //        [introVC.view addGestureRecognizer:swipeRight];
    //        [introVC.view addGestureRecognizer:swipeUp];
    //
    //    }];
    [self introDidFinish];
}



- (void)introDidFinish{
    
    [SVProgressHUD show];
    
    [self dismissPopupViewControllerAnimated:NO completion:^{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"SecondRun"];
        [defaults synchronize];
        
        [self showHomeView];
        NSLog(@"Intro Finished...........");
    }];
    
}





/*****************************************************************************************************
 *
 * METHOD TO CALL VIEW BASED ON WHETHER THE USER HAS ACCEPTED THE CONTRACT, PREVIOUSLY STARTED THE APP
 *
 ******************************************************************************************************/

-(void)determineViewToShow{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL isSecondRun = [defaults boolForKey:@"SecondRun"];
    BOOL contractAccepted = [defaults boolForKey:@"contractAccepted"];
    
    if (!contractAccepted) {
        
        [SVProgressHUD dismiss];
        contractView = [[ContractViewController alloc] initWithNibName:@"ConractView" bundle:nil];
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        
        [self addChildViewController:contractView];
        
        contractView.view.frame = screenRect;
        
        [self.view addSubview:contractView.view];
        [contractView didMoveToParentViewController:self];
        
        [contractView.acceptBtn addTarget:self action:@selector(acceptAction) forControlEvents:UIControlEventTouchUpInside];
        
        [contractView.declineBtn addTarget:self action:@selector(declineAction) forControlEvents:UIControlEventTouchUpInside];
        
        contractView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, screenHeight);
        
        NSLog(@"Showing Contract View");
        
        if(!isSecondRun){
            
            NSUserDefaults *ageRangeVal = [NSUserDefaults standardUserDefaults];
            [ageRangeVal setFloat:10 forKey:@"lower"];
            [ageRangeVal setFloat:100 forKey:@"upper"];
            [defaults setValue:@"Both" forKey:@"Gender"];
            [ageRangeVal synchronize];
        }
        
    }else{
        
        [self showHomeView];
        
    }
}



/*********************************************************************************
 *
 * ------------ CONFIGURES THE DRAWER CONTROLLER AND DISPLAYS IT ----------------
 *
 *********************************************************************************/

-(void)showHomeView {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Drawer *drawerController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSString *searchSB = @"MainIdentifier";
    NSString *drawerStoryboardId = @"drawerController";
    SearchViewController *centerVC = [storyboard instantiateViewControllerWithIdentifier:searchSB];
    
    NSString *inboxStoryBoardId = @"inboxviewcontroller";
    NSString *dashStoryboardId = @"DashBoard";
    
    DashBoardViewController *leftVC = [storyboard instantiateViewControllerWithIdentifier:dashStoryboardId];
    InboxViewController *rightVC = [storyboard instantiateViewControllerWithIdentifier:inboxStoryBoardId];
    
    
    drawerController = [[storyboard instantiateViewControllerWithIdentifier:drawerStoryboardId]
                        initWithCenterViewController:centerVC
                        leftDrawerViewController:leftVC rightDrawerViewController:rightVC];
    
    drawerController.chat = [[ChatObject alloc] init] ;
    drawerController.chat.delegate = drawerController;
    drawerController.chat.currentUser = self.currentUser;
    
    
    
    rightVC.chat = drawerController.chat;
    leftVC.chat = drawerController.chat;
    centerVC.chat = drawerController.chat;
    
    
    drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
    drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
    
    [SVProgressHUD dismiss];
    appDelegate.drawerController = drawerController;
    
    if(_gotoChatView){
        appDelegate.drawerController.gotoChatView = YES;
    }else{
        appDelegate.drawerController.gotoChatView = NO;
    }
    
    appDelegate.drawerController.chat.showTSMessage = YES;
    appDelegate.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    appDelegate.window.rootViewController = drawerController;
    
    [appDelegate.window makeKeyAndVisible];
    
}




/*********************************************
 *
 * ---------- NAVIGATION METHODS ------------
 *
 *********************************************/


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        
        //need to query for selected user here
        NSLog(@"Selected User = %@", self.notificationUser);
        
        vc.selectedUser = self.notificationUser;
        
        [SVProgressHUD dismiss];
        
        NSLog(@"Selected User Obj Id = %@",self.notificationUser.uid);
        
        
        
        
    }
    
    [SVProgressHUD dismiss];
    
}



- (IBAction)unwindToLogin:(UIStoryboardSegue *)unwindSegue
{
    
}



@end
