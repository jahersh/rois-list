//
//  ContractViewController.m
//  Complice
//
//  Created by Justin Hershey on 12/16/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import "ContractViewController.h"
#import "SearchViewController.h"

@interface ContractViewController ()



@end


@implementation ContractViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    _acceptBtn.hidden = YES;
    _declineBtn.hidden = YES;
    _scrollToAcceptLbl.hidden = NO;
    
    [self.view layoutIfNeeded];
    
    _webview.delegate = self;
    _webview.scrollView.delegate = self;

    NSString *path = [[NSBundle mainBundle] pathForResource:@"Both" ofType:@"html"];

    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    self.webview.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    
    //reverse commenting out on next two lines after done testing
    [_webview loadRequest:request];
    _downArrow.hidden = NO;
    
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    
    float scrollOffset = scrollView.contentOffset.y;
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    
    if (scrollOffset == 0)
    {
        
        _acceptBtn.hidden = YES;
        _declineBtn.hidden = YES;
        _scrollToAcceptLbl.hidden = NO;
        _downArrow.hidden = NO;
        
    }
    
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        // then we are at the end
        _acceptBtn.hidden = NO;
        _declineBtn.hidden = NO;
        _scrollToAcceptLbl.hidden = YES;
        _downArrow.hidden = YES;
    }
    
}

- (IBAction)declineAction:(id)sender {
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
    NSLog(@"Declined Contract");
    
}

- (IBAction)acceptAction:(id)sender {
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    

    
    NSLog(@"Accepted Contract");
    
}

@end
