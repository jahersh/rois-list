//
//  SettingViewController.m
//  Accomplice
//
//  Created by ibuildx on 3/18/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "SettingViewController.h"
#import "NMRangeSlider.h"
#import "DemoMessagesViewController.h"
#import "SearchViewController.h"
#import <SVProgressHUD.h>
#import "TermsViewController.h"
#import <TSMessage.h>
#import "DashBoardViewController.h"
#import "BlockViewController.h"
#import "InstagramManager.h"
#import <FBSDKLoginManager.h>


@interface SettingViewController () <UIAlertViewDelegate, UIWebViewDelegate>
{
    UIAlertController *alert;
    UIAlertController *alertDelete;
    UIAlertController *alertLogout;
    InstagramManager *instagramManager;
    
    //delete InboxData variables
    NSUInteger deleteOffset;
    NSUInteger deletedPage;
    int totalDeletedPages;
    UIWebView *instaLogin;
}
@end

@implementation SettingViewController

#pragma mark - View Life Cycle Methods

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc{
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view layoutIfNeeded];
    [self maskTriananlgesAndCircularBorderedImage];
    
    _kPropertyContractedProductID = @"com.rois.list.automonthly";
    
    instagramManager = [[InstagramManager alloc] init];
    
    
    if ([self.chat.currentUser.instagramLinked isEqualToString:@"0"]){
        
        [self.linkInstagramSwitch setOn:NO];
        
    }else{
        
        [self.linkInstagramSwitch setOn:YES];
    }
    
    
    
    if ([self.chat.currentUser.propertyContracted isEqualToString:@"0"]){
        
        [self.propertyContractedSwitch setOn:NO];
        
    }else{
        
        [self.propertyContractedSwitch setOn:YES];
    }
    
    
    
    //Subscription
    if ([self.chat.currentUser.subscriptionPurchased isEqualToString:@"0"]){
        
        [self.premiumSwitch setOn:NO];
        
    }else{
        
        if ([self.chat.currentUser.boostRadiusNotification isEqualToString:@"0"]){
            
            [self.notifySwitch setOn:NO];
            
        }else{
            
            [self.notifySwitch setOn:YES];
        }
        
        
        self.notifyView.backgroundColor = [DataManager getGreenColor];
        [self.premiumSwitch setOn:YES];
    }
    
    
    
    
    
    if (!self.usersFound) {
        self.usersFound = [[NSArray alloc] init];
    }
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = self.chat.currentUser.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        UIImage *image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.userImageView setImage:image];
            
        });
    });
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    int radius = [defaults integerForKey:@"SearchRadius"];
    [self.distanceSlider setValue:radius];
    
    NSString *radiusStr;
    if (radius == 100) {
        radiusStr = [NSString stringWithFormat:@"Search Radius : 100+ Miles"];
    }else{
        radiusStr = [NSString stringWithFormat:@"Search Radius : %d Miles", radius];
    }
    
    self.radiusText.text = radiusStr;
    
    
    //TODO turn on when moved
    //[self fetchAvailableProducts];

    
    [self setupView];
    
    _chat.delegate = self ;
    
    
}




- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if([self.view respondsToSelector:@selector(setTintColor:)])
    {
        self.view.tintColor = [UIColor blackColor];
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}





/*********************************************
 *
 * ---------- UI SETUP METHODS ------------
 *
 *********************************************/
-(void) setupView{
    
    
    self.blockListBtn.layer.cornerRadius = 10;
    self.logoutBtn.layer.cornerRadius = 10;
    self.searchRadiusView.layer.cornerRadius = 10;
    self.deleteBtn.layer.cornerRadius = 10;
    self.legalBtn.layer.cornerRadius = 10;
    self.linkInstagramView.layer.cornerRadius = 10;
    self.propertyContractedView.layer.cornerRadius = 10;
    self.premiumView.layer.cornerRadius = 10;
    
    
    //button/image drop shadows
    self.legalBtn.clipsToBounds = NO;
    self.legalBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.legalBtn.layer.shadowOpacity = 0.4;
    self.legalBtn.layer.shadowRadius = 8;
    self.legalBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.premiumView.clipsToBounds = NO;
    self.premiumView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.premiumView.layer.shadowOpacity = 0.4;
    self.premiumView.layer.shadowRadius = 8;
    self.premiumView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.notifyView.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii:(CGSize){10.0, 10.}].CGPath;
    self.notifyView.layer.mask = maskLayer;
    
    self.goPremiumLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.goPremiumLbl.layer.shadowRadius = 5;
    self.goPremiumLbl.layer.shadowOpacity = 0.4;
    self.goPremiumLbl.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.propertyContractedView.clipsToBounds = NO;
    self.propertyContractedView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.propertyContractedView.layer.shadowOpacity = 0.4;
    self.propertyContractedView.layer.shadowRadius = 8;
    self.propertyContractedView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.propertyUnderContractLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.propertyUnderContractLbl.layer.shadowRadius = 5;
    self.propertyUnderContractLbl.layer.shadowOpacity = 0.4;
    self.propertyUnderContractLbl.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.linkInstaLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.linkInstaLbl.layer.shadowRadius = 5;
    self.linkInstaLbl.layer.shadowOpacity = 0.4;
    self.linkInstaLbl.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.radiusText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.radiusText.layer.shadowRadius = 5;
    self.radiusText.layer.shadowOpacity = 0.4;
    self.radiusText.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.linkInstagramView.clipsToBounds = NO;
    self.linkInstagramView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.linkInstagramView.layer.shadowOpacity = 0.4;
    self.linkInstagramView.layer.shadowRadius = 8;
    self.linkInstagramView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);

    self.deleteBtn.clipsToBounds = NO;
    self.deleteBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.deleteBtn.layer.shadowOpacity = 0.4;
    self.deleteBtn.layer.shadowRadius = 8;
    self.deleteBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.deleteBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.deleteBtn.titleLabel.layer.shadowRadius = 5;
    self.deleteBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.deleteBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.logoutBtn.clipsToBounds = NO;
    self.logoutBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.logoutBtn.layer.shadowOpacity = 0.4;
    self.logoutBtn.layer.shadowRadius = 8;
    self.logoutBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.logoutBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.logoutBtn.titleLabel.layer.shadowRadius = 5;
    self.logoutBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.logoutBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    self.blockListBtn.clipsToBounds = NO;
    self.blockListBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.blockListBtn.layer.shadowOpacity = 0.4;
    self.blockListBtn.layer.shadowRadius = 8;
    self.blockListBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.blockListBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.blockListBtn.titleLabel.layer.shadowRadius = 5;
    self.blockListBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.blockListBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    // -- view drop shadows
    self.seeMeView.clipsToBounds = NO;
    self.seeMeView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.seeMeView.layer.shadowOpacity = 0.4;
    self.seeMeView.layer.shadowRadius = 8;
    self.seeMeView.layer.shadowOffset = CGSizeMake(8.0f, 8.0f);
    
    self.iSeeView.clipsToBounds = NO;
    self.iSeeView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.iSeeView.layer.shadowOpacity = 0.4;
    self.iSeeView.layer.shadowRadius = 8;
    self.iSeeView.layer.shadowOffset = CGSizeMake(8.0f, 8.0f);
    
    self.searchRadiusView.clipsToBounds = NO;
    self.searchRadiusView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.searchRadiusView.layer.shadowOpacity = 0.4;
    self.searchRadiusView.layer.shadowRadius = 8;
    self.searchRadiusView.layer.shadowOffset = CGSizeMake(8.0f, 8.0f);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)maskTriananlgesAndCircularBorderedImage {
    
    self.userImageView.backgroundColor = [UIColor clearColor];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
    self.userImageView.clipsToBounds = YES;
    
    UIColor *bgColor = [UIColor whiteColor];
    
    self.userImageView.layer.borderWidth = self.userImageView.frame.size.width / 25;
    self.userImageView.layer.borderColor = bgColor.CGColor;
}




/******************************************
 *
 * ---------- ACTION METHODS ------------
 *
 ******************************************/

- (IBAction)backBtnAction:(id)sender {
    
    [self performSegueWithIdentifier:@"backToDashboard" sender:nil];
    
}

- (IBAction)logOutBtnAction:(id)sender {
    
}


//Will catch all current Userss inbox data and mark it as deleted ASYNC
-(void) markDbDataAsDeleted: (NSString *) userToRemove {
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    FIRDatabaseReference *userRef = [[FirebaseManager getUsersPath] child:uid];
    FIRDatabaseReference *inboxListRef = [[FirebaseManager getInboxListPath] child:uid];
    FIRDatabaseReference *instagramRef = [[FirebaseManager getInstagramPath] child:uid];
    FIRDatabaseReference *geoRef = [[FirebaseManager getGeoPath] child:uid];
    
    NSString *deletedDate = [NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]];
    
    [[userRef child:@"deletedDate"] setValue:deletedDate];
    [[userRef child:@"instagramLinked"] setValue:@"0"];
    
    [inboxListRef removeValue];
    [instagramRef removeValue];
    [geoRef removeValue];
    
    
    
}



- (IBAction)deleteLogoutAction:(id)sender {
    
    //code to delete backenedless data here
    alertDelete=   [UIAlertController
                    alertControllerWithTitle:@"Warning"
                    message:@"Delete your Data and Logout?"
                    preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             //Delete Data and Logout
                             NSLog(@"OK button touched!");
                             UserObject *userToDelete = self.chat.currentUser;
                             NSLog(@"Current User id is %@", self.chat.currentUser.uid) ;
                             
                             deletedPage = 0;
                             //Kick off Async task to mark messages as deleted
                             [self markDbDataAsDeleted:userToDelete.uid];
                             
                             
                             //logout
                             FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                             [login logOut];
                             
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                             [defaults setValue:@"NO" forKey:@"AlreadyUserLogin"];
                             [defaults setBool:NO forKey:@"SecondRun"];
                             [defaults synchronize];
                             
                             [alertDelete dismissViewControllerAnimated:YES completion:nil];
                             
                             [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                             
                             NSError *signOutError;
                             BOOL status = [[FIRAuth auth] signOut:&signOutError];
                             if (!status) {
                                 NSLog(@"Error signing out: %@", signOutError);
                                 return;
                             }
                             
                             [self performSegueWithIdentifier:@"ToLogout" sender:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alertDelete dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alertDelete addAction:ok];
    [alertDelete addAction:cancel];
    
    [self presentViewController:alertDelete animated:YES completion:nil];
    
}



//should redirect to Instagram
- (IBAction)linkInstagramAction:(id)sender {
    
    if(!self.linkInstagramSwitch.isOn){
        
        //was on disconnect Insta
        NSString *uid = [[FIRAuth auth] currentUser].uid;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"0" forKey:@"INSTA_ACCESS_TOKEN"];
        [defaults synchronize];
        
        //set user instagramLinked value to 0 and delete the instagram photo reference path
        FIRDatabaseReference *instaRef = [[FirebaseManager getInstagramPath] child:uid];
        [FirebaseManager updateFirebaseUserValue:@"instagramLinked" value:@"0" uid:uid];
        self.chat.currentUser.instagramLinked = @"0";
        [instaRef removeValue];
        
    }else{
        
        //was off, update token or connect
        if (![[[NSUserDefaults standardUserDefaults] stringForKey:@"INSTA_ACCESS_TOKEN"] isEqualToString:@"0"]){
            
            UIAlertController *alertLink =   [UIAlertController
                                              alertControllerWithTitle:@"Account Previously Linked"
                                              message:@"Touch Update to refresh access token"
                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alertLogout dismissViewControllerAnimated:YES completion:nil];
                                 }];
            
            UIAlertAction* update = [UIAlertAction
                                     actionWithTitle:@"Update"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alertLogout dismissViewControllerAnimated:YES completion:nil];
                                         [self getInstagramToken];
                                     }];
            [alertLink addAction:ok];
            [alertLink addAction: update];
            
            [self presentViewController:alertLink animated:YES completion:nil];
        }else{
            
            [self getInstagramToken];
            
        }
    }
}

- (IBAction)propertySwitchAction:(id)sender {
    
    UserObject *user = self.chat.currentUser;
    
    FIRDatabaseReference *userRef = [[FirebaseManager getUsersPath] child:user.uid];
    
//    if([user.subscriptionPurchased isEqualToString:@"1"]){
    
        if (self.propertyContractedSwitch.isOn){
            
            user.propertyContracted = @"1";
            self.chat.currentUser.propertyContracted = @"1";
            [[userRef child:@"propertyContracted"] setValue:@"1"];
            
        }else{
            
            user.propertyContracted = @"0";
            self.chat.currentUser.propertyContracted = @"0";
            [[userRef child:@"propertyContracted"] setValue:@"0"];
            
        }
        
}

- (IBAction)premiumSwitchAction:(id)sender {
    
    UserObject *user = self.chat.currentUser;
    
    FIRDatabaseReference *userRef = [[FirebaseManager getUsersPath] child:user.uid];
    
    if (self.premiumSwitch.isOn){
        
        user.subscriptionPurchased = @"1";
        self.chat.currentUser.subscriptionPurchased = @"1";
        self.notifyView.backgroundColor = [DataManager getGreenColor];
        [[userRef child:@"subscriptionPurchased"] setValue:@"1"];
        
    }else{
        
        user.subscriptionPurchased = @"0";
        self.chat.currentUser.subscriptionPurchased = @"0";
        [[userRef child:@"subscriptionPurchased"] setValue:@"0"];
        self.notifyView.backgroundColor = [UIColor darkGrayColor];
        
        [self.notifySwitch setOn:NO];
        [self notifySwitchAction:nil];
        
    }
}

- (IBAction)notifySwitchAction:(id)sender {
    
    UserObject *user = self.chat.currentUser;
    
    FIRDatabaseReference *userRef = [[FirebaseManager getUsersPath] child:user.uid];
    FIRDatabaseReference *premiumBoostRef = [[FirebaseManager getBoostedNotificationUsersPath] child:user.uid];
    
    
    if (self.notifySwitch.isOn){
        
        if ([user.subscriptionPurchased isEqualToString:@"1"]){
            
            NSMutableDictionary *boostedData = [[NSMutableDictionary alloc] init];
            [boostedData setValue:user.fcmToken forKey:@"fcmToken"];
            [boostedData setValue:user.firstName forKey:@"firstName"];
            [boostedData setValue:user.searchRadius forKey:@"searchRadius"];
            [boostedData setValue:user.latitude forKey:@"latitdue"];
            [boostedData setValue:user.longitude forKey:@"longitude"];
            [boostedData setValue:user.uid forKey:@"uid"];
            
            user.boostRadiusNotification = @"1";
            self.chat.currentUser.boostRadiusNotification = @"1";
            [[userRef child:@"boostRadiusNotification"] setValue:@"1"];
            [premiumBoostRef setValue:boostedData];
        }else{
            
//            [self.premiumSwitch setOn:YES];
            [self premiumSwitchAction:nil];
        }

    }else{

        user.boostRadiusNotification = @"0";
        self.chat.currentUser.boostRadiusNotification = @"0";
        [[userRef child:@"boostRadiusNotification"] setValue:@"0"];
        [premiumBoostRef removeValue];

    }
    
    
}


- (IBAction)boostBtnAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toBoostSegue" sender:self];
}



-(void) getInstagramToken{
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"INSTA_ACCESS_TOKEN"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *urlString = @"https://api.instagram.com/oauth/authorize/?client_id=9df1f352e3cc491d9af3e32eac0645de&redirect_uri=https://complice.club/&response_type=token";
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    instaLogin = [[UIWebView alloc] init];
    instaLogin.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    instaLogin.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.view addSubview:instaLogin];
    [instaLogin loadRequest:request];
    
}



- (IBAction)blockListAction:(id)sender {
    [self performSegueWithIdentifier:@"ToBlockListView" sender:nil];
}




- (IBAction)legalAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toLegalView" sender:nil];
}






/*********************************************
 *
 * -------- WEBVIEW DELEGATE METHODS ---------
 * ---------- for Instagram Login ------------
 *
 *********************************************/


-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    
    NSString *urlString = [request.URL absoluteString];
    NSLog(@"URL STRING %@", urlString);
    NSArray *UrlParts = [urlString componentsSeparatedByString:@"access_token="];
    NSRange accessToken = [urlString rangeOfString:@"#access_token="];
    
    
    
    if ([UrlParts count] > 1){
        
        // do any of the following here
        urlString = UrlParts[1];
        
        NSArray *tokenParts = [urlString componentsSeparatedByString:@"."];
        NSString *instaID = tokenParts[0];
        
        if (accessToken.location != NSNotFound){
            
            NSString *strAccessToken = urlString;
            
            [[NSUserDefaults standardUserDefaults] setValue:strAccessToken forKey:@"INSTA_ACCESS_TOKEN"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"AccessToken = %@", strAccessToken);
            
            UserObject *user = self.chat.currentUser;
            FIRDatabaseReference *myInstaId = [[FirebaseManager getInstagramPath] child:user.uid];
            
            [myInstaId setValue:instaID];
            [FirebaseManager updateFirebaseUserValue:@"instagramLinked" value:instaID uid:user.uid];
            self.chat.currentUser.instagramLinked = instaID;
            
            [instaLogin removeFromSuperview];
            [instagramManager replaceInstagramPictures:instaID];
            
            UIAlertController *alertL=   [UIAlertController
                                          alertControllerWithTitle:@"Success"
                                          message:@"Instagram Account Linked"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [alertL dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            
            [alertL addAction:ok];
            [self presentViewController:alertL animated:YES completion:nil];
            
        }
        return NO;
        
    }
    return YES;
}



-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    NSLog(@"webview failed to load %@", error);
    
    UIAlertController *alertF=   [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:@"Failed To Load Webpage"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [alertF dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    
    [alertF addAction:ok];
    [self presentViewController:alertF animated:YES completion:nil];
}




- (IBAction)distanceSliderValueChanged:(UISlider *)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString *radiusStr = [NSString stringWithFormat:@"Search Radius : %i Miles", (int)roundf(sender.value)];
    [defaults setInteger:(int)roundf(sender.value) forKey:@"SearchRadius"];
    
    //TODO: Update distance labels for over 100 miles
    if (sender.value == 100) {
        radiusStr = [NSString stringWithFormat:@"Search Radius : 100+ Miles"];
    }else{
        radiusStr = [NSString stringWithFormat:@"Search Radius : %.0f Miles", sender.value];
    }
    
    [defaults synchronize];
    
    self.radiusText.text = radiusStr;
}




- (IBAction)searchUsersBtn:(id)sender {
    
    
    alertLogout=   [UIAlertController
                    alertControllerWithTitle:@"Warning"
                    message:@"Log out?"
                    preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                             [login logOut];
                             
                             NSError *signOutError;
                             BOOL status = [[FIRAuth auth] signOut:&signOutError];
                             if (!status) {
                                 NSLog(@"Error signing out: %@", signOutError);
                                 return;
                             }
                             
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                             [defaults setValue:@"NO" forKey:@"AlreadyUserLogin"];
                             [defaults synchronize];
                             
                             [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                             [self performSegueWithIdentifier:@"ToLogout" sender:nil];
                             [alertLogout dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alertLogout dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alertLogout addAction:ok];
    [alertLogout addAction:cancel];
    
    [self presentViewController:alertLogout animated:YES completion:nil];
    
}







/*********************************************
 *
 * --------- NAVIGATION METHODS -----------
 *
 *********************************************/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"SettingsView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;
        
        vc.delegateModal = self;
    }
    else if ([segue.identifier isEqualToString:@"ToSearch"]) {
        
        SearchViewController *vc = [[SearchViewController alloc]init];
        
        vc.currentUser = self.currentUser ;
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = segue.destinationViewController;
        
    }
    
    else if([segue.identifier isEqualToString:@"backToDashboard"])
    {
        DashBoardViewController *messageView = segue.destinationViewController;
        
        [messageView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = messageView;
        
    }
    else if([segue.identifier isEqualToString:@"ToBlockListView"])
    {
        BlockViewController *messageView = segue.destinationViewController;
        
        [messageView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = messageView;
        
        
    }
    
    _chat = nil;
    
}

- (IBAction)unwindToSetting:(UIStoryboardSegue *)unwindSegue
{
}



/*********************************************
 *
 * ------ CHATOBJECT DELEGATE METHODS --------
 *
 *********************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                           
                                           
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}





/*****************************************************************************
 *
 *
 *__________________PURCHASE AGE HIDING_______________________
 *
 *
 *****************************************************************************/


-(void)fetchAvailableProducts{
    
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:_kPropertyContractedProductID,nil];
    _productsRequest = [[SKProductsRequest alloc]
                        initWithProductIdentifiers:productIdentifiers];
    
    _productsRequest.delegate = self;
    
    [_productsRequest start];
}


- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}


- (void)purchaseMyProduct:(SKProduct*)product{
    
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        [SVProgressHUD dismiss];
    }
    else{
        
        UIAlertController *alertP=   [UIAlertController
                                      alertControllerWithTitle:@"Purchases are disabled in your device"
                                      message:[NSString stringWithFormat:@""]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [SVProgressHUD dismiss];
                                 [alertP dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        [alertP addAction:ok];
        [self presentViewController:alertP animated:YES completion:nil];
        
    }
}


#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:_kPropertyContractedProductID]) {
                    
                    NSLog(@"Purchased ");
                    
                    //Add hours to profile
                    UserObject *user = self.chat.currentUser;
                    
                    
                    
                    
                    [FirebaseManager updateFirebaseUserValue:@"subscriptionPurchased" value:@"1" uid:user.uid];
                    [self.chat.currentUser setSubscriptionPurchased:@"1"];
                    
                    UIAlertController *alertQ=   [UIAlertController
                                                  alertControllerWithTitle:@"Purchase completed succesfully"
                                                  message:[NSString stringWithFormat:@""]
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alertQ dismissViewControllerAnimated:YES completion:nil];
                                         }];
                    
                    [alertQ addAction:ok];
                    [self presentViewController:alertQ animated:YES completion:nil];
                    
                    
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                break;
                
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    if (count>0) {
        _validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        if ([validProduct.productIdentifier
             isEqualToString:_kPropertyContractedProductID]) {
        }
    } else {
        
        
        
        UIAlertController *alertR=   [UIAlertController
                                      alertControllerWithTitle:@"Not Available"
                                      message:[NSString stringWithFormat:@"No products to purchase"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertR dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        [alertR addAction:ok];
        [self presentViewController:alertR animated:YES completion:nil];
        
    }
    
    [_activityIndicatorView stopAnimating];
    
}




@end
