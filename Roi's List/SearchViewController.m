//
//  InboxView.m
//  LiveChatWithNearest
//
//  Created by ibuildx on 2/29/16.
//  Copyright © 2016 ibuildx. All rights reserved.



#import "SearchViewController.h"
#import "UsersTableViewCell.h"
#import "DashBoardViewController.h"
#import "ProfileViewController.h"
#import "SettingViewController.h"
#import "DataManager.h"
#import <SVProgressHUD.h>
#import "FlagViewController.h"
#import "UIViewController+CWPopup.h"
#import "ProfilePopUp.h"
#import "InstagramProfilePopUp.h"
#import "BlockUser.h"
#import "RKNotificationHub.h"
#import <EAIntroView.h>
#import <TSMessage.h>
#import "InboxViewController.h"
#import "SettingViewController.h"
#import "DashBoardViewController.h"
#import "INTULocationManager.h"
#import "AppDelegate.h"
#import "EditProfileViewController.h"
#import "InstagramManager.h"



@interface SearchViewController () < UIAlertViewDelegate, EAIntroDelegate, UIScrollViewDelegate>

{
    
    UIAlertController *alert;
    int iVal;
    BOOL scrollToBottom;
    InstagramManager *instagramManager;
    
}

@end


@implementation SearchViewController



- (UIStatusBarStyle)preferredStatusBarStyle
{
    
    return UIStatusBarStyleDefault;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.searchBar.delegate = self;
    
    NSLog(@"VIEW DID LOAD");
    _noMoreUsers = NO;
    
    _cachedImages = [[NSMutableDictionary alloc] init];
    instagramManager = [[InstagramManager alloc] init];

    
    [SVProgressHUD show];
    self.locationRequestID = NSNotFound;

    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor =  [UIColor blackColor];
    _refreshControl.backgroundColor = [UIColor lightGrayColor];
    [_refreshView setFrame:_refreshControl.bounds];
    
    [self.tableView addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isSecondRun = [defaults boolForKey:@"SecondRun"];

    //these keys ensure we are only refreshing the inbox/search when there have been changes
    [defaults setBool:YES forKey:@"refreshSearch"];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    NSLog(@"SecondRun = %d", isSecondRun);
    
    
    
    //update badge count
    [FirebaseManager getBadgeCount:^(int badgeCount) {
        
        if (_hub == nil){
            _hub = [[RKNotificationHub alloc] initWithView:self.backBtn];
        }
        
        if( badgeCount == 0)
        {
            [_chat setBadgeCount:0];
            [_hub setCount:0];
            [_hub hideCount];
            [_hub setCircleColor:[UIColor clearColor] labelColor:[UIColor clearColor]];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            
        }
        else
        {
            
            [_chat setBadgeCount:badgeCount];
            [_hub setCircleColor:[UIColor redColor] labelColor:[UIColor whiteColor]];
            [_hub setCount:badgeCount];
            [_hub showCount];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
        }
    }];
    
    
    if (!isSecondRun) {
         
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"SecondRun"];
        [defaults synchronize];
        [defaults synchronize];
    }
    

    //open drawer image setup
    self.openDrawerBtn.clipsToBounds = YES;
    self.openDrawerBackground.layer.borderColor = [DataManager getGreenColor].CGColor;
    self.openDrawerBackground.clipsToBounds = NO;
    
    [self.openDrawerBtn setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    
    [self.openDrawerBtn addGestureRecognizer:singleTap];
    
    [self tableViewIntegration];
    
    

    self.adView.adUnitID = @"ca-app-pub-6416144291405618/9693659683";
    self.adView.rootViewController = self;
    [self.adView loadRequest:[GADRequest request]];
        
    [self.adView setHidden:NO];
    [self.view bringSubviewToFront:self.adView];
    
    
    
    
    //UPDATE INSTAGRAM PHOTOS then Cleanup Old Photos
    if([self.chat.currentUser.instagramLinked isEqualToString:@"0"]){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
            [instagramManager uploadNewPhotos];
            
        });
    }
}



-(void)dealloc{
    
    _cachedImages = nil;
    _usersFound = nil;
    _labelsArray = nil;
    _masterSearchList = nil;
    
}



-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    NSLog(@"VIEW WILL APPEAR");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL refresh = [defaults boolForKey:@"refreshSearch"];
    
    
    if ([self.chat.currentUser.subscriptionPurchased isEqualToString:@"1"]){

        [self.adView setHidden:YES];
        
    }else{

        [self.adView setHidden:NO];

    }
    
    
    if (!self.masterSearchList) {
        
        self.usersFound = [[NSMutableArray alloc] init];
        self.masterSearchList = [[NSMutableArray alloc] init];
    }
    
    
    if (refresh){
        
        [SVProgressHUD show];
        [defaults setBool:NO forKey:@"refreshSearch"];
        
        [self startSingleLocationRequest:
         ^(NSString *bc) {
             
                self.usersFound = [[NSMutableArray alloc] init];
                self.masterSearchList = [[NSMutableArray alloc] init];
                [self fetchUsersAsync: NO];
         }];
    }
}

    



- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    NSLog(@"View Did Appear CALLED");
    
    if(_hub == nil){
        _hub = [[RKNotificationHub alloc] initWithView:self.backBtn];
    }
    
    //need to be run after the view has appeared
    self.openDrawerBtn.layer.cornerRadius = self.openDrawerBtn.frame.size.width / 2;
    self.openDrawerBackground.layer.cornerRadius = _openDrawerBackground.frame.size.width /2;
    self.openDrawerBackground.layer.borderWidth = self.openDrawerBackground.frame.size.width / 25;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = self.chat.currentUser.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.openDrawerBtn setImage:[UIImage imageWithData:data]];

        });
    });
    
    
    
    
    
    //APP RATING CHECK
    //Tweak so we aren't prompted evvvvery login
    
    //checking if the user has rated the app yet and when it was last rated
//    NSString *lastCheckDateString = self.currentUser.rateDate;
//    double lastCheckDate = [self.currentUser.rateDate doubleValue];
//    double today = [[NSDate date] timeIntervalSince1970];
//    
//    
//    if ([lastCheckDateString isEqualToString:@""]){
//        
//        lastCheckDate = today;
//    }
//    
//    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
//    dayComponent.day = 7;
//    
//    NSCalendar *theCalendar = [NSCalendar currentCalendar];
//    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:lastCheckDate options:0];
    
    
//    
//    NSLog(@"Today: %@, nextDate: %@", today, nextDate);
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    BOOL isSecondRun = [defaults boolForKey:@"SecondRun"];
//    
//    if(![defaults boolForKey:@"appRated"] && ([today timeIntervalSinceReferenceDate] > [nextDate timeIntervalSinceReferenceDate]) && isSecondRun){
//        
//        [self.currentUser updateProperties:@{@"rateDate" : today}];
//        
//        [backendless.userService
//         update:self.currentUser
//         response:^(BackendlessUser *updatedUser) {
//             NSLog(@"User has been updated (ASYNC): %@", updatedUser);
//             
//         }
//         error:^(Fault *fault) {
//             NSLog(@"Server reported an error (ASYNC): %@", fault);
//         }];
//        
//        //prompt to Rate at app store
//        NSLog(@"Time to Rate");
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            [self rateAppAlert];
//            
//        });
//        
//    }
    
    
    
//    BOOL new = [[NSUserDefaults standardUserDefaults] boolForKey:@"newFeaturesSeen"];
//    
//    if (!new){
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            
//            [self newFeaturesAlert:@"Pull in your recent Instagram photos and Boost your profile! Check it out in Settings!"];
//        });
//        
//    }

    //initially set showTSMessage to NO, chatObject will set it back to YES when it determines whether there are no notifications or it has not shown pushNotifications in the app message queue when opening the app
    _chat.showTSMessage = NO;

    
}


-(void) newFeaturesAlert:(NSString*) message{
    UIAlertController *ialert =   [UIAlertController
               alertControllerWithTitle:@"New Features"
               message:message
               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [ialert dismissViewControllerAnimated:YES completion:nil];
                             
                             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                             [userDefaults setBool:YES forKey:@"newFeaturesSeen"];
                             [userDefaults synchronize];

                         }];
    
    [ialert addAction:ok];
    
    [self presentViewController:ialert animated:YES completion:nil];
    
    
    
}

//-- Functions performed when pulling down in the SearchView Table to refresh
- (void)refresh:(UIRefreshControl *)refreshControl {
    
    // Do your job, when done:
    _currentPage = 0;
    _noMoreUsers = NO;
    
    [self startSingleLocationRequest:
     ^(NSString *bc) {
         
         self.usersFound = [[NSMutableArray alloc] init];
         self.masterSearchList = [[NSMutableArray alloc] init];
         [self.tableView reloadData];

         [self fetchUsersAsync: NO];
         
     }];
}






- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*************************************************
 *
 * ---------- UI SETUP METHODS ------------
 *
 *************************************************/

-(void)maskTriananlgesAndCircularBorderedImage {
    
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){0, 0}];
    [path addLineToPoint:(CGPoint){0, self.leftDarkView.frame.size.height}];
    [path addLineToPoint:(CGPoint){self.view.frame.size.width / 2, 0}];
    [path addLineToPoint:(CGPoint){0, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = self.leftDarkView.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    self.leftDarkView.layer.mask = mask;
    
    // Build a triangular path
    UIBezierPath *pathRight = [UIBezierPath new];
    [pathRight moveToPoint:(CGPoint){0, 0}];
    [pathRight addLineToPoint:(CGPoint){self.view.frame.size.width / 2 , self.leftDarkView.frame.size.height}];
    [pathRight addLineToPoint:(CGPoint){self.view.frame.size.width / 2 , 0}];
    [pathRight addLineToPoint:(CGPoint){0, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *maskRight = [CAShapeLayer new];
    maskRight.frame = self.rightDarkView.bounds;
    maskRight.path = pathRight.CGPath;
    
    // Mask the imageView's layer with this shape
    self.rightDarkView.layer.mask = maskRight;
    self.userImageView.backgroundColor = [UIColor redColor];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
    self.userImageView.clipsToBounds = YES;
    UIColor *bgColor = self.backgroundLightView.backgroundColor;
    self.userImageView.layer.borderWidth = self.userImageView.frame.size.width / 20;
    self.userImageView.layer.borderColor = bgColor.CGColor;
    
}



-(void)checkUserWithName :(NSString *)name {
    for (UserObject* findingUser in self.usersFound) {
        if ([findingUser.firstName containsString:name]) {
            NSLog(@"User searched___________ %@", name);
            break;
        }
    }
}


-(void)logUsrNames :(NSMutableArray *)allUsers {
    for (UserObject *usr in allUsers) {
        NSLog(@"%@",usr.firstName);
    }
}



-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ((_tableView.contentSize.height - _tableView.contentOffset.y) == (_tableView.frame.size.height)){
        if(!_noMoreUsers){

            NSLog(@"Made it to bottom of table, load more users");
        }
    }
}


#pragma mark - Backendless And Responses Methods

-(void)fetchUsersAsync :(BOOL)check {


    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isSecondRun = [defaults boolForKey:@"SecondRun"];
    if (isSecondRun && check) {
        [SVProgressHUD showWithStatus:@"Loading"];
    }

    
    NSInteger searchRadius = [[defaults valueForKey:@"SearchRadius"] integerValue];
    
    NSLog(@"%@", [defaults valueForKey:@"SearchRadius"]);
    if (searchRadius > 99) {
        
        searchRadius = 3000;
    }
    
    double latitude = [self.chat.currentUser.latitude doubleValue];
    double longitude = [self.chat.currentUser.longitude doubleValue];
    
    NSLog(@"%f",latitude);
    NSLog(@"%f", longitude);

    
    //GeoFire uses a search radius in Kilometers so we need to convert from miles
    double radius = [DataManager milesToKilometers:searchRadius];
    CLLocation *center = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

    [FirebaseManager queryNearbyUsersWithRadius:center radius:radius completion:^(NSDictionary *users) {

        
        __block NSMutableDictionary *workingDictionary = [[NSMutableDictionary alloc] init];
        workingDictionary = [users mutableCopy];
        workingDictionary = [self removeCurrentUserFromUsers:workingDictionary];
        
        [FirebaseManager getBlockedUserListToExclude:^(NSDictionary *users) {
            
            workingDictionary = [self removeBlockedUsers:[users mutableCopy] FromUsers:workingDictionary];
            
            if (workingDictionary.count == 0) {
                
                [self noUsersAlert];
            }else{
                
                NSMutableArray *finalUsersArray = [[NSMutableArray alloc] init];
                
                
                for (NSString *key in workingDictionary.allKeys){
                    
                    [FirebaseManager getUserFirebaseDataForUid:key completion:^(UserObject *userdata) {
                        
                        if (userdata != nil){
                            [finalUsersArray addObject:userdata];
                        }else{
                            [SVProgressHUD dismiss];
                        }
                        
                        if (finalUsersArray.count == workingDictionary.count){
                            
                            self.usersFound = [self sortUsersBasedOnDistance:finalUsersArray];
                            self.masterSearchList = [self sortUsersBasedOnDistance:finalUsersArray];
                            
                            [SVProgressHUD dismiss];
                            [self.tableView reloadData];
                            [_refreshControl endRefreshing];
                            
                        }
                    }];
                }
            }
        }];
    }];
}

-(void) rateAppAlert{
    
    alert =   [UIAlertController
               alertControllerWithTitle:@"Rate Us!"
               message:@"Let us know what you think"
               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Rate"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                             [userDefaults setBool:YES forKey:@"appRated"];
                             [userDefaults synchronize];
                             
                             NSString *appId = @"1207459676";
                             NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", appId]];

                             [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)updateProfileAlert{
    [SVProgressHUD dismiss];
    
    alert=   [UIAlertController
               alertControllerWithTitle:@"Update profile Info!"
               message:@"take me there..."
               preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [self performSegueWithIdentifier:@"EditProfileSegue" sender:self];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                            
                         }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}



-(void)noUsersAlert{
    [SVProgressHUD dismiss];
    alert=   [UIAlertController
              alertControllerWithTitle:@"No Users"
              message:@"No users found for your current setting preferences"
              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [_refreshControl endRefreshing];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}





//Allows us to cache user images to prevent redownloading over and over
- (UIImage *)imageForIndexPath:(NSIndexPath *)indexPath {
    
    UserObject *user = [self.usersFound objectAtIndex:indexPath.row];
    NSString *userID = user.uid;
    
    
    __block UIImage *image = [_cachedImages valueForKey:userID];
    
    if (!image) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            NSString *imgUrl = user.imgUrl;
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
            
            image = [UIImage imageWithData:data];
            if (image != nil){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_cachedImages setObject:image forKey:userID];
                    [self.tableView reloadData];
                    
                });
            }
        });
    }
    
    return image;
}




/*************************************************
 *
 * ---------- FILTER DATA METHODS ------------
 *
 *************************************************/




-(NSMutableDictionary *) removeCurrentUserFromUsers :(NSMutableDictionary *)users {
    
    NSMutableDictionary *newFilteredUsers = [[NSMutableDictionary alloc]init];
    
    for (NSString *key in users.allKeys) {
        if (![self.chat.currentUser.uid isEqualToString:key]) {
            [newFilteredUsers setObject:[users valueForKey:key] forKey:key];
        }
    }
    return newFilteredUsers;
}





-(NSMutableArray *) filterUsersArray :(NSMutableArray *)users FromGender :(NSString *)gender {
    
    NSMutableArray *filteredArrayResult = [[NSMutableArray alloc] init];
    
    for (UserObject *userToSearch in users) {
        if ([userToSearch.gender isEqualToString:gender]) {
            [filteredArrayResult addObject:userToSearch];
        }
    }
    return filteredArrayResult;
}




-(NSMutableDictionary *) removeBlockedUsers :(NSDictionary *)blockedUsers FromUsers :(NSMutableDictionary *)users {
    
    for (NSString *key in blockedUsers.allKeys){
        
        
        if ([users valueForKey:key] != nil){
            
            [users removeObjectForKey:key];

        }
    }
    
    
    return users ;
}






//NOTE -- SORTING  SHOULD HAPPEN IN THE BACKENDLESS QUERY so paging will work properly in the TableView -- backendless sorting not currently working ?
-(NSMutableArray *) sortUsersBasedOnDistance :(NSMutableArray *)users {
    
    users = [[users sortedArrayUsingComparator: ^(UserObject* a, UserObject* b)
    {
        
        CLLocation *locationA = [[CLLocation alloc] initWithLatitude:[a.latitude doubleValue] longitude:[a.longitude doubleValue]];
        CLLocation *locationB = [[CLLocation alloc] initWithLatitude:[b.latitude doubleValue] longitude:[b.longitude doubleValue]];
        
        CLLocationDegrees latitudeCurrent = [self.chat.currentUser.latitude doubleValue];
        CLLocationDegrees longitudeCurrent = [self.chat.currentUser.longitude doubleValue];
        CLLocation *locationCurrent = [[CLLocation alloc]initWithLatitude:latitudeCurrent longitude:longitudeCurrent];
        
        CLLocationDistance dist_a = [locationA distanceFromLocation:locationCurrent];
        CLLocationDistance dist_b = [locationB distanceFromLocation:locationCurrent];
        
        if ( dist_a < dist_b ) {
            return (NSComparisonResult)NSOrderedAscending;
        } else if ( dist_a > dist_b) {
            return (NSComparisonResult)NSOrderedDescending;
        } else {
            return (NSComparisonResult)NSOrderedSame;
        }
    }] mutableCopy];

    return users;
}
    

//if we sort users from backendless we won't need this method
-(NSMutableArray *) filterSearchRadius :(NSMutableArray *)users {
    
    NSMutableArray *usersRadiousFilter = [[NSMutableArray alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int searchDistance = (int)[defaults integerForKey:@"SearchRadius"];
    
    if (searchDistance >= 100 || searchDistance == 0) {
        return users;
    }
    else{
        for (UserObject *userToSearch in users) {
            if ([DataManager getDistanceBetweenUser:[[self.chat currentUser] userAsDictionary] andOtherUser:[userToSearch userAsDictionary]] <= searchDistance) {
                [usersRadiousFilter addObject:userToSearch];
            }
        }
        return usersRadiousFilter;
    }
}






/***************************************************
 *
 * --------- SEARCHBAR DELEGATE METHODS ----------
 *
 ***************************************************/


-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSLog(@"Search String = %@",searchBar.text);
    [self.usersFound removeAllObjects];
    
    if ([searchBar.text isEqualToString:@""]) {
        [_usersFound addObjectsFromArray:_masterSearchList];
        [self.tableView reloadData] ;
        return ;
    }
    
    for (UserObject *user in _masterSearchList) {
        if ([user.firstName localizedCaseInsensitiveContainsString:searchBar.text] || [user.profileInfo localizedCaseInsensitiveContainsString:searchBar.text]|| [user.city localizedCaseInsensitiveContainsString:searchBar.text] || [user.state localizedCaseInsensitiveContainsString:searchBar.text]) {
            [_usersFound addObject:user];
        }
    }
    [self.tableView reloadData];
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.searchBar resignFirstResponder];
    [self.searchBar endEditing:YES];
}


-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    self.searchBar.showsCancelButton = YES;
    
}


-(void) searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
    self.searchBar.showsCancelButton = NO;
}
    
-(void) resignSearchResponder{
    [self.searchBar resignFirstResponder];
    [self.searchBar endEditing:YES];
    
}
    

/***************************************************
 *
 * ---------- TABLEVIEW DELEGATE METHODS ------------
 *
 ***************************************************/

-(void)tableViewIntegration {
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.usersFound.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"UsersTableViewCell";
    UsersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.clipsToBounds = NO;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOpacity = 0.3;
    cell.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    cell.backGroundRoundedView.layer.cornerRadius = 15 ;
    cell.backGroundRoundedView.clipsToBounds = YES;
    
    if (cell == nil) {
        
        cell = [[UsersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:CellIdentifier];
    }

    UserObject *user = self.usersFound[indexPath.row];
    cell.userName.text = user.firstName;
    
    NSString *profileBoosted = user.boostedProfile;
    
    NSDate *now = [NSDate date];
    double nowMillis = [DataManager convertToMillisSince1970:now];
    double sExpireTime = [user.boostExpireTime doubleValue];
    
    if ([profileBoosted isEqualToString: @"1"] && (nowMillis < sExpireTime)){
        cell.backGroundRoundedView.backgroundColor = [DataManager getLightGreenColor];
        [cell.waveBtn setHidden:YES];
        [cell.messageBtn setHidden:YES];
        [cell.boostTimerLbl setHidden:NO];
        double interval = sExpireTime - nowMillis;
                
        NSInteger ti = (NSInteger)interval;
        NSInteger minutes = (ti / 60) % 60;
        NSInteger hours = (ti / 3600);
    
        cell.boostTimerLbl.text = [NSString stringWithFormat:@"Expires In: %02ldhr %02ldmin", (long)hours, (long)minutes];

        
    }else{
        cell.backGroundRoundedView.backgroundColor = [UIColor whiteColor];
        [cell.waveBtn setHidden:NO];
        [cell.messageBtn setHidden:NO];
        [cell.boostTimerLbl setHidden:YES];
    }
    
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backGroundRoundedView.clipsToBounds = NO;
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width / 2;
    cell.userImageView.clipsToBounds = YES;
    [cell.userImageView setImage:[self imageForIndexPath:indexPath]];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectedBackgroundView = nil;
    
    [cell.messageBtn setTintColor:[DataManager getGreenColor]];
    cell.messageBtn.tag = indexPath.row;
    [cell.messageBtn addTarget:self action:@selector(cellMessageAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.waveBtn.tag = indexPath.row;
    [cell.waveBtn addTarget:self action:@selector(cellWaveAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.waveBtn setTintColor:[DataManager getGreenColor]];
    
    cell.cityLbl.text = user.city;
    cell.stateLbl.text = user.state;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:indexPath.row forKey:@"CellNum"];
    [defaults synchronize];
    
    self.selectedUser = self.usersFound[indexPath.row];
    
    //disable table view interaction or stuff can be touched when viewing the popup view
    self.tableView.userInteractionEnabled = NO;
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
    appDelegate.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeNone;
    self.backBtn.userInteractionEnabled = NO;
    self.openDrawerBtn.userInteractionEnabled = NO;
    
    NSLog(@"User Linked Instagram: %@",_selectedUser.instagramLinked );
    
    //determine popup view based on instagram linked or not
    if (![_selectedUser.instagramLinked isEqualToString:@"0"]){
        
        InstagramProfilePopUp *profilePopUp = [[InstagramProfilePopUp alloc] initWithNibName:@"InstagramProfilePopUp" bundle:nil];
        profilePopUp.user = self.usersFound[indexPath.row];
        profilePopUp.cachedImages = self.cachedImages;
        
        profilePopUp.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width*0.85, self.view.frame.size.height*0.9);
        
        [self presentPopupViewController:profilePopUp animated:YES completion:^(void) {
            
            [profilePopUp.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [profilePopUp.messageBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [profilePopUp.waveBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [profilePopUp.flagBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];

            UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToDismiss)];
            swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
            [profilePopUp.view addGestureRecognizer:swipeDown];
            
        }];
        
    }else{
        
        ProfilePopUp *profilePopUp = [[ProfilePopUp alloc] initWithNibName:@"ProfilePopUp" bundle:nil];
        profilePopUp.user = self.usersFound[indexPath.row];
        profilePopUp.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width*0.85, self.view.frame.size.height*0.7);
        
        [self presentPopupViewController:profilePopUp animated:YES completion:^(void) {
            
            [profilePopUp.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [profilePopUp.messageBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [profilePopUp.waveBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [profilePopUp.flagBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            
            UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToDismiss)];
            swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
            [profilePopUp.view addGestureRecognizer:swipeDown];
            
        }];
    }
}





/*************************************************
 *
 * ------------- ACTION METHODS ---------------
 *
 *************************************************/


//Goes to the Inbox View when button in top right is pressed
- (IBAction)inboxAction:(id)sender {
    //    [self performSegueWithIdentifier:@"ToInbox" sender:sender];
    NSLog(@"Open Right Drawer Touched");
    
    [_refreshControl endRefreshing];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
    
}



//will show the left drawer on right nav bar button pressed

- (IBAction)openDrawerAction:(id)sender {
    
    NSLog(@"Open Left Drawer Touched");
    [_refreshControl endRefreshing];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    
    NSLog(@"Open Drawer Touched");
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}



-(void)cellMessageAction:(UIButton*)sender
{
    {
        self.selectedUser = self.usersFound[sender.tag];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"" forKey:@"selectedConversationId"];
        
        [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
    }
}

-(void)cellWaveAction:(UIButton*)sender
{
    
    UserObject *selectedToSend = self.usersFound[sender.tag];
    
    [SVProgressHUD show];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.drawerController.chat connectAndSendWave:selectedToSend.uid token:selectedToSend.fcmToken andName:selectedToSend.firstName WithCompletion:^(BOOL success) {
        if (success) {
            
            [JSQSystemSoundPlayer jsq_playMessageSentSound];
            
            alert=   [UIAlertController
                      alertControllerWithTitle:@"Handshake sent"
                      message:@"Handshake has been successfully sent"
                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [alert addAction:ok];
            [SVProgressHUD dismiss];
            [self presentViewController:alert animated:YES completion:nil];

            
        }
        else{
            alert=   [UIAlertController
                      alertControllerWithTitle:@"Handshake failed"
                      message:@"Handshake has failed to send"
                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [alert addAction:ok];
            [SVProgressHUD dismiss];
            
            [self presentViewController:alert animated:YES completion:nil];

        }
    }];
}


-(void)cellPicAction:(UIButton*)sender
{
    
    self.selectedUser = self.usersFound[sender.tag];
    [self performSegueWithIdentifier:@"ToProfileView" sender:nil];
    
}

-(void)swipeToDismiss{
    
    if (self.popupViewController != nil) {
        
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            appDelegate.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
            appDelegate.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
            
            self.backBtn.userInteractionEnabled = YES;
            self.openDrawerBtn.userInteractionEnabled = YES;
            self.tableView.userInteractionEnabled = TRUE;
            self.popupViewController = nil;
            
        }];
    }
}


- (void)dismissPopup {
    
    if (self.popupViewController != nil) {
        
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            appDelegate.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
            appDelegate.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
            
            self.backBtn.userInteractionEnabled = YES;
            self.openDrawerBtn.userInteractionEnabled = YES;
            self.tableView.userInteractionEnabled = TRUE;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSInteger btnNum = [[defaults valueForKey:@"BtnNum"] integerValue];
            
            self.popupViewController = nil;
            
            if (btnNum != -1) {
                
                if (btnNum == -3) {
                    self.selectedUser = self.usersFound[[[defaults valueForKey:@"CellNum"] integerValue]];
                    [self performSegueWithIdentifier:@"ToProfileView" sender:nil];
                    
                    [defaults setInteger:-1 forKey:@"BtnNum"];
                    [defaults synchronize];
                }
                else
                    if (btnNum == -4)
                    {
                        
                        [self fetchUsersAsync:YES];
                        [defaults setInteger:-1 forKey:@"BtnNum"];
                        [defaults synchronize];
                        
                    }
                
                else
                    if (btnNum == -5)
                    {
                        
                        //handshake action
                        [appDelegate.drawerController.chat connectAndSendWave:self.selectedUser.uid token:self.selectedUser.fcmToken andName:self.selectedUser.firstName WithCompletion:^(BOOL success) {
                            if (success) {
                                
                                
                                [JSQSystemSoundPlayer jsq_playMessageSentSound];
                                alert=   [UIAlertController
                                          alertControllerWithTitle:@"Handshake sent"
                                          message:@"Handshake has been successfully sent"
                                          preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction* ok = [UIAlertAction
                                                     actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                                                     {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                         
                                                     }];
                                
                                [alert addAction:ok];
                                
                                [self presentViewController:alert animated:YES completion:nil];

                                
                            }
                            else{
                                
                                alert=   [UIAlertController
                                          alertControllerWithTitle:@"Handshake failed"
                                          message:@"Handshake has failed to send"
                                          preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction* ok = [UIAlertAction
                                                     actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                                                     {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                         
                                                     }];
                                
                                [alert addAction:ok];
                                
                                [self presentViewController:alert animated:YES completion:nil];
                            }
                        }];
                        
                        
                        [defaults setInteger:-1 forKey:@"BtnNum"];
                        [defaults synchronize];
                        
                    }else if(btnNum == -6){
                        
                        [defaults setInteger:-1 forKey:@"BtnNum"];
                        [defaults synchronize];
                        [self performSegueWithIdentifier:@"toFlagUserView" sender:nil];
                        
                        
                    }
                    else
                    {
                        
                        self.selectedUser = self.usersFound[[[defaults valueForKey:@"CellNum"] integerValue]];
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setValue:@"" forKey:@"selectedConversationId"];
                        
                        [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                    }
            }
        }];
    }
}





/****************************************
 *
 * --------- LOCATION METHODS ----------
 *
 ****************************************/



- (void)startSingleLocationRequest:
(void(^)(NSString *))completionBlock2

{
    
    __weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                                                timeout:10.0
                                                   delayUntilAuthorized:YES
                                                                  block:
                              ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                  __typeof(weakSelf) strongSelf = weakSelf;
                                  
                                  if (status == INTULocationStatusSuccess) {
                                      // achievedAccuracy is at least the desired accuracy (potentially better)
                                      
                                      NSLog(@"%@",[NSString stringWithFormat:@"Location request successful! Current Location:\n%@", currentLocation]);
                                    
                                      NSString *latStr = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
                                      NSString *longStr = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
                                      
                                      double latDouble = [latStr doubleValue];
                                      double longDouble = [longStr doubleValue];
                                      
                                      AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                                      
                                      [appDelegate.drawerController.chat.currentUser setLongitude:longStr];
                                      [appDelegate.drawerController.chat.currentUser setLatitude:latStr];
                                      
                                      [DataManager getAddressFromLatLon:currentLocation completion:^(CLPlacemark *placemark) {
                                          
                                          NSString *city = [NSString stringWithFormat:@"%@",placemark.locality];
                                          NSString *state = [NSString stringWithFormat:@"%@",placemark.administrativeArea];
                                          
                                          [appDelegate.drawerController.chat.currentUser setCity:city];
                                          [appDelegate.drawerController.chat.currentUser setState:state];
                                          self.chat.currentUser = appDelegate.drawerController.chat.currentUser;
                                          
                                          //SET LOCATION
                                          
                                          NSString *uid = [[FIRAuth auth] currentUser].uid;
                                          [FirebaseManager setGeoFireLocationForUser:uid latitude:latDouble longitude:longDouble];
                                          
                                          [FirebaseManager updateFirebaseUserValue:@"latitude" value:latStr uid:uid];
                                          [FirebaseManager updateFirebaseUserValue:@"city" value:city uid:uid];
                                          [FirebaseManager updateFirebaseUserValue:@"state" value:state uid:uid];
                                          [FirebaseManager updateFirebaseUserValue:@"longitude" value:longStr uid:uid];
                                          
                                          self.chat.currentUser = appDelegate.drawerController.chat.currentUser;
                                          
                                          strongSelf.locationRequestID = NSNotFound;
                                          completionBlock2(@"Location Retrieved");
                                          
                                      }];
                                      
                                  }
                                  
                                  
                                  else if (status == INTULocationStatusTimedOut) {
                                      
                                      // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                      NSLog(@"%@",[NSString stringWithFormat:@"Location request timed out. Current Location:\n%@", currentLocation]);
                                      completionBlock2(@"Location Request TImeout");
                                      
                                  }
                                  else {
                                      
                                      // An error occurred
                                      NSLog(@"%@",[strongSelf getLocationErrorDescription:status]);
                                      completionBlock2(@"Error Occured - Single Location Request");
                                      
                                  }

                              }];
}

/*
 * Saving Location responders
 */

-(void)pointResponseHandler:(id) savedUser{
    
    NSLog(@"User Location Updated" );
    [self.tableView reloadData];

    
}



- (NSString *)getLocationErrorDescription:(INTULocationStatus)status
{
    
    if (status == INTULocationStatusServicesNotDetermined) {
        return @"Error: User has not responded to the permissions alert.";
    }
    if (status == INTULocationStatusServicesDenied) {
        return @"Error: User has denied this app permissions to access device location.";
    }
    if (status == INTULocationStatusServicesRestricted) {
        return @"Error: User is restricted from using location services by a usage policy.";
    }
    if (status == INTULocationStatusServicesDisabled) {
        return @"Error: Location services are turned off for all apps on this device.";
    }
    return @"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)";
    
}




/*********************************************
 *
 * ---------- NAVIGATION METHODS ------------
 *
 *********************************************/

- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    _chat = appDelegate.drawerController.chat;
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.selectedUser = self.selectedUser;
        vc.currentUser = [self.chat currentUser];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.selectedUser.uid forKey:@"selectedUserId"];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        
        [defaults setValue:@"SearchView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = appDelegate.drawerController;
        _chat.delegate = segue.destinationViewController;
        [SVProgressHUD dismiss];
    }else if([segue.identifier isEqualToString:@"ToProfileView"]){
        ProfileViewController *profileView = segue.destinationViewController;
        profileView.currentUser = self.chat.currentUser;
        profileView.selectedUser = self.selectedUser;
        
        [profileView setChat:_chat];
        _chat.returned = appDelegate.drawerController;
        _chat.delegate = profileView;
        [SVProgressHUD dismiss];
    }else if([segue.identifier isEqualToString:@"toFlagUserView"]){
        FlagViewController *fvc = segue.destinationViewController;
        
        fvc.senderView = @"list";
        fvc.selectedUser = self.selectedUser;
        
        [fvc setChat:_chat];
        _chat.returned = appDelegate.drawerController;
        _chat.delegate = fvc;
        [SVProgressHUD dismiss];
        
        NSLog(@"UserName: %@", fvc.selectedUser.uid);

    }else if([segue.identifier isEqualToString:@"EditProfileSegue"]){
        
        EditProfileViewController *messageView = segue.destinationViewController;
        [messageView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = messageView;
        
    }
    
    _chat = nil;
    
}

//on returning to Search View Controller, update badges at hub and ensure TS messages will show
- (IBAction)unwindToList:(UIStoryboardSegue *)unwindSegue
{
    [self resignFirstResponder];
    _chat.showTSMessage = YES;
    
}

- (IBAction)unwindToBlockList:(UIStoryboardSegue *)unwindSegue
{
    
    
}



/*************************************************
 *
 * --------- CHATOBJECT DELEGATE METHODS ----------
 *
 *************************************************/



-(int)getHubCount {
    
    return [_hub count];
    
}

-(void)decrementHubCount{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
       [_hub decrement];
        
        if([_hub count] == 0){
            
            [_hub setCount:0];
            [_hub hideCount];
            [_hub setCircleColor:[UIColor clearColor] labelColor:[UIColor clearColor]];
        }
    });
}


-(void)incrementHubCount{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_hub setCircleColor:[UIColor redColor] labelColor:[UIColor whiteColor]];
        [_hub showCount];
        [_hub increment];
        [_hub pop];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    });
    
}





-(void)notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSLog(@"Notif touched handled by Drawer");
    //do nothing
}







@end
