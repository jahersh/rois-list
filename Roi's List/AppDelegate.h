//
//  AppDelegate.h
//  LiveChatWithNearest
//
//  Created by ibuildx on 2/26/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Drawer.h"
#import "FirebaseManager.h"
#import <Firebase.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <UserNotifications/UserNotifications.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>

//
//@import Firebase;
//@import FirebaseMessaging;
//@import UserNotifications;
//@import FirebaseInstanceID;


@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

@property Drawer *drawerController;
-(MMDrawerSide)openSide;

@property (nonatomic) BOOL leftDrawerWasOpen;
@property NetworkCheck *reach;
@property BOOL changeCount;

@end

