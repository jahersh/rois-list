//
//  FlagViewController.m
//  Complice
//
//  Created by Justin Hershey on 10/31/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import "FlagViewController.h"
#import <TSMessage.h>
#import <SVProgressHUD.h>
#import "SearchViewController.h"
#import "BlockViewController.h"
#import "AppDelegate.h"
#import <MMDrawerController.h>


@implementation FlagViewController


- (UIStatusBarStyle)preferredStatusBarStyle
{
    
    return UIStatusBarStyleLightContent;
    
}


-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self setupUI];
    
    self.flagUserText.text = @"What is your reason for flagging this user?";
    self.flagUserText.textColor = [UIColor lightGrayColor];
    self.flagUserText.delegate = self;
    [self.nameLbl setText:self.selectedUser.firstName];

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}



-(void) setupUI {
    
    self.submitBtn.layer.cornerRadius = 15;
    self.submitBtn.clipsToBounds = NO;
    self.submitBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.submitBtn.layer.shadowOpacity = 0.4;
    self.submitBtn.layer.shadowRadius = 8;
    self.submitBtn.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
}




/*****************************************
 *
 * ----- ACTION METHODS -----
 *
 *****************************************/

- (IBAction)submitFlagAction:(id)sender {
    
    [self sendEmailToSupport:self.flagUserText.text from:self.chat.currentUser.firstName email:self.chat.currentUser.email];
}


- (IBAction)backAction:(id)sender {
    
    [self performSegueWithIdentifier:@"backToBlock" sender:nil];

}



- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    self.flagUserText.text = @"";
    self.flagUserText.textColor = [UIColor blackColor];
    return YES;
    
}



-(void) textViewDidChange:(UITextView *)textView
{
    
    if(self.flagUserText.text.length == 0){
        
        self.flagUserText.textColor = [UIColor lightGrayColor];
        self.flagUserText.text = @"Why are you flagging this user?";
        [self.flagUserText resignFirstResponder];
    }
}


/*****************************************
 *
 * ----- SEND EMAIL METHOD -----
 *
 *****************************************/

-(void)sendEmailToSupport :(NSString *)msg from :(NSString *)senderName email :(NSString *)email {
    
    [SVProgressHUD show];
    NSString *userID = self.selectedUser.uid;
    NSString *flaggedName = self.selectedUser.firstName;
    NSLog(@"usersName: %@",flaggedName);
    NSLog(@"userID: %@", userID);
    
    
    NSString *body = [NSString stringWithFormat:@"%@ Is Flagging user %@ (%@). Reason : %@  Offender UserID: %@", senderName, flaggedName, email, msg, userID];
    
    
    [FirebaseManager flagUser:self.chat.currentUser.uid reason:body offenderUID:userID offenderEmail:self.selectedUser.email reporterEmail:self.selectedUser.email completion:^(bool success) {
        
        if (success) {
            
            [SVProgressHUD dismiss];
            NSLog(@"ASYNC: HTML email has been sent");
            
            
            _alert=   [UIAlertController
                       alertControllerWithTitle:@"Success"
                       message:@"Flag has been sent to Support, You will be notified with reply a soon."
                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [_alert dismissViewControllerAnimated:YES completion:nil];
                                     [self performSegueWithIdentifier:@"unwindToBlock" sender:nil];
                                     
                                 }];
            
            [_alert addAction:ok];
            
            [self presentViewController:_alert animated:YES completion:nil];

        }else{
            
                     [SVProgressHUD dismiss];
                     NSLog(@"Server reported an error");
            
            
                     _alert=   [UIAlertController
                               alertControllerWithTitle:@"Failed"
                               message:@"Flag cannot be sent, Please try again Later."
                               preferredStyle:UIAlertControllerStyleAlert];
            
                     UIAlertAction* ok = [UIAlertAction
                                          actionWithTitle:@"OK"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              [_alert dismissViewControllerAnimated:YES completion:nil];
                                              [self performSegueWithIdentifier:@"backToBlock" sender:nil];
            
                                          }];
                     
                     [_alert addAction:ok];
                     
                     [self presentViewController:_alert animated:YES completion:nil];
            
        }
    }];
}



/*****************************************
 *
 * ----- NAVIGATION METHODS -----
 *
 *****************************************/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller

    if ([segue.identifier isEqualToString:@"backToSearch"]) {
        
        SearchViewController *searchView = segue.destinationViewController;
        [searchView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = searchView;
        
    } else if ([segue.identifier isEqualToString:@"backToBlock"]) {
        
        if([self.senderView  isEqual: @"list"]){
            
            SearchViewController *searchView = segue.destinationViewController;
            
            [searchView setChat:_chat];
            _chat.returned = self;
            _chat.delegate = searchView;
            
        }else{
            
            BlockViewController *blockView = segue.destinationViewController;
            
            [blockView setChat:_chat];
            _chat.returned = self;
            _chat.delegate = blockView;
        }
    }
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"FlagView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;

        [SVProgressHUD dismiss];
        
    }
    
    _chat = nil;
}


- (IBAction)unwindToFlag:(UIStoryboardSegue *)unwindSegue
{
    
}




/*****************************************
 *
 * ----- CHABOBJECT DELEGATE METHODS -----
 *
 *****************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                           
                                           
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}




@end
