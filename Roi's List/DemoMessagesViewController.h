//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//


// Import all the things
#import "JSQMessages.h"
#import "ChatObject.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>


@class ChatObject;


@class DemoMessagesViewController;

@protocol JSQDemoViewControllerDelegate <NSObject>

- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc;

@end



@interface DemoMessagesViewController : JSQMessagesViewController <UIActionSheetDelegate, JSQMessagesComposerTextViewPasteDelegate, ChatProtocol, JSQDemoViewControllerDelegate,UIGestureRecognizerDelegate, UITextViewDelegate>


@property (weak, nonatomic) id<JSQDemoViewControllerDelegate> delegateModal;
@property BOOL scrollViewInitialScroll;
@property (strong) NSMutableArray *messages;
@property(nonatomic,strong) NSMutableArray *inboxData;

- (void)closePressed:(UIBarButtonItem *)sender;

@property (strong) NSString *conversationId;
@property (strong) UserObject *currentUser;
@property (strong) UserObject *selectedUser;
@property (strong, nonatomic) ChatObject *chat;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@end
