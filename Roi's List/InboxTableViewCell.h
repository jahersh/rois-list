//
//  InboxTableViewCell.h
//  Accomplice
//
//  Created by ibuildx on 4/19/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *userImageView;

@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView *unseenMsgView;

@property (strong, nonatomic) IBOutlet UILabel *userIntro;


@property (strong, nonatomic) IBOutlet UIView *backGroundRoundedView;

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *unblockBtn;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet UIButton *flagBtn;

@end
