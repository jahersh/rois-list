//
//  IntroViewController.h
//  Complice
//
//  Created by Justin Hershey on 4/19/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *skipButton;

@end
