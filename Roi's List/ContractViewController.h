//
//  ContractViewController.h
//  Complice
//
//  Created by Justin Hershey on 12/16/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContractViewController : UIViewController <UITextViewDelegate, UIScrollViewDelegate, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webview;

@property NSString* contractString;

@property (weak, nonatomic) IBOutlet UIButton *declineBtn;

@property (weak, nonatomic) IBOutlet UILabel *scrollToAcceptLbl;

@property (weak, nonatomic) IBOutlet UIImageView *downArrow;

- (IBAction)declineAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
- (IBAction)acceptAction:(id)sender;

@end
