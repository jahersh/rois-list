//
//  TermsViewController.m
//  Complice
//
//  Created by Justin Hershey on 5/7/16.
//  Copyright © 2016 fenapnu. All rights reserved.
//

#import "TermsViewController.h"
#import <TSMessage.h>
#import "LegalViewController.h"
#import <SVProgressHUD.h>

@interface TermsViewController ()

@end

@implementation TermsViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    
    [self maskTriananlgesAndCircularBorderedImage];
    
    NSString *path;
    
    
    
    if ([self.legalView  isEqual: @"privacy"]) {
        self.userNameText.text  = @"PRIVACY POLICY" ;
        
        path = [[NSBundle mainBundle] pathForResource:@"PrivacyPolicy10" ofType:@"html"];
        
    }else if([self.legalView  isEqual: @"usage"]){
        self.userNameText.text = @"GUIDELINES";
        path = [[NSBundle mainBundle] pathForResource:@"CompliceGuidelines" ofType:@"html"];
        
    }
    else if ([self.legalView  isEqual: @"terms"]){
        self.userNameText.text  = @"TERMS OF USE" ;
        
        path = [[NSBundle mainBundle] pathForResource:@"CompliceTermsofUse10" ofType:@"html"];
        
    }
    
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    _webview.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    [_webview loadRequest:request];
    

}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationIsActive:)
//                                                 name:UIApplicationDidBecomeActiveNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationDidEnterBackground:)
//                                                 name:UIApplicationDidEnterBackgroundNotification
//                                               object:nil];
    
}



-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}

//-(void)applicationDidEnterBackground:(NSNotification *)notification{
//    
//    _chat.showTSMessage = NO;
//}
//
//- (void)applicationIsActive:(NSNotification *)notification {
//    NSLog(@"Application Did Become Active");
//    if(!notification){
//        _chat.showTSMessage = YES;
//    }
//    
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/**************************************
 *
 * -------- UI SETUP METHODS ----------
 *
 *************************************/

-(void)maskTriananlgesAndCircularBorderedImage {
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){0, 0}];
    [path addLineToPoint:(CGPoint){0, self.leftDarkView.frame.size.height}];
    [path addLineToPoint:(CGPoint){self.view.frame.size.width / 2, 0}];
    [path addLineToPoint:(CGPoint){0, 0}];
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = self.leftDarkView.bounds;
    mask.path = path.CGPath;
    // Mask the imageView's layer with this shape
    self.leftDarkView.layer.mask = mask;
    // Build a triangular path
    UIBezierPath *pathRight = [UIBezierPath new];
    [pathRight moveToPoint:(CGPoint){0, 0}];
    [pathRight addLineToPoint:(CGPoint){self.view.frame.size.width / 2 , self.leftDarkView.frame.size.height}];
    [pathRight addLineToPoint:(CGPoint){self.view.frame.size.width / 2 , 0}];
    [pathRight addLineToPoint:(CGPoint){0, 0}];
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *maskRight = [CAShapeLayer new];
    maskRight.frame = self.rightDarkView.bounds;
    maskRight.path = pathRight.CGPath;
    // Mask the imageView's layer with this shape
    self.rightDarkView.layer.mask = maskRight;
}


/**************************************
 *
 * -------- ACTION METHODS ----------
 *
 *************************************/

- (IBAction)backBtnAction:(id)sender {
//    [self performSegueWithIdentifier:@"backToLegal" sender:nil];
    
}



/**************************************
 *
 * --- CHATOBJECT DELEGATE METHODS ----
 *
 *************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                           
                                           
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}


- (IBAction)unwindToTerms:(UIStoryboardSegue *)unwindSegue
{
}

/**************************************
 *
 * ------- NAVIGATION METHODS ---------
 *
 *************************************/
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"backToLegal"]) {
        LegalViewController *legalView = segue.destinationViewController;
        //        dashView.fbObject = self.fbObject;
        
        [legalView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = legalView;
    }else if ([segue.identifier isEqualToString:@"segueModalDemoVC"]){
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"TermsView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;
    }
    _chat = nil;
}



@end
