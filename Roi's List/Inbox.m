//
//  Inbox.m
//  Accomplice
//
//  Created by Muhammad Saqib Yaqeen on 4/1/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "Inbox.h"

@implementation Inbox

//To sort the inbox by date created
-(NSComparisonResult) compare:(Inbox*) other {
    return [other.created compare:self.created];
}


-(NSDictionary*) inboxToDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:_senderID forKey:@"senderID"];
    [dictionary setValue:_receiverID forKey:@"receiverID"];
    [dictionary setValue:_isSeen forKey:@"isSeen"];
    [dictionary setValue:_messageBody forKey:@"messageBody"];
    [dictionary setValue:_created forKey:@"created"];
    [dictionary setValue:_objectId forKey:@"objectId"];
    
    return dictionary;
    
}

-(void) dictionaryToInbox:(NSDictionary*)userdict{
    
    self.senderID = [userdict valueForKey:@"senderID"];
    self.receiverID = [userdict valueForKey:@"receiverID"];
    self.isSeen = [userdict valueForKey:@"isSeen"];
    self.messageBody = [userdict valueForKey:@"messageBody"];
    self.created = [userdict valueForKey:@"created"];
    self.objectId = [userdict valueForKey:@"objectId"];
    
}


@end
