//
//  UIViewController+LegalViewController.h
//  Complice
//
//  Created by Justin Hershey on 10/30/16.
//  Copyright © 2016 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMRangeSlider.h"
#import "DemoMessagesViewController.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>

@interface LegalViewController : UIViewController <ChatProtocol>

@property NSString* legalView;


//Buttons
@property (weak, nonatomic) IBOutlet UIButton *privacyBtn;
@property (weak, nonatomic) IBOutlet UIButton *termsBtn;
@property (weak, nonatomic) IBOutlet UIButton *usageBtn;
@property (weak, nonatomic) IBOutlet UIButton *backToSettingsBtn;

//Actions

- (IBAction)privacyAction:(id)sender;
- (IBAction)termsAction:(id)sender;
- (IBAction)usageAction:(id)sender;
- (IBAction)backToSettingsAction:(id)sender;

@property (nonatomic, strong) ChatObject *chat;
@property (strong) UserObject *currentUser;

@property (strong) UserObject *selectedUser;

@end
