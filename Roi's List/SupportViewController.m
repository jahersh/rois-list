//
//  SupportViewController.m
//  Accomplice
//
//  Created by ibuildx on 3/18/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "SupportViewController.h"
#import "DemoMessagesViewController.h"
#import <SVProgressHUD.h>
#import <TSMessage.h>
#import "DashBoardViewController.h"
#import "FaqViewController.h"
#import "LegalViewController.h"
#import <FBSDKLoginManager.h>

@interface SupportViewController () <ChatProtocol, UIAlertViewDelegate, UITextViewDelegate>{
    
    UIAlertController *alert;
}


@end

@implementation SupportViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



#pragma mark - View Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.view layoutIfNeeded];

    [self maskTriananlgesAndCircularBorderedImage];
    
    if (!self.usersFound) {
        self.usersFound = [[NSMutableArray alloc] init];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *fbPicString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=1080&height=1080",self.chat.currentUser.facebookId];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fbPicString]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.userImageView setImage:[UIImage imageWithData:data]];
            
        });
    });
    
    self.supportMessageTestView.text = @" Please enter your support query";
    self.supportMessageTestView.textColor = [UIColor lightGrayColor];
    self.supportMessageTestView.delegate = self;
    
    [self setupView];
    
    _chat.delegate = self ;
    

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationIsActive:)
//                                                 name:UIApplicationDidBecomeActiveNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationDidEnterBackground:)
//                                                 name:UIApplicationDidEnterBackgroundNotification
//                                               object:nil];
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}

//-(void)applicationDidEnterBackground:(NSNotification *)notification{
//    
//    _chat.showTSMessage = NO;
//}
//
//- (void)applicationIsActive:(NSNotification *)notification {
//    NSLog(@"Application Did Become Active");
//    if(!notification){
//        _chat.showTSMessage = YES;
//    }
//    
//    
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*********************************************
 *
 * --------- UI SETUP METHODS -----------
 *
 *********************************************/

-(void) setupView{
    
    
    self.sendBtn.layer.cornerRadius = 10;
    self.sendBtn.clipsToBounds = NO;
    self.sendBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.sendBtn.layer.shadowOpacity = 0.8;
    self.sendBtn.layer.shadowRadius = 8;
    self.sendBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.sendBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.sendBtn.titleLabel.layer.shadowRadius = 5;
    self.sendBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.sendBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    
    self.faqBtn.layer.cornerRadius = 10;
    self.faqBtn.clipsToBounds = NO;
    self.faqBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.faqBtn.layer.shadowOpacity = 0.8;
    self.faqBtn.layer.shadowRadius = 8;
    self.faqBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.faqBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.faqBtn.titleLabel.layer.shadowRadius = 5;
    self.faqBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.faqBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    
    self.legalBtn.layer.cornerRadius = 10;
    self.legalBtn.clipsToBounds = NO;
    self.legalBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.legalBtn.layer.shadowOpacity = 0.8;
    self.legalBtn.layer.shadowRadius = 8;
    self.legalBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    self.legalBtn.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.legalBtn.titleLabel.layer.shadowRadius = 5;
    self.legalBtn.titleLabel.layer.shadowOpacity = 0.4;
    self.legalBtn.titleLabel.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    
    self.supportMessageTestView.clipsToBounds = NO;
    self.supportMessageTestView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.supportMessageTestView.layer.shadowOpacity = 0.8;
    self.supportMessageTestView.layer.shadowRadius = 8;
    self.supportMessageTestView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    
    
}
#pragma mark - View Starting Methods

-(void)maskTriananlgesAndCircularBorderedImage {

    self.userImageView.backgroundColor = [UIColor redColor];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
    self.userImageView.clipsToBounds = YES;
    UIColor *bgColor = self.backgroundLightView.backgroundColor;
    self.userImageView.layer.borderWidth = self.userImageView.frame.size.width / 20;
    self.userImageView.layer.borderColor = bgColor.CGColor;
}



/*********************************************
 *
 * ------- TEXTVIEW DELEGATE METHODS ---------
 *
 *********************************************/

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    self.supportMessageTestView.text = @"";
    self.supportMessageTestView.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(self.supportMessageTestView.text.length == 0){
        self.supportMessageTestView.textColor = [UIColor lightGrayColor];
        self.supportMessageTestView.text = @" Please enter your support query";
        [self.supportMessageTestView resignFirstResponder];
    }
}


/*********************************************
 *
 * --------- ACTION METHODS -----------
 *
 *********************************************/

- (IBAction)backBtnAction:(id)sender {
    [self performSegueWithIdentifier:@"backToDashboard" sender:nil];
    
}

- (IBAction)logOutBtnAction:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"NO" forKey:@"AlreadyUserLogin"];
    [defaults synchronize];
    
    [self performSegueWithIdentifier:@"backToLogin" sender:nil];
    
}

- (IBAction)sendAction:(id)sender {
    
    
    [self sendEmailToSupport:self.supportMessageTestView.text from:self.chat.currentUser.firstName email:self.chat.currentUser.email];
    
    
}

- (IBAction)legalAction:(id)sender {
}



/*********************************************
 *
 * --------- SUPPORT EMAIL METHOD -----------
 *
 *********************************************/

-(void)sendEmailToSupport :(NSString *)msg from :(NSString *)senderName email :(NSString *) email {
    [SVProgressHUD show];
    
//    NSString *subject = @"Support Call";
    NSString *body = [NSString stringWithFormat:@"%@ - %@ wants to know : %@", senderName, email,msg];
//    NSString *recipient = @"support@complice.club";
    
    
    
    
    
    [FirebaseManager sendSupportRequest:self.chat.currentUser.uid reason:body reporterEmail:self.selectedUser.email completion:^(bool success) {
        
        [SVProgressHUD dismiss];
        
        if (success){
            
            NSLog(@"ASYNC: HTML email has been sent");
            
            alert=   [UIAlertController
                      alertControllerWithTitle:@"Success"
                      message:@"Query has been sent to Support, You will be notified with reply a soon."
                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self performSegueWithIdentifier:@"backToDashboard" sender:nil];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        } else {
            [SVProgressHUD dismiss];
            
            alert=   [UIAlertController
                      alertControllerWithTitle:@"Failed"
                      message:@"Query cannot be sent, Please try again Later."
                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
    }];
}




/*********************************************
 *
 * --------- NAVIGATION METHODS -----------
 *
 *********************************************/

- (IBAction)unwindToSupport:(UIStoryboardSegue *)unwindSegue
{
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"SupportView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;

        
    }
    
    else if([segue.identifier isEqualToString:@"backToDashboard"]) {
            DashBoardViewController *messageView = segue.destinationViewController;
            //            messageView.fbObject = self.fbObject;
            
            [messageView setChat:_chat];
            _chat.returned = self;
            _chat.delegate = messageView;
            
            _chat = nil;
    }
    
    else if([segue.identifier isEqualToString:@"toFAQView"]) {
        FaqViewController *faqView = segue.destinationViewController;
        //            messageView.fbObject = self.fbObject;
        
        [faqView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = faqView;
        
        _chat = nil;
    }
    
    else if([segue.identifier isEqualToString:@"toLegalView"]) {
        
        LegalViewController *faqView = segue.destinationViewController;
        //            messageView.fbObject = self.fbObject;
        
        [faqView setChat:_chat];
        _chat.returned = self;
        _chat.delegate = faqView;
        
    }
    _chat = nil;
}




/*********************************************
 *
 * ------ CHATOBJECT DELEGATE METHODS --------
 *
 *********************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                           
                                           
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}


@end
