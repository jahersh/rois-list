//
//  SettingViewController.h
//  Accomplice
//
//  Created by ibuildx on 3/18/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMRangeSlider.h"
#import "DemoMessagesViewController.h"
#import <StoreKit/StoreKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>



@interface SettingViewController : UIViewController <JSQDemoViewControllerDelegate, ChatProtocol,SKProductsRequestDelegate,SKPaymentTransactionObserver>


@property (strong) NSArray  *users_Found;
@property (weak, nonatomic) IBOutlet UIView *shadowView;


@property (weak, nonatomic) IBOutlet UIView *backgroundLightView;
@property (weak, nonatomic) IBOutlet UIView *leftDarkView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *rightDarkView;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *logOutBtn;

@property (weak, nonatomic) IBOutlet UIView *propertyContractedView;
@property (weak, nonatomic) IBOutlet UISwitch *propertyContractedSwitch;


@property (strong) NSArray  *usersFound;
@property (strong) UserObject *currentUser;
@property (strong) UserObject *selectedUser;

//--------block views


@property (weak, nonatomic) IBOutlet UIView *seeMeView;
@property (weak, nonatomic) IBOutlet UIView *searchRadiusView;
@property (weak, nonatomic) IBOutlet UIView *iSeeView;

@property (weak, nonatomic) IBOutlet UIButton *legalBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

//----------buttons
@property (weak, nonatomic) IBOutlet UIButton *blockListBtn;
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;

@property (weak, nonatomic) IBOutlet UIView *linkInstagramView;
@property (weak, nonatomic) IBOutlet UISwitch *linkInstagramSwitch;
@property (weak, nonatomic) IBOutlet UILabel *linkInstaLbl;
@property (weak, nonatomic) IBOutlet UISwitch *premiumSwitch;
@property (weak, nonatomic) IBOutlet UIView *premiumView;
@property (weak, nonatomic) IBOutlet UILabel *goPremiumLbl;
@property (weak, nonatomic) IBOutlet UISwitch *notifySwitch;
@property (weak, nonatomic) IBOutlet UIView *notifyView;


- (IBAction)backBtnAction:(id)sender;
- (IBAction)logOutBtnAction:(id)sender;
- (IBAction)deleteLogoutAction:(id)sender;
- (IBAction)linkInstagramAction:(id)sender;
- (IBAction)propertySwitchAction:(id)sender;
- (IBAction)premiumSwitchAction:(id)sender;
- (IBAction)notifySwitchAction:(id)sender;

@property BOOL isPrivacy ;

- (IBAction)searchUsersBtn:(id)sender;


@property NSString * userId;


@property (weak, nonatomic) IBOutlet UILabel *propertyUnderContractLbl;

- (IBAction)blockListAction:(id)sender;
- (IBAction)legalAction:(id)sender;

@property (weak, nonatomic) IBOutlet UISlider *distanceSlider;
@property (weak, nonatomic) IBOutlet UILabel *radiusText;
- (IBAction)distanceSliderValueChanged:(UISlider *)sender;

@property (nonatomic, strong) ChatObject *chat;

@property NSString *kPropertyContractedProductID;
@property SKProductsRequest *productsRequest;
@property NSArray *validProducts;
@property UIActivityIndicatorView *activityIndicatorView;

- (void)fetchAvailableProducts;
- (BOOL)canMakePurchases;
- (void)purchaseMyProduct:(SKProduct*)product;


@end
