//
//  ProfileViewController.m
//  Accomplice
//
//  Created by ibuildx on 3/15/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "ProfileViewController.h"
#import "DemoMessagesViewController.h"
#import "ChatObject.h"
#import "DataManager.h"
#import <TSMessage.h>
#import "DashBoardViewController.h"
#import "AppDelegate.h"


@interface ProfileViewController (){

}


@end

@implementation ProfileViewController

#pragma mark - View Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view layoutIfNeeded];
    [self maskTriananlgesAndCircularBorderedImage];


    NSLog(@"backendless user Obj Id = %@",self.chat.currentUser.uid);
    
    _chat.showTSMessage = YES;

    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    
}

//-(void)applicationDidEnterBackground:(NSNotification *)notification{
//    
//    _chat.showTSMessage = NO;
//    
//}
//
//- (void)applicationIsActive:(NSNotification *)notification {
//    NSLog(@"Application Did Become Active");
//    if(!notification){
//        _chat.showTSMessage = YES;
//    }
//    
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*************************************************
 *
 * ---------- UI SETUP METHODS ------------
 *
 *************************************************/

-(void)maskTriananlgesAndCircularBorderedImage {

    self.userImageView.backgroundColor = [UIColor redColor];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
    self.userImageView.clipsToBounds = YES;
    UIColor *bgColor = [UIColor whiteColor];
    self.userImageView.layer.borderWidth = self.userImageView.frame.size.width / 30;
    self.userImageView.layer.borderColor = bgColor.CGColor;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *imgUrl = self.chat.currentUser.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.userImageView setImage:[UIImage imageWithData:data]];
            
        });
    });
    
    self.userNameText.text = self.chat.currentUser.firstName;
    
    self.ageText.text = [NSString stringWithFormat:@"%@, %@",self.chat.currentUser.city, self.chat.currentUser.state];
    self.profileInfoText.text = self.chat.currentUser.profileInfo;
    
    [self setupView];
}

-(void) setupView{
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.6;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);

    self.ageText.clipsToBounds = NO;
    self.ageText.layer.shadowColor = [UIColor blackColor].CGColor;
    self.ageText.layer.shadowOpacity = 0.6;
    self.ageText.layer.shadowRadius = 8;
    self.ageText.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
}


/*************************************************
 *
 * ---------- CHATOBJECT DELEGATE METHODS ------------
 *
 *************************************************/



-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                           
                                           
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}




/*************************************************
 *
 * ---------- ACTION METHODS ------------
 *
 *************************************************/
- (IBAction)backBtnAction:(id)sender {
    [self performSegueWithIdentifier:@"backToList" sender:nil];
    
}

- (IBAction)logOutBtnAction:(id)sender {
    [self performSegueWithIdentifier:@"backToLogin" sender:nil];
    
}

- (IBAction)startChatAction:(id)sender {
    

    [self performSegueWithIdentifier:@"segueModalDemoVC" sender:nil];

    
}

- (IBAction)listBtnAction:(id)sender {
    [self performSegueWithIdentifier:@"Home-List" sender:sender];
    
}

- (IBAction)settingAction:(id)sender {
    
    
}

#pragma mark - ActioSheet Method

-(void)showActioSheetWithMessage :(NSString *)msg {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        

        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}







/*************************************************
 *
 * ---------- NAVIGATION METHODS ------------
 *
 *************************************************/



// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"ProfileView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;

    }else
        if([segue.identifier isEqualToString:@"backToList"])
        {
            
            DashBoardViewController *messageView = segue.destinationViewController;
            
            [messageView setChat:_chat];
            _chat.returned = self;
            _chat.delegate = messageView;
            
        }
    _chat = nil;
}

- (IBAction)unwindToProfile:(UIStoryboardSegue *)unwindSegue
{
    
}


- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
