//
//  IntroViewController.m
//  Complice
//
//  Created by Justin Hershey on 4/19/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import "IntroViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _skipButton.layer.shadowColor = (__bridge CGColorRef _Nullable)([UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:.7]);
    
    _skipButton.layer.shadowRadius = 8.0;
    _skipButton.layer.shadowOffset = CGSizeMake(4.0, -4.0);
    
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
