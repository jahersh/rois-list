//
//  InstagramManager.m
//  Complice
//
//  Created by Justin Hershey on 7/23/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import "InstagramManager.h"


@implementation InstagramManager


//Replace all Insta Photos in Backendless with Users' most recent insta photos
- (void)replaceInstagramPictures:(NSString *)forUserID{
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"INSTA_ACCESS_TOKEN"];
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.instagram.com/v1/users/self/media/recent/?access_token=%@",token];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSURL *picEndpointURL = [NSURL URLWithString:urlString];
        NSDictionary *dataDict = [[NSDictionary alloc] init];
        @try {
            NSData *data = [NSData dataWithContentsOfURL:picEndpointURL];
            dataDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        } @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception.description);
        }
        
        NSDictionary *picDictionary = [dataDict valueForKey:@"data"];
        
        for (NSDictionary *imageSet in picDictionary) {
            
            NSDictionary *imageDict = [[imageSet valueForKey:@"images"] valueForKey:@"standard_resolution"];
            NSString *urlString = [imageDict valueForKey:@"url"];

            NSString *creationMillis = [NSString stringWithFormat:@"%.0f", [[imageSet valueForKey:@"created_time"] doubleValue] * 1000];
            
            NSString *caption = [[imageSet valueForKey:@"caption"] valueForKey:@"text"];
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            UIImage *image = [UIImage imageWithData:data];

            
            [FirebaseManager uploadPhoto:UIImageJPEGRepresentation(image, 0.1) creationDate:creationMillis caption:caption completion:^(NSString *downloadString) {
                
                if([downloadString isEqualToString:@""]){
                    
                    NSLog(@"Upload Error");
                }else{
                    
                    NSLog(@"Photo Successfully Uploaded with URL: %@", downloadString);
                }
            }];
            
        }
        
    });
    
}





//Compare Most Recent Backendless Photo update time to the most recent Instagram posts and upload new Insta
- (void)uploadNewPhotos{

    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"INSTA_ACCESS_TOKEN"];
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.instagram.com/v1/users/self/media/recent/?access_token=%@",token];
    
    __block int numberOfNewPhotos = 0;
    
    [self getPhotosListingAsync:[[FIRAuth auth] currentUser].uid completion:^(double mostRecentTime, NSDictionary* currentPhotosData) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            NSURL *picEndpointURL = [NSURL URLWithString:urlString];
            NSDictionary *dataDict = [[NSDictionary alloc] init];
            @try {
                NSData *data = [NSData dataWithContentsOfURL:picEndpointURL];
                dataDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            } @catch (NSException *exception) {
                NSLog(@"Exception: %@", exception.description);
            }
            
            NSDictionary *picDictionary = [dataDict valueForKey:@"data"];
            NSMutableDictionary *captions = [[NSMutableDictionary alloc] init];
            
            for (NSDictionary *imageSet in picDictionary) {
                
                NSDictionary *imageDict = [[imageSet valueForKey:@"images"] valueForKey:@"standard_resolution"];
                NSString *urlString = [imageDict valueForKey:@"url"];
                NSString *createdSeconds = [imageSet valueForKey:@"created_time"];
                double createdMillis = [createdSeconds doubleValue] * 1000;
                
                
                //If the photo is newer than the last update time, upload the photo
                if(createdMillis > mostRecentTime){

                    numberOfNewPhotos++;
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                    UIImage *image = [UIImage imageWithData:data];
                    
                    //Set caption in dict
                    NSString *caption = [[imageSet valueForKey:@"caption"] valueForKey:@"text"];
                    [captions setValue:caption forKey:[NSString stringWithFormat:@"%f",createdMillis]];
                    
                    [FirebaseManager uploadPhoto:UIImageJPEGRepresentation(image, 0.1) creationDate:[NSString stringWithFormat:@"%.0f", createdMillis] caption:caption completion:^(NSString *downloadString) {
                        
                        if([downloadString isEqualToString:@""]){
                            
                            NSLog(@"Upload Error");
                            
                        }else{

                            NSLog(@"Photo Successfully Uploaded with URL: %@", downloadString);
                            
                        }
                    }];
                }
            }

            
            //PHOTO CLEANUP so user has 20 photos max in BACKEND
            if(numberOfNewPhotos > 0 && ([currentPhotosData count] + numberOfNewPhotos > 20)){
                
                FIRDatabaseReference *instaRef = [[FirebaseManager getInstagramPath] child:[[FIRAuth auth] currentUser].uid];
                    
                [[[instaRef queryOrderedByKey] queryLimitedToFirst:numberOfNewPhotos] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                        
                    if (snapshot.exists){
                            
                        NSDictionary *toDelete = snapshot.value;
                        
                        for(NSString* key in toDelete){
                            
                            [FirebaseManager deletePhoto:key completion:^(bool success) {
                                
                                if (!success){
                                    NSLog(@"Error Deleting Photo");
                                }
                            
                            }];
                            
                        }
                    }
                }];
            }
        });
    }];
}



- (void)getPhotosListingAsync:(NSString*)uid completion:(void (^)(double mostRecentTime, NSDictionary* currentPhotoData))completion {
    
    __block double latestTimeCreated = 0;
    
    [FirebaseManager getPhotoDictionary:uid completion:^(NSDictionary *photosData) {
        
        
        if (photosData != nil){
            
            for(NSString *timeKey in photosData.allKeys){
                if ([timeKey doubleValue] > latestTimeCreated){
                    latestTimeCreated = [timeKey doubleValue];
                }
            }
            completion(latestTimeCreated, photosData);
        }else{
            
            completion(0,nil);
        }
    }];
}





@end
