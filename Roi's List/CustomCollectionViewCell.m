//
//  UICollectionViewCell+CustomCollectionViewCell.m
//  Complice
//
//  Created by Justin Hershey on 5/24/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import "CustomCollectionViewCell.h"

@implementation CustomCollectionViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
