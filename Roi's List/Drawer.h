//
//  Drawer.h
//  Complice
//
//  Created by Justin Hershey on 1/12/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMDrawerController.h>
#import "SearchViewController.h"
#import "InboxViewController.h"
#import "DashBoardViewController.h"
#import "ChatObject.h"
#import <MMDrawerController+Subclass.h>
#import "UIViewController+CWPopup.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <Firebase.h>
#import "FirebaseManager.h"


@interface Drawer : MMDrawerController<ChatProtocol>

@property MMDrawerController *drawerController;
@property ChatObject *chat;
@property DashBoardViewController *leftDrawer;
@property InboxViewController *rightDrawer;
@property SearchViewController *centerDrawer;

@property (strong) NSArray  *usersFound;

@property BOOL gotoChatView;
@property UserObject *currentUser;
@property UserObject *selectedUser;

//@property UIAlertController *alert;

-(ChatObject*) getChat;
-(void)showProfile;
//-(void)updateHubCount;
-(int)getHubCount;
-(void)decrementHubCount;
-(void) incrementHubCount;
//-(void) updateProfileAlert;

@end
