//
//  FirebaseManager.m
//  Complice
//
//  Created by Justin Hershey on 8/23/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import "FirebaseManager.h"



@implementation FirebaseManager


// Database References



/************************************************
 *
 * ------------  REFERENCE PATHS
 *
 ************************************************/

+(FIRDatabaseReference*) getUsersPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"Users"];
    return ref;
}

+(FIRDatabaseReference*) getBoostedNotificationUsersPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"BoostedNotifications"];
    return ref;
}

+(FIRDatabaseReference*) getInboxListPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"InboxList"];
    return ref;
}

+(FIRDatabaseReference*) getFlaggedUsersPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"FlaggedUsers"];
    return ref;
}

+(FIRDatabaseReference*) getMyBlockedUsersPath{
    
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"BlockedUsers"];
    return ref;
}

+(FIRDatabaseReference*) getBlockedMeUsersPath{
    
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"BlockedMe"];
    return ref;
}


+(FIRDatabaseReference*) getConversationPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"Conversations"];
    return ref;
}


+(FIRDatabaseReference*) getSupportPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"Support"];
    return ref;
}

+(FIRDatabaseReference*) getInstagramPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"Instagram"];
    return ref;
}

+(FIRDatabaseReference*) getGeoPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"GeoFire"];
    return ref;
}

+(FIRDatabaseReference*) getNotifcationsPath{
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"Notifications"];
    return ref;
}


+(FIRStorageReference*) getStoragePath{
    
    FIRStorage *storage = [FIRStorage storage];
    
    FIRStorageReference *storageRef = [storage reference];
    return storageRef;
}






/************************************************
 *
 * ------------ UPDATE FIREBASE VALUES
 *
 ************************************************/



+(void) updateFirebaseUserValue:(NSString*)key value:(NSString*)value uid:(NSString*)uid{
    FIRDatabaseReference *ref = [[[FirebaseManager getUsersPath] child:uid] child:key];
    
    [ref setValue:value];
    
}





/**************************************
 *
 *    USER DATA RETRIEVAL
 *
 ***************************************/

+(void) getUserFirebaseDataForUid:(NSString*)uid completion:(void (^)(UserObject* user))completion{
    
    FIRDatabaseReference *ref = [[[[FIRDatabase database] reference] child:@"Users"] child:uid];
    
    [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        
        if (snapshot.exists){
            
            UserObject *user = [[UserObject alloc] init];
            NSDictionary *dict = snapshot.value;
            
            [user setUserData:dict.mutableCopy];
            completion(user);
            
        }else{
            
            completion(nil);
        }
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}




/**************************************
 *
 *    INBOX MESSAGES DATA RETRIEVAL
 *
 ***************************************/


+(void) getInboxListRemovingBlockedUsers:(void (^)(NSDictionary* inboxList))completion{
    
    [self getBlockedUserListToExclude:^(NSDictionary *users) {
        
        __block NSMutableDictionary *withoutBlockedUsers = [[NSMutableDictionary alloc] init];
        
        [self getInboxList:^(NSDictionary *inboxList) {
            
            if (inboxList != nil){
                
                for (NSString *uid in inboxList.allKeys){
                    
                    if ([users objectForKey:uid] == nil){
                        [withoutBlockedUsers setObject:[inboxList valueForKey:uid] forKey:uid];
                    }
                }
                completion(withoutBlockedUsers);
                
            }else{
                completion(nil);
                
            }
        }];
    }];
}




+(void)getInboxList:(void (^)(NSDictionary* inboxList))completion{
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    FIRDatabaseReference *inboxRef = [[self getInboxListPath] child:uid];
    
    [inboxRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            NSDictionary *dict = snapshot.value;
            
            completion(dict);
            
        }else{
            completion(nil);
        }
    }];
}



+(void)getLatestMessage:(NSString*)objectID completion:(void (^)(Inbox* inbox))completion{
    
    FIRDatabaseReference *inboxRef = [[self getConversationPath] child:objectID];
    
    [[[inboxRef queryOrderedByKey] queryLimitedToLast:1] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            NSDictionary *dictionary = snapshot.value;
            
            Inbox *latest = [[Inbox alloc] init];
            
            for(NSString *key in dictionary.allKeys){
                //should be only one result since we limit to last in our query
                [latest dictionaryToInbox:[dictionary valueForKey:key]];
            }
            
            completion(latest);
            
        }else{
            
            completion(nil);
            
        }
    }];
}





/*********************************************************************************************************
 *
 * ------------------------------------- UPDATE CONVO LIST---------------------------------------
 *
 * Code that gathers inbox items and extracts all unique senders/receivers to update the local inbox list
 *
 *********************************************************************************************************/
//
+(void) updateInboxList: (void (^)(NSDictionary *inboxList))completion {
    
    
    [FirebaseManager getInboxListRemovingBlockedUsers:^(NSDictionary *inboxList) {
        
        NSUInteger inboxCount = [inboxList count];
        
        __block NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if(inboxList == nil){
            
            [defaults setObject:[[NSMutableDictionary alloc] init] forKey:@"inboxList"];
            [defaults synchronize];
            completion(nil);
            
            
        }else{
            
            [defaults setObject:inboxList forKey:@"inboxList"];
            [defaults synchronize];
            
            NSMutableDictionary *latestMessagesDict = [[NSMutableDictionary alloc] init];
            
            for(NSString *key in inboxList.allKeys){
                
                NSString *convoId = [inboxList valueForKey:key];
                
                [FirebaseManager getLatestMessage:convoId completion:^(Inbox *inbox) {
                    
                    NSDictionary *latest = [inbox inboxToDictionary];
                    if (inbox != nil){
                        [latestMessagesDict setObject:latest forKey:key];
                    }
                    
                    if (latestMessagesDict.count == inboxCount){
                        
                        NSLog(@"All Users latest Message received");
                        
                        [defaults setObject:latestMessagesDict forKey:@"latestMessages"];
                        [defaults synchronize];
                        
                        completion(latestMessagesDict);
                    }
                }];
            }
        }
    }];
}




+(void) getBadgeCount:(void (^)(int badgeCount))completion{
    
    __block int count = 0;
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    [FirebaseManager getInboxList:^(NSDictionary *inboxList) {
        
        
        for (NSString *key in inboxList.allKeys){
            
            NSString *convoId = [inboxList valueForKey:key];
            
            [FirebaseManager getInboxMessagesForObjectId:convoId completion:^(NSArray *messageArray) {
                
                if(messageArray != nil){
                    
                    for (NSDictionary *message in messageArray){
                        
                        if([[message valueForKey:@"isSeen"] isEqualToString:@"NO"] && ![[message valueForKey:@"senderID"] isEqualToString:uid]){
                            
                            count += 1;
                        }
                    }
                    completion(count);
                    
                }else{
                    completion(0);
                }
            }];
        }
    }];
}




+(void) updateInboxListsForUser:(NSString*)withUserUid completion:(void (^)(NSString* conversationId))completion{
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    //When a message is sent to a new user, update the receiving users's inbox list as well as the sending
    FIRDatabaseReference *myInboxRef = [[[self getInboxListPath] child:uid] child:withUserUid];
    FIRDatabaseReference *theirInboxRef = [[[self getInboxListPath] child:withUserUid] child:uid];
    
    NSString *dateStr = [self nowInMillisString];
    
    [myInboxRef setValue:dateStr];
    [theirInboxRef setValue:dateStr];
    completion(dateStr);
    
}

+(void) updateSingleInboxListForUser:(NSString*)uid parter:(NSString*)parter conversationId:(NSString*)conversationId{
    
    FIRDatabaseReference *inboxRef = [[[self getInboxListPath] child:uid] child:parter];
    [inboxRef setValue:conversationId];
    
}



+(void) getConversationObjectId:(NSString*)withUserUid completion:(void (^)(NSString* conversationId))completion{
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    //When a message is sent to a new user, update the receiving users's inbox list as well as the sending
    FIRDatabaseReference *myInboxRef = [[[self getInboxListPath] child:uid] child:withUserUid];
    FIRDatabaseReference *theirInboxRef = [[[self getInboxListPath] child:withUserUid] child:uid];
    
    __block bool myReturn = NO;
    __block bool theirReturn = NO;
    __block NSString *convoId = @"";
    
    __block int count = 0;
    __block NSString *singleUserParentToUpdate = @"";
    __block NSString *singleUserChildToUpdate = @"";
    
    [theirInboxRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        theirReturn = YES;
        
        if (snapshot.exists){
            convoId = snapshot.value;
        }else{
            count += 1;
            singleUserParentToUpdate = withUserUid;
            singleUserChildToUpdate = uid;
        }
        
        if (myReturn && theirReturn){
            
            completion(convoId);
            
            if (count == 1){
                [self updateSingleInboxListForUser:singleUserParentToUpdate parter:singleUserChildToUpdate conversationId:convoId];
            }
        }
    }];
    
    
    [myInboxRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        myReturn = YES;
        
        if (snapshot.exists){
            convoId = snapshot.value;
        }else{
            count += 1;
            singleUserParentToUpdate = uid;
            singleUserChildToUpdate = withUserUid;
        }
        
        if (myReturn && theirReturn){
            
            completion(convoId);
            
            if (count == 1){
                [self updateSingleInboxListForUser:singleUserParentToUpdate parter:singleUserChildToUpdate conversationId:convoId];
            }
        }
    }];
}





+(void) getInboxMessagesForObjectId:(NSString*)objectId completion:(void (^)(NSArray* messageArray))completion{
    
    FIRDatabaseReference *messagesRef = [[self getConversationPath] child:objectId];
    
    NSMutableArray *allMessages = [[NSMutableArray alloc] init];
    
    [messagesRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            NSDictionary *messages = snapshot.value;
            
            for (NSString *key in messages.allKeys){
                
                [allMessages addObject:[messages valueForKey:key]];
            }
            
            //remove last object since starting to observe the last child added will return the last child
            NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created"
                                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
            NSMutableArray *sortedArray = [allMessages sortedArrayUsingDescriptors:sortDescriptors].mutableCopy;
            
            
            [sortedArray removeLastObject];
            
            FIRDatabaseQuery *newMsgQuery = [messagesRef queryLimitedToLast:1];
            completion(sortedArray);
            
            
            //adding observer for messages sent after the initial load
            [newMsgQuery observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                
                if (snapshot.exists){
                    
                    NSDictionary *message = snapshot.value;
                    NSArray *temp = [NSArray arrayWithObject:message];
                    completion(temp);
                    
                }else{
                    
                    completion(nil);
                }
            }];
        }else{
            
            completion(nil);
        }
    }];
    
    
}


//Will check if a conversation between the two users exists. If no, create one. If yes, just add message to conversation

+(void) setInboxMessage:(NSString*)message recipient:(NSString*)recipient completion:(void (^)(NSString* conversationId))completion{
    
    [self getConversationObjectId:recipient completion:^(NSString *conversationId) {
        if ([conversationId isEqualToString:@"" ]){
            
            //conversation doesn't yet exist, create it
            [self updateInboxListsForUser:recipient completion:^(NSString *conversationId) {
                
                if (![conversationId isEqualToString:@""]){
                    
                    NSString *objId = [self setNewMessageDictionary:message recipient:recipient conversationId:conversationId];
                    completion(conversationId);
                }
            }];
        }else{
            //conversation ID obtained, save new message to firebase
            
            NSString *objId = [self setNewMessageDictionary:message recipient:recipient conversationId:conversationId];
            completion(conversationId);
        }
    }];
}


+(void)postNotification:(NSString*)deviceToken conversationId:(NSString*)conversationId forName:(NSString*)forName withMessage:(NSString*)withMessage{
    
    NSString *sender = [[FIRAuth auth] currentUser].uid;
    NSMutableDictionary *notification = [[NSMutableDictionary alloc] init];
    
    [notification setValue:withMessage forKey:@"messageBody"];
    [notification setValue:deviceToken forKey:@"token"];
    [notification setValue:forName forKey:@"name"];
    [notification setValue:sender forKey:@"sender"];
    [notification setValue:conversationId forKey:@"conversationId"];
    
    FIRDatabaseReference *notifRef = [[self getNotifcationsPath] childByAutoId];
    [notifRef setValue:notification];
    
}




+(NSString*) setNewMessageDictionary:(NSString*)message recipient:(NSString*)recipient conversationId:(NSString*)conversationId{
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    Inbox *inbox = [[Inbox alloc] init];
    inbox.senderID = uid;
    inbox.receiverID = recipient;
    inbox.isSeen = @"NO";
    inbox.messageBody = message;
    NSString *objId = [self nowInMillisString];
    
    inbox.created = objId;
    inbox.objectId = objId;
    
    NSDictionary *final = [inbox inboxToDictionary];
    
    
    FIRDatabaseReference *convoRef = [[[self getConversationPath] child:conversationId] child:objId];
    [convoRef setValue:final];
    
    
    return objId;
}

/**************************************
 *
 *    SEARCH USERS/LOCATION METHODS
 *
 ***************************************/

+(void)setGeoFireLocationForUser:(NSString*)uid latitude:(double)latitude longitude:(double)longitude{
    
    FIRDatabaseReference *geofireRef = [[[FIRDatabase database] reference] child:@"GeoFire"];
    GeoFire *geoFire = [[GeoFire alloc] initWithFirebaseRef:geofireRef];
    
    [geoFire setLocation:[[CLLocation alloc] initWithLatitude:latitude longitude:longitude] forKey:uid
     
     withCompletionBlock:^(NSError *error) {
         
         if (error != nil) {
             NSLog(@"An error occurred: %@", error);
         } else {
             NSLog(@"Saved location successfully!");
         }
     }];
}



+(void)queryNearbyUsersWithRadius:(CLLocation*)location radius:(double)radius completion:(void (^)(NSDictionary * users))completion{
    
    FIRDatabaseReference *geofireRef = [[[FIRDatabase database] reference] child:@"GeoFire"];
    GeoFire *geoFire = [[GeoFire alloc] initWithFirebaseRef:geofireRef];
    
    CLLocation *center = location;
    
    GFCircleQuery *circleQuery = [geoFire queryAtLocation:center withRadius:radius];
    
    NSMutableDictionary *usersInRange =[[NSMutableDictionary alloc] init];
    
    //Query For users and set
    [circleQuery observeEventType:GFEventTypeKeyEntered withBlock:^(NSString *key, CLLocation *location) {
        NSLog(@"Key '%@' entered the search area and is at location '%@'", key, location);
        
        [usersInRange setObject:location forKey:key];
        
        
    }];
    
    //When Query is Complete
    [circleQuery observeReadyWithBlock:^{
        NSLog(@"All initial data has been loaded and events have been fired!");
        
        [circleQuery removeAllObservers];
        completion(usersInRange);
        
    }];
}




/**************************************
 *
 *    BLOCKED USERS METHODS
 *
 ***************************************/


+(void)getAllUsersWhoBlockedMe:(void (^)(NSDictionary * users))completion{
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    FIRDatabaseReference *blockedRef = [[self getBlockedMeUsersPath] child:uid];
    
    __block NSDictionary *blockedUsers = [[NSDictionary alloc] init];
    
    [blockedRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            blockedUsers = snapshot.value;
            
            completion(blockedUsers);
            
        }else{
            
            completion(nil);
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}



+(void)checkIfUserBlocked:(NSString*)uid completion:(void (^)(bool blocked))completion{
    
    NSString *myUid = [[FIRAuth auth] currentUser].uid;
    
    FIRDatabaseReference *blockedRef = [[[self getMyBlockedUsersPath] child:myUid] child:uid];
    
    [blockedRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            completion(YES);
        }else{
            
            completion(NO);
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}



+(void)getAllBlockedUsersForUser:(void (^)(NSDictionary * users))completion{
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    FIRDatabaseReference *blockedRef = [[self getMyBlockedUsersPath] child:uid];
    
    __block NSDictionary *blockedUsers = [[NSDictionary alloc] init];
    
    [blockedRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            blockedUsers = snapshot.value;
            
            completion(blockedUsers);
        }else{
            
            completion(nil);
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}



//Retrieves Dictionary of Users blocked by me and users who blocked me
+(void)getBlockedUserListToExclude:(void (^)(NSDictionary * users))completion{
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    FIRDatabaseReference *iBlockedRef = [[self getMyBlockedUsersPath] child:uid];
    FIRDatabaseReference *blockedMeRef = [[self getBlockedMeUsersPath] child:uid];
    
    __block bool finished = NO;
    
    //All blocked users, by me or otherwise
    __block NSMutableDictionary *combinedBlocked = [[NSMutableDictionary alloc] init];
    
    //Blocks to get data, will return when both have completed
    [iBlockedRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            NSDictionary *iBlockedUsers = snapshot.value;
            [combinedBlocked addEntriesFromDictionary:iBlockedUsers];
        }
        
        if (finished){
            completion(combinedBlocked);
            
        }else{
            finished = YES;
        }
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
    
    
    [blockedMeRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if (snapshot.exists){
            
            NSDictionary *iBlockedUsers = snapshot.value;
            [combinedBlocked addEntriesFromDictionary:iBlockedUsers];
        }
        
        if (finished){
            
            completion(combinedBlocked);
        }else{
            finished = YES;
        }
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}




+(void) setBlockedUser:(NSString*)uid{
    
    NSString *currentLoggedUid = [[FIRAuth auth] currentUser].uid;
    FIRDatabaseReference *blockedRef = [[[self getMyBlockedUsersPath] child:currentLoggedUid] child:uid];
    
    FIRDatabaseReference *theirBlockedRef = [[[self getBlockedMeUsersPath] child:uid] child:currentLoggedUid];
    
    [blockedRef setValue:@"1" withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        
        if (error){
            NSLog(@"Failed to Block User");
        }
    }];
    
    [theirBlockedRef setValue:@"1" withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        
        if (error){
            NSLog(@"Failed to Block User");
        }
    }];
}


+(void)unblockUser:(NSString*)uid{
    
    NSString *currentLoggedUid = [[FIRAuth auth] currentUser].uid;
    
    FIRDatabaseReference *blockedRef = [[[self getMyBlockedUsersPath] child:currentLoggedUid] child:uid];
    
    FIRDatabaseReference *theirBlockedRef = [[[self getBlockedMeUsersPath] child:uid] child:currentLoggedUid];
    
    [blockedRef removeValue];
    [theirBlockedRef removeValue];
    
}




/**************************************
 *
 *    FLAG USERS METHODS
 *
 ***************************************/

+(void)flagUser:(NSString*)uid reason:(NSString*)reason offenderUID:(NSString*)offenderUID offenderEmail:(NSString*)offenderEmail reporterEmail:(NSString*)reporterEmail completion:(void (^)(bool success))completion{
    
    FIRDatabaseReference *flagRef = [[[self getFlaggedUsersPath] child:uid] child:offenderUID];
    
    id objects[] = {reason, offenderEmail, offenderUID, reporterEmail, uid};
    id keys[] = {@"reason", @"offenderEmail", @"offenderUID", @"reporterEmail", @"reporterUID"};
    NSUInteger count = sizeof(objects)/sizeof(id);
    
    NSDictionary *toFlag = [NSDictionary dictionaryWithObjects:objects forKeys:keys count:count];
    
    
    [flagRef setValue:toFlag withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        
        if (error == nil){
            
            completion(YES);
        }else{
            
            completion(NO);
        }
    }];
}



/**************************************
 *
 *    SEND SUPPORT QUERY
 *
 ***************************************/


+(void)sendSupportRequest:(NSString*)uid reason:(NSString*)reason reporterEmail:(NSString*)reporterEmail completion:(void (^)(bool success))completion{
    
    NSString *dateStr = [self nowInMillisString];
    
    FIRDatabaseReference *supportRef = [[[self getSupportPath] child:uid] child:dateStr];
    
    id objects[] = {reason, reporterEmail, uid};
    id keys[] = {@"reason", @"reporterEmail", @"reporterUID"};
    NSUInteger count = sizeof(objects)/sizeof(id);
    
    NSDictionary *toFlag = [NSDictionary dictionaryWithObjects:objects forKeys:keys count:count];
    
    
    [supportRef setValue:toFlag withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        
        if (error == nil){
            
            completion(YES);
        }else{
            
            completion(NO);
        }
    }];
}




/**************************************
 *
 *      PROFILE PHOTO STORAGE
 *
 ***************************************/



+(void)uploadProfilePhoto:(NSData*)imageData completion:(void (^)(NSString *downloadString))completion{
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    FIRStorageReference *myRef = [[[[self getStoragePath] child:uid] child:@"Profile"]child:@"profilePhoto.jpg"];
    
    FIRStorageTask *uploadTask = [myRef putData:imageData
          metadata:nil
        completion:^(FIRStorageMetadata *metadata,
                     NSError *error) {
            if (error != nil) {
                // Uh-oh, an error occurred!
                completion(@"");
                
            } else {
                // Metadata contains file metadata such as size, content-type, and download URL.
                NSURL *downloadURL = metadata.downloadURL;
                NSString *downloadString = downloadURL.absoluteString;
                
                FIRDatabaseReference *userDataPath = [[[self getUsersPath] child:uid] child:@"imgUrl"];

                [userDataPath setValue:downloadString];
                completion(downloadString);
                
            }
        }];
    
    
}








/**************************************
 *
 *    Instagram PHOTO STORAGE METHODS
 *
 ***************************************/


+(void) getPhotoDictionary:(NSString*)uid completion:(void (^)(NSDictionary *photosData))completion{
    
    FIRDatabaseReference *instagramRef = [[self getInstagramPath] child:uid];
    
    [instagramRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if(snapshot.exists){
            
            NSDictionary *photosData = snapshot.value;
            completion(photosData);
            
        }else{
            completion(nil);
        }
    }];
}




+(void)uploadPhoto:(NSData*)imageData creationDate:(NSString*)creationDate caption:(NSString*)caption completion:(void (^)(NSString *downloadString))completion{
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    FIRStorageReference *myRef = [[[[self getStoragePath] child:uid] child:@"Instagram"]child:[NSString stringWithFormat:@"%@.jpg", creationDate]];
    
    
    [myRef putData:imageData
          metadata:nil
        completion:^(FIRStorageMetadata *metadata,
                     NSError *error) {
            if (error != nil) {
                // Uh-oh, an error occurred!
                completion(@"");
                
            } else {
                // Metadata contains file metadata such as size, content-type, and download URL.
                NSURL *downloadURL = metadata.downloadURL;
                NSString *downloadString = downloadURL.absoluteString;
                
                FIRDatabaseReference *instaDataPath = [[self getInstagramPath] child:uid];
                
                NSMutableDictionary *photoData = [[NSMutableDictionary alloc] init];
                
                
                [photoData setValue:downloadString forKey:@"url"];
                [photoData setValue:creationDate forKey:@"created"];
                [photoData setValue:caption forKey:@"caption"];
                
                [[instaDataPath child:creationDate] setValue:photoData];
                completion(downloadString);
                
            }
        }];
    
    
}






+(void)deletePhoto:(NSString*)creationDate completion:(void (^)(bool))completion{
    
    NSString *uid = [[FIRAuth auth] currentUser].uid;
    
    FIRStorageReference *myPhotoRef = [[[self getStoragePath] child:uid] child:[NSString stringWithFormat:@"%@.jpg", creationDate]];
    
    FIRDatabaseReference *myDataRef = [[[self getInstagramPath] child:uid] child:[NSString stringWithFormat:@"%@", creationDate]];
    
    
    [myPhotoRef deleteWithCompletion:^(NSError *error){
        if (error != nil) {
            // Uh-oh, an error occurred!
            completion(NO);
        } else {
            // File deleted successfully
            [myDataRef removeValue];
            completion(YES);
        }
    }];
}



+(NSString*) nowInMillisString{
    
    NSTimeInterval date = [[NSDate date] timeIntervalSince1970];
    NSString *dateStr = [NSString stringWithFormat:@"%.0f",date];
    
    return dateStr;
    
}


@end
