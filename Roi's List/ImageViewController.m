//
//  ImageViewController.m
//  Complice
//
//  Created by Justin Hershey on 6/9/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CaptionsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"captionsCollectionCell"];
    
    
    
}

-(void) viewDidLayoutSubviews{
    
    // set the frame otherwise a few pictures get cut off
    self.collectionView.frame = CGRectMake(0,self.parentViewController.view.frame.origin.y + self.parentViewController.view.frame.size.width/3,self.parentViewController.view.frame.size.width, self.parentViewController.view.frame.size.width + self.parentViewController.view.frame.size.width/2);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Allows us to cache user images to prevent redownloading over and over
- (UIImage *)imageForUrlString:(NSString*)urlString {
    
    if(_cachedImages){
        NSLog(@"Image For Key: %@", [_cachedImages valueForKey:urlString]);
        
    }
    
    __block UIImage *image = [_cachedImages valueForKey:urlString];
    
    if (!image) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            image = [UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [_cachedImages setObject:image forKey:urlString];
                [self.collectionView reloadData];
                
            });
        });
        
    }
    return image;
}

/******************************
 *
 * COLLECITON VIEW DELEGATES
 *
 *****************************/


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    return CGSizeMake(self.parentViewController.view.frame.size.width, self.parentViewController.view.frame.size.width + self.parentViewController.view.frame.size.width/2);
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
     CaptionsCollectionViewCell *cell = (CaptionsCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"captionsCollectionCell" forIndexPath:indexPath];
    
    
    if(_instagramPhotoArray){
        
        if(![[_instagramPhotoArray[indexPath.row] valueForKey:@"url" ] isEqualToString:@"toInstagram"]){
            
            cell.imageView.image = [self imageForUrlString:[_instagramPhotoArray[indexPath.row] valueForKey:@"url"]];
            
            if (![[_instagramPhotoArray[indexPath.row] valueForKey:@"caption"] isEqualToString:@""]){
                cell.captionsLbl.text = [_instagramPhotoArray[indexPath.row] valueForKey:@"caption"];
            }else{
                
                cell.captionsLbl.text = @"";
            }
            
        }else{
            
            cell.imageView.image = [UIImage imageNamed:@"toInstagram"];
            cell.captionsLbl.text = @"";
            
        }
    }

    return cell;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    NSInteger count = 0;
    
    if (_instagramPhotoArray){
        
        count = (NSInteger)_instagramPhotoArray.count;
    }
    return count;
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    
    return 1;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"Open Instagram");
    
    NSURL *instagramApp = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?id=%@",[self.user instagramLinked]]];
    
    NSLog(@"%@", instagramApp);
    
    if([[UIApplication sharedApplication] canOpenURL:instagramApp]){
        [[UIApplication sharedApplication] openURL:instagramApp options:@{} completionHandler:nil];
        
    }else{
        
        NSURL *instagram = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/user?id=%@",self.user.instagramLinked]];
        NSLog(@"Instagram not installed");
        [[UIApplication sharedApplication] openURL:instagram options:@{} completionHandler:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
