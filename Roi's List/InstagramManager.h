//
//  InstagramManager.h
//  Complice
//
//  Created by Justin Hershey on 7/23/17.
//  Copyright © 2017 Complice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"
#import "FirebaseManager.h"

@import Firebase;
@import FirebaseStorage;


@interface InstagramManager : NSObject


-(void) replaceInstagramPictures:(NSString *)forUserID;
- (void)getPhotosListingAsync:(NSString*)uid completion:(void (^)(double mostRecentTime, NSDictionary* currentPhotoData))completion;
- (void)uploadNewPhotos;


@end
