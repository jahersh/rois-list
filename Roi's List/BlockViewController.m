//
//  BlockViewController.m
//  Accomplice
//
//  Created by ibuildx on 5/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "BlockViewController.h"
#import "InboxTableViewCell.h"
#import "DataManager.h"
#import "DemoMessagesViewController.h"
#import "ChatObject.h"
#import "BlockUser.h"
#import <SVProgressHUD.h>
#import <TSMessage.h>
#import "DashBoardViewController.h"
#import "FlagViewController.h"
#import <FBSDKLoginManager.h>


@interface BlockViewController ()
{
    NSMutableArray *allBlockedUsers;
    UIAlertController *alert;
    UIAlertController *uAlert;
    
}
@end



@implementation BlockViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


    NSLog(@"User Obj Id = %@",self.chat.currentUser.uid);
    
    
    [self.view layoutIfNeeded];
    [self maskTriananlgesAndCircularBorderedImage];
    _noBlockedLbl.hidden = YES;
    
    [self tableViewIntegration];

    
    if (!self.usersFound) {
        self.usersFound = [[NSMutableArray alloc] init];
    }
    
    self.shadowView.clipsToBounds = NO;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.6;
    self.shadowView.layer.shadowRadius = 8;
    self.shadowView.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    
    _cachedImages = [[NSMutableDictionary alloc] init];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *imgUrl = self.chat.currentUser.imgUrl;
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.userImageView setImage:[UIImage imageWithData:data]];
            
        });
    });
    
    
    
    allBlockedUsers = [[NSMutableArray alloc] init];
    _usersFound = [[NSMutableArray alloc] init];
    
    [SVProgressHUD show];
    [self getBlockedUsers];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    allBlockedUsers = [[NSMutableArray alloc] init];
    _usersFound = [[NSMutableArray alloc] init];
    _chat.delegate = self;
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}
    

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    

    
    
    
    
-(void) getBlockedUsers{
    __block int blockedCount = 0;
    __block NSMutableArray *blockedUsers = [[NSMutableArray alloc] init];
    
    [FirebaseManager getAllBlockedUsersForUser:^(NSDictionary *busers) {
        
        blockedCount = busers.count;
        
        if (blockedCount == 0){
           
            [SVProgressHUD dismiss];
            
            
        }
        
        for (NSString *key in busers.allKeys){
            
            [FirebaseManager getUserFirebaseDataForUid:key completion:^(UserObject *userdata) {
                
                [blockedUsers addObject:userdata];
                
                if (blockedUsers.count == blockedCount){
                    
                    [_usersFound addObjectsFromArray:blockedUsers];
                    [allBlockedUsers addObjectsFromArray:blockedUsers];
                    
                    [SVProgressHUD dismiss];
                    [self.tableView reloadData];
                }
            }];
        }
    }];
}
    

-(void) noBlockedUsersAlert{
    
    
    UIAlertController *balert=   [UIAlertController
               alertControllerWithTitle:@""
               message:[NSString stringWithFormat:@"No blocked users"]
               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                            
                             [self performSegueWithIdentifier:@"backToSetting" sender:nil];
                             [balert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [balert addAction:ok];
    
    [self presentViewController:balert animated:YES completion:nil];
}

    
    
    
    
    

-(UIImage*)imageForID:(NSString*)fbID{
    
    __block NSString *imgUrl = fbID;
    __block UIImage *image = [_cachedImages valueForKey:imgUrl];
    
    
    if (!image) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
            
            image = [UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_cachedImages setObject:image forKey:imgUrl];
                [self.tableView reloadData];
            });
        });
    }
    return image;
    
}

/*************************************************
 *
 * --------- TABLEVIEW DELEGATE METHODS ----------
 *
 *************************************************/



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _usersFound.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"InboxTableViewCell";
    InboxTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[InboxTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:CellIdentifier];
    }
    cell.clipsToBounds = NO;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOpacity = 0.4;
    cell.layer.shadowOffset = CGSizeMake(6.0f, 6.0f);
    
    UserObject *user = _usersFound[indexPath.row];
    cell.userName.text = user.firstName;
    cell.userIntro.text = user.profileInfo;
    
    [cell.userImageView setImage:[self imageForID:user.imgUrl]];
    
    cell.backGroundRoundedView.layer.cornerRadius = 10 ;
    cell.backGroundRoundedView.clipsToBounds = YES;
    
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width / 2;
    cell.userImageView.clipsToBounds = YES;
    cell.backgroundColor = [UIColor clearColor];
    
    cell.unblockBtn.layer.cornerRadius = 10;
    cell.unblockBtn.tag = indexPath.row;
    
    [cell.unblockBtn addTarget:self action:@selector(unblockAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectedBackgroundView = nil;
    
    return cell;
}
    

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
    

/*************************************************
 *
 * ------------- BUTTON ACTIONS --------------
 *
 *************************************************/


-(void)cellPicAction:(UIButton*)sender
{
    self.selectedUser = _usersFound[sender.tag];
    [self performSegueWithIdentifier:@"ToProfileView" sender:nil];
}


-(void)flagAction:(UIButton*)sender{
    self.selectedUser = _usersFound[sender.tag];
    
    [self performSegueWithIdentifier:@"flagUserView" sender:nil];
    
}

-(void)unblockAction:(UIButton*)sender
{
    self.selectedUser = _usersFound[sender.tag];
    NSString *nameStr = self.selectedUser.firstName;

    
    uAlert=   [UIAlertController
              alertControllerWithTitle:@"Alert"
              message:[NSString stringWithFormat:@"Are you sure you want to unblock %@.", nameStr]
              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             
                             [self reinstateContact:self.selectedUser atLocation:sender.tag];

                             [uAlert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                         actionWithTitle:@"NO"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [uAlert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [uAlert addAction:ok];
    [uAlert addAction:cancel];
    
    [self presentViewController:uAlert animated:YES completion:nil];
    

    
}


- (IBAction)backBtnAction:(id)sender {
    [self performSegueWithIdentifier:@"backToSetting" sender:nil];
    
}

- (IBAction)logOutBtnAction:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"NO" forKey:@"AlreadyUserLogin"];
    [defaults synchronize];
    
    [self performSegueWithIdentifier:@"backToLogin" sender:nil];
    
}




/*************************************************
 *
 * ------------- UNBLOCK METHODS --------------
 *
 *************************************************/



-(void)reinstateContact:(UserObject *)blockedUser atLocation:(int)atlocation
{

    
    [FirebaseManager unblockUser:blockedUser.uid];
    [_usersFound removeObjectAtIndex:atlocation];
    [allBlockedUsers removeObjectAtIndex:atlocation];
    
    [self.tableView reloadData];
    

}

    

#pragma mark - responder
-(id)responseHandler:(id)response
{
    NSLog(@"%@", response);
    return response;
}




#pragma mark - View Starting Methods

-(void)tableViewIntegration {
    
    //set table data source
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}
    
    

-(void)maskTriananlgesAndCircularBorderedImage {
    // Build a triangular path
    self.userImageView.backgroundColor = [UIColor redColor];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
    self.userImageView.clipsToBounds = YES;
    UIColor *bgColor = [UIColor whiteColor];
    self.userImageView.layer.borderWidth = self.userImageView.frame.size.width / 25;
    self.userImageView.layer.borderColor = bgColor.CGColor;
}



    
#pragma mark - Demo delegate
- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}





/*****************************************
 *
 * ----- CHABOBJECT DELEGATE METHODS -----
 *
 *****************************************/

-(void) notificationTouched:(NSString *)type fromUser:(NSString *)fromUser conversationId:(NSString *)conversationId msg:(NSString *)msg title:(NSString *)title{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"refreshInbox"];
    [defaults synchronize];
    
    
    [TSMessage showNotificationInViewController:self
                                          title:title
                                       subtitle:msg
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:2
                                       callback:^{
                                           
                                           [FirebaseManager getUserFirebaseDataForUid:fromUser completion:^(UserObject *userdata) {
                                               
                                               _selectedUser = userdata;
                                               [self performSegueWithIdentifier:@"segueModalDemoVC" sender:self];
                                           }];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
    
    
}



#pragma mark - Navigation

/*****************************************
 *
 * --------- NAVIGATION METHODS ---------
 *
 *****************************************/


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"segueModalDemoVC"]) {
        UINavigationController *nc = segue.destinationViewController;
        DemoMessagesViewController *vc = (DemoMessagesViewController *)nc.topViewController;
        
        vc.currentUser = [self.chat currentUser];
        vc.selectedUser = self.selectedUser;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        vc.conversationId =  [defaults valueForKey:@"selectedConversationId"];
        [defaults setValue:@"BlockView" forKey:@"Source"];
        [defaults synchronize];
        
        [vc setChat:_chat];
        _chat.returned = self;
        _chat.delegate = vc;
        
//        [_chat connectToUser:self.selectedUser.uid];
    }else if([segue.identifier isEqualToString:@"flagUserView"]){
        
        
        FlagViewController *fc = segue.destinationViewController;
        fc.selectedUser = self.selectedUser;
        
        NSLog(@"UserName: %@", fc.selectedUser.firstName);

    }
    else if([segue.identifier isEqualToString:@"backToSetting"])
        {
            
            DashBoardViewController *messageView = segue.destinationViewController;
            [messageView setChat:_chat];
            _chat.returned = self;
            _chat.delegate = messageView;
            
        }
    _chat = nil;
}

- (IBAction)unwindToBlockList:(UIStoryboardSegue *)unwindSegue
{
}


@end
